import sys, os
import urllib, urllib2, re, requests
import xbmc, xbmcplugin, xbmcgui, xbmcaddon
import HTMLParser
import urlresolver

#TV DASH - by You 2008.

addon_id = 'plugin.video.fullmatches'
addon = xbmcaddon.Addon('plugin.video.fullmatches')
fullmatchespath = addon.getAddonInfo('path')

h = HTMLParser.HTMLParser()

def CATEGORIES():
        url='http://www.fullmatchesandshows.com/'
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        req.add_header('Referer', 'http://www.fullmatchesandshows.com/')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        cont=re.compile('<div.+?class="vc_row wpb_row td-pb-row">(.+?)<div class="td-block-row">').findall(link)
        acp_atts=re.compile('<div class="wpb_wrapper">.+?<script>.+?atts.*?=.*?\'(.+?)\';').findall(link)
        ##acp_page=re.compile('td_current_page = (.+?); //').findall(link)
        acp_content=re.compile('<li class="td-subcat-item"><a class="td-subcat-link" id="(.+?)" data-td_filter_value=(.+?) data-td_block_id="(.+?)" href="#">(.+?)</a></li>').findall(cont[0])
        for uid, td_filter_value, td_block_id, sub_cat in acp_content:
                td_atts = acp_atts[0]
                ##for td_current_page in acp_page:
                td_filter_value = td_filter_value.replace('\"','')
                extra='1ZAHY'+td_block_id+'ZAHY'+td_atts+'ZAHY'+td_filter_value
                ##print 'td_block_id: ' + td_block_id +', sub_cat: ' + sub_cat + ', td_filter_value: ' + td_filter_value + ', td_current_page: ' + td_current_page
                addDir(sub_cat,' ',1,extra,os.path.join(fullmatchespath,'resources',td_filter_value+'01.jpg'))


def SUBCATEGORIES(i,block_id,atts,filter_value):
        url2='http://www.fullmatchesandshows.com/wp-admin/admin-ajax.php'

        values = {'action' : 'td_ajax_block',
                  'td_atts' : atts,
                  'td_block_id' : block_id,
                  'td_column_number' : '3',
                  'td_current_page' : i,
                  'block_type' : 'td_block_3',
                  'td_filter_value' : filter_value,
                  'td_filter_ui_uid' : '',
                  'td_user_action' : '' }

        headers = { 'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0'}
        data = urllib.urlencode(values)
        req = urllib2.Request(url2, data, headers)
        response = urllib2.urlopen(req)
        link=response.read()
        response.close()
        block=re.compile('<div class=\\\\"td-block-span4\\\\">.+?<a href=\\\\"(.+?)\\\\".+?title=\\\\"(.+?)\\\\">.+?class=\\\\"entry-thumb\\\\" src=\\\\"(.+?)\\\\"').findall(link)
        for url ,title, thumb in block:
                title = h.unescape(title.replace('&#8211;', '-').replace('\u0107', 'c').replace('\u2013', '-').replace('&#038;','&').replace('&#8217;', '').replace('\u2019', '\''))
                addDir(title,url.replace("\\", ""),2,'',thumb.replace("\\", ""))
        extra=i+'ZAHY'+block_id+'ZAHY'+atts+'ZAHY'+filter_value
        addDir("Next Page: "+str(int(i)+1),' ',1,extra,os.path.join(fullmatchespath,'resources',filter_value+'.jpg'))

def SHOWS(url,thumb,title):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        
        try:
          response = urllib2.urlopen(req)
          link=response.read().replace("\n", "")
          response.close()
        except urllib2.HTTPError, e:
          if e.getcode() == 500:
            link = e.read().replace("\n", "")
          else:
            raise

        try:
          acp_post = re.compile('<input id="acp_post" type="hidden" value="(.+?)"').findall(link)
          acp_pid = acp_post[0]
        except:
          acp_post = re.compile('post:\'(.+?)\'').findall(link)
          acp_pid = acp_post[0]
          pass

        acp_paging_menu=re.compile('id=\"item\d\">(.*?)<div class="acp_title">(.+?)<').findall(link)
        
        if acp_paging_menu:
            for url2,title in acp_paging_menu:
              url2=url2.replace('<a href="','').replace('">','').replace('#','')
              if not url2:
                url2='1'
              print "URL: "+url2+" / acp_pid: "+acp_pid+" / Title: "+title
              addDir(title,url2+'/'+acp_pid,3,' ',thumb)
        else:
            url2=url
            print "URL: "+url2+" / acp_pid: "+acp_pid+" / Title: "+title
            addDir(title,url2+'/'+acp_pid,3,' ',thumb)
                

def QUALITY(url,name,thumb):
        print 'Quality'
        if 'http' in url:
                req = urllib2.Request(url)
                req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                response = urllib2.urlopen(req)
                link=response.read().replace("\n", "")
                response.close()
        else:        
                url,acp_pid = url.split('/')
                params={'acp_currpage': url,
                        'acp_pid':acp_pid,
                        'acp_shortcode':'acp_shortcode',
                        'action':'pp_with_ajax'
                        }
                response = requests.post('http://www.fullmatchesandshows.com/wp-admin/admin-ajax.php',data=params)
                link =response.text

        source = re.compile('src="(.+?)"').findall(link)
        print 'Source'
        print source
        for player in source:
                
          if player.startswith("//"):
             player = 'https:'+player
          print 'Player: '+player

          if 'ok.ru' in player:
            print 'Source: ok.ru' 
            try:
              req = urllib2.Request(player)
              req.add_header('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20150101 Firefox/47.0 (Chrome)')
              response = urllib2.urlopen(req)
              vidconfig1=response.read().replace("\n", "")
              response.close()
              vid2=re.compile('{\\\&quot;name\\\&quot;:\\\&quot;(.+?)\\\&quot;.+?url\\\&quot;:\\\&quot;(.+?)\\\&quot;').findall(vidconfig1)
              for name, vidurl in vid2:
                mediaurl= vidurl.replace('\\\\u0026','&')
                print 'Name: ok.ru ('+name+') / URL: '+mediaurl
                addDir('ok.ru ('+name+')',mediaurl,4,' ',thumb)

            except:
              print "skip ok.ru"
              pass

          elif 'streamable' in player:
            print 'Source: streamable' 
            try:
              req = urllib2.Request(player)
              req.add_header('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20150101 Firefox/47.0 (Chrome)')
              response = urllib2.urlopen(req)
              vidconfig1=response.read().replace("\n", "")
              response.close()
              vid2=re.compile('poster="(.+?)".+?<source src="(.+?)"').findall(vidconfig1)
              for poster, vidurl in vid2:
                if vidurl.startswith('//'):
                  mediaurl = 'https:'+vidurl
                print 'Name: Streamable / URL: '+mediaurl
                addDir('Streamable',mediaurl,4,' ',thumb)
            except:
              print "skip Streamable"
              pass 

          elif 'weshare' in player:
            print 'Source: weshare'
            try:
              req = urllib2.Request(player)
              req.add_header('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20150101 Firefox/47.0 (Chrome)')
              response = urllib2.urlopen(req)
              vidconfig1=response.read().replace("\n", "")
              response.close()
              vid2=re.compile('poster="(.+?)".+?<source src="(.+?)"').findall(vidconfig1)
              for poster, vidurl in vid2:
                print 'Name: WeShare / URL: '+mediaurl
                addDir('WeShare',mediaurl,4,' ',thumb)
            except:
              print "skip WeShare"
              pass

          elif 'dailymotion' in player:
            print 'Source: dailymotion'
            url = player.replace("/embed", "")
            req = urllib2.Request(url)
            req.add_header('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20150101 Firefox/47.0 (Chrome)')
            response = urllib2.urlopen(req)
            link=response.read().replace("\n", "")
            response.close()
            match=re.compile("application\\\/x-mpegURL.+?\"}\],(.+?)},").findall(link)
            print match

            for quality in match:
                  if 'mp4' in quality:
                    print 'Dailymotion MP4'
                    match2=re.compile("\"(.+?)\":\[{\"type\":\"video\\\/mp4\",\"url\":\"(.+?)\"}\]").findall(quality)
                    for res, video in match2:
                          mediaurl=urllib.unquote_plus(video).replace("\\", "")
                          print "Dailymotion Res: "+res+"P / Vid:"+mediaurl
                          addDir('Dailymotion Res: '+res+'P',mediaurl,4,' ',thumb)
                  else:
                    print 'Dailymotion m3u8'
                    match2=re.compile("\"(.+?)\":\[{\"type\":\"application\\\/x-mpegURL\",\"url\":\"(.+?)\"").findall(quality)
                    for res, video in match2:
                          vidurl=urllib.unquote_plus(video).replace("\\", "")
                          req = urllib2.Request(vidurl)
                          req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                          response = urllib2.urlopen(req)
                          vidconfig2=response.read().replace("\n", "")
                          response.close()
                          vid=re.compile("NAME=\".*?\"(.+?)#").findall(vidconfig2)
                          mediaurl=vid[0]
                          print "Dailymotion "+res+"P / Vid:"+mediaurl
                          addDir('Dailymotion Res: '+res+'P',mediaurl,4,' ',thumb)

          elif 'openload' in player:
            print 'Source: openload'
            try:
              stream_url = urlresolver.resolve(player)
              print 'Name: OpenLoad / URL: '+stream_url
              addDir('OpenLoad',stream_url,4,' ',thumb)
            except:
              print "skip OpenLoad"
              pass

def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]

        return param

def playVid(url,name):
        listitem = xbmcgui.ListItem(name)
        xbmc.Player().play(url,listitem)
        return

def addDir(name,url,mode,extra,iconimage):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&thumb="+str(iconimage)+"&mode="+str(mode)+"&extra="+str(extra)+"&name="+urllib.quote_plus(name.replace('\'',''))
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        liz.setProperty('fanart_image',iconimage)
        ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        return ok

params=get_params()

url=None
name=None
mode=None
thumb=None
extra=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass
try:
        thumb=urllib.unquote_plus(params["thumb"])
except:
        pass
try:
        extra=str(params["extra"])
except:
        pass

print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "Thumb: "+str(thumb)
print "Extra: "+str(extra)

if mode==None or url==None or len(url)<1:
        print ""
        CATEGORIES()
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
       
elif mode==1:
        print ""+extra
        substr = urllib.unquote(extra).split('ZAHY')
        if 'Next Page: ' in name:
            i= name.split('Next Page: ')[1]
        else:
            i= substr[0]
        print i
        block_id =substr[1]
        print block_id
        atts =substr[2]
        print atts
        filter_value =substr[3]
        print filter_value
        SUBCATEGORIES(i,block_id,atts,filter_value)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==2:
        print ""+url
        SHOWS(url,thumb,name)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==3:
        print ""+url
        QUALITY(url,name,thumb)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==4:
        print "stream_url "+url
        playVid(url,name)
