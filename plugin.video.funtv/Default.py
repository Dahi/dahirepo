# -*- coding: utf-8 -*-
import sys, os
import urllib, urllib2, re

import xbmc, xbmcplugin, xbmcgui, xbmcaddon

#TV DASH - by You 2008.

host='https://www.tvfun.ma'
addon_id = 'plugin.video.funtv'
addon = xbmcaddon.Addon('plugin.video.funtv')
funtvpath = addon.getAddonInfo('path')


def CATEGORIES():
        url='https://www.tvfun.ma/'
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
        req.add_header('Accept-Language', 'en-US,en;q=0.5')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<nav>(.+?)</nav>').findall(link)
        match2=re.compile('href="(.+?)"><span>(.+?)</span>').findall(match[0])
        for url, title in match2:
          if url.find('-') > -1:
             print 'url: '+ url+' / title: '+title
             img = url[url.find('-')+1:].replace('/','')
             print img
             thumb=os.path.join(funtvpath,'resources', 'Categories',img+'.jpg')
             addDir(title,url,1,thumb,thumb)
            
def SHOWS(url):
        url=host+url
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
        req.add_header('Accept-Language', 'en-US,en;q=0.5')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<div class="WhiteHeaderWide">(.+?)id="sidebar"').findall(link)
        match2=re.compile('<a href="(.+?)".*?title="(.+?)".*?src="(.+?)"').findall(match[0])
        for url, title, img in match2:
            print 'URL: '+ url+' / title: '+title+' / Thumb: https:'+img
            addDir(title,url,2,'https:'+img,'https:'+img)
        pages=re.compile('<li><a href=\'(.+?)\'.*?>(.+?)\s*?<').findall(match[0])
        for url, title in pages:
            print 'URL: '+ url+' / title: '+title
            addDir(title,url,1,os.path.join(funtvpath,'resources', 'Pages',title+'.jpg'),os.path.join(funtvpath,'resources', 'Pages','pages.jpg'))

def EPISODES(url):        
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
        req.add_header('Accept-Language', 'en-US,en;q=0.5')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<div class="WhiteHeaderWide">(.+?)id="sidebar"').findall(link)
        description=re.compile('class="content"><p>(.+?)</p>').findall(match[0])
        match2=re.compile('<div class="video-thumb"> <a href="(.+?)".*?title="(.+?)".+?src="(.+?)"').findall(match[0])

        for url, title, img in match2:
            if not url.startswith('javascript'):
                print 'URL: '+ url+' / title: '+title+' / Thumb: https:'+img
                addDir(title, url,3,'https:'+img,'https:'+img)
                
        pages=re.compile('<li><a href=\'(.+?)\'.*?>(.+?)\s*?<').findall(match[0])
        for url, title in pages:
            print 'URL: '+ url+' / title: '+title
            addDir(title,url,2,os.path.join(funtvpath,'resources', 'Pages',title+'.jpg'),os.path.join(funtvpath,'resources', 'Pages','pages.jpg'))
                         
def SOURCE(url,name,thumb):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('class="videocontainer"(.+?)</div>').findall(link)
        vid=re.compile('src="(.+?)"').findall(match[0])
        
        if 'dailymotion.com/embed' in vid[0]:
                vidurl = 'https:'+vid[0][:vid[0].find('?')]
                print 'VIDURL: '+vidurl
                addDir('Dailymotion: '+name, vidurl,4,thumb,thumb)
                
        elif '/pl' in vid[0]:
                req = urllib2.Request('https:'+ vid[0])
                req.add_header('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20150101 Firefox/47.0 (Chrome)')
                response = urllib2.urlopen(req)
                vidconfig1=response.read().replace("\n", "")
                response.close()
                vid2=re.compile('\"link\": \"(.+?)"').findall(vidconfig1)
                for idx, vidurl in enumerate(vid2):
                        print 'Title: (Part '+str(idx+1) +') / URL: '+vidurl
                        if 'dai.ly' in vidurl:
                                streamurl='https://www.dailymotion.com/embed/video/'+vidurl.replace('https://dai.ly/','')
                                addDir(name+' (Part '+str(idx+1)+')', streamurl,4,thumb,thumb)
                                
        elif 'dailymotion.com/widget/jukebox' in vid[0]:
                vidurl = 'https:'+ vid[0]
                print 'URL: '+ vidurl
                req = urllib2.Request(vidurl)
                req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
                response = urllib2.urlopen(req)
                vidlink=response.read().replace("\n", "")
                vidmatch=re.compile('href="/video/(.+?)"').findall(vidlink)
                response.close()
                for idx, vidurl in enumerate(vidmatch):
                        print 'Title: (Part '+str(idx+1) +') / URL: '+vidurl
                        streamurl='https://www.dailymotion.com/embed/video/'+vidurl
                        addDir(name+' (Part '+str(idx+1)+')', streamurl,4,thumb,thumb)

        elif '/panet' in vid[0]:
                req = urllib2.Request('https:'+vid[0])
                req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
                response = urllib2.urlopen(req)
                link=response.read().replace("\n", "")
                match=re.compile('imageHtml\(\'(.+?)\'\)').findall(link)
                response.close()
                vidurl = urllib.unquote_plus(match[0])
                print 'VIDURL: '+vidurl
                addDir('Panet',vidurl,6,' ',thumb)
                
        elif 'ok.ru' in vid[0]:
                print 'URL: https:'+ vid[0]
                req = urllib2.Request('https:'+ vid[0])
                req.add_header('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20150101 Firefox/47.0 (Chrome)')
                response = urllib2.urlopen(req)
                vidconfig1=response.read().replace("\n", "")
                response.close()
                vid2=re.compile('{\\\&quot;name\\\&quot;:\\\&quot;(.+?)\\\&quot;.+?url\\\&quot;:\\\&quot;(.+?)\\\&quot;').findall(vidconfig1)
                for name, vidurl in vid2:
                        mediaurl= vidurl.replace('\\\\u0026','&')
                print 'Name: ok.ru ('+name+') / URL: '+mediaurl
                addDir('ok.ru ('+name+')',mediaurl,5,' ',thumb)
                    
        else:
                print 'unknown'
        
def DM_VID(stream_url,name):
        import urlresolver
        stream_url = urlresolver.resolve(stream_url)
        print "stream_url: "+stream_url
        listitem = xbmcgui.ListItem(name)
        xbmc.Player().play(stream_url,listitem)
        return

def playVid(url,name):
        listitem = xbmcgui.ListItem(name)
        xbmc.Player().play(url,listitem)
        return
        
def play_panet(url):
    host = url[:url.find('.net')+4].replace('http://','')
    liz=xbmcgui.ListItem()
    dictn = {'Host': host,
             'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0',
             'Accept': 'video/webm,video/ogg,video/*;q=0.9,application/ogg;q=0.7,audio/*;q=0.6,*/*;q=0.5',
             'Accept-Language': 'en-US,en;q=0.5',
             'Range': 'bytes=0-',
             'Connection': 'keep-alive',
             'Pragma': 'no-cache',
             'Cache-Control': 'no-cache'}
    keys = urllib.urlencode(dictn)
    stream_url = url + "|" + keys
    xbmc.Player().play(stream_url,liz)

    return
             
def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param

def addDir(name,url,mode,iconimage,fanart):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        liz.setProperty('fanart_image',fanart)
        xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        ##ok=xbmcplugin.addSortMethod(int(sys.argv[1]), xbmcplugin.SORT_METHOD_TITLE)
        return ok
        
              
params=get_params()
url=None
name=None
mode=None
thumb=None
fanart=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass
try:
        thumb=urllib.unquote_plus(params["thumb"])
except:
        pass
try:
        fanart=urllib.unquote_plus(params["fanart"])
except:
        pass
    
print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "Thumb: "+str(thumb)
print "FanArt: "+str(fanart)

if mode==None or url==None or len(url)<1:
        print ""
        CATEGORIES()
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==1:
        print ""+url
        SHOWS(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==2:
        print ""+url
        EPISODES(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==3:
        print ""+url
        SOURCE(url,name,thumb)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
      
elif mode==4:
        print "stream_url: "+url
        DM_VID(url,name)
        
elif mode==5:
        print "stream_url: "+url
        playVid(url,name)
        
elif mode==6:
        print "stream_url: "+url
        play_panet(url)
