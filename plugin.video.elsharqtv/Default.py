import urllib, urllib2, re
import xbmc, xbmcplugin, xbmcgui, xbmcaddon

addon_id = 'plugin.video.elsharqtv'

def QUALITIES():
        url= 'http://cdn.edge.tmed.pw:8081/elsharqtv_main/main/playlist.m3u8'
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('RESOLUTION=(.+?),.+?http(.+?)nimblesessionid=(.+\d)').findall(link)
        for res, url,sid in match:
                url="http"+url+"nimblesessionid="+sid
                addDir(res,url,1,'')

def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param

def playVid(url,name):
        listitem = xbmcgui.ListItem(name)
        xbmc.Player().play(url,listitem)
        return

def addDir(name,url,mode,iconimage):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        liz.setProperty('fanart_image',iconimage)
        ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        return ok
        
              
params=get_params()
url=None
name=None
mode=None
thumb=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass
try:
        thumb=urllib.unquote_plus(params["thumb"])
except:
        pass
    
print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "Thumb: "+str(thumb)

if mode==None or url==None or len(url)<1:
        print ""
        QUALITIES()
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
       
elif mode==1:
        print ""+url
        playVid(url,name)

