# -*- coding: utf-8 -*-

import sys, os
import urllib, urllib2, re
import xbmc, xbmcplugin, xbmcgui, xbmcaddon
import urlresolver

addon_id = 'plugin.video.mobikora'
addon = xbmcaddon.Addon(addon_id)
mobikorapath = addon.getAddonInfo('path')

def CATEGORIES():
        addDir('قنوات','http://mobikora.tv/app/channels',1,' ',' ',os.path.join(mobikorapath,'resources','channels.png'))
        addDir('بطولات','http://mobikora.tv/app/leagues',2,' ',' ',os.path.join(mobikorapath,'resources','leagues.png'))
        

def CHANNELS(url):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        channel=re.compile('"id": .+?"name": "(.*?)",.+?"logo": "(.+?)",.+?"streams": \[(.*?)\]').findall(link)
        for chname, chlogo, chstream in channel:
            if len(chname.replace(' ','').replace('    ','')) > 1 and len(chstream.replace(' ','').replace('    ','')) > 1:
               addDir(chname,' ',4,' ',chstream,chlogo)

            
def LEAGUES(url):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        dates=re.compile('"date": "(.+?)",').findall(link)
        for date in dates:
          league=re.compile('"league_id": (.+?), "league_name": "(.+?)", "league_logo": "(.+?)",.+?league_matches":(.+?)} ] }').findall(link)
          for lid, lname, llogo,matches in league:
            addDir(lname,lid,3,date,matches,llogo)


def MATCHES(leadue_id,league,date,link2):
        url='http://mobikora.tv/app/channels'
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        matches=re.compile('"team1logo": "(.+?)", "team2logo": "(.+?)", "match_time": "(.+?)", "channels_id": (.+?), "team1": "(.+?)", "team2": "(.+?)"').findall(link2)
        for tlogo1,tlogo2,matchtime,channelid,team1,team2 in matches:
          channel=re.compile('"id": '+channelid+',.+?"name": "(.+?)",.+?"logo": "(.+?)",.+?"streams": \[(.+?)\]').findall(link)
          for chname, chlogo, chstream in channel:
            addDir(team1+' vs. '+team2,chname+'\n'+league,4,date,chstream,chlogo)
            
            
def QUALITY(streams,date,match,channel_logo):
        game=re.compile('quality": "(.+?)",.+?"url": "(.+?)"').findall(streams)
        for res, url in game:
          if str(url).find('iphone')>0:
            res2=res+' (iPhone)'
          else:
            res2=res
            
          addDir(res2,url,5,date,match,channel_logo)
          
                
def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param


def playVid(url,name):
        listitem = xbmcgui.ListItem(name)
        xbmc.Player().play(url,listitem)
        return


def addDir(name,url,mode,date,matches,iconimage):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&date="+str(date)+"&matches="+str(matches)+"&name="+urllib.quote_plus(name)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        if mode == 1:
           liz.setInfo( type="Video", infoLabels={ "Title": name, "Date": "", "Plot": ""} )
           liz.setProperty('fanart_image',os.path.join(mobikorapath,'resources','channels_fanart.jpg'))
        if mode == 2:
           liz.setInfo( type="Video", infoLabels={ "Title": name, "Date": "", "Plot": ""} )
           liz.setProperty('fanart_image',os.path.join(mobikorapath,'resources','leagues_fanart.jpg'))
        elif mode == 3 or mode == 5:
           liz.setInfo( type="Video", infoLabels={ "Title": name, "Date": date, "Plot": ""} )
           liz.setProperty('fanart_image',os.path.join(mobikorapath,'resources','mobikora_fanart.jpg'))
        elif mode == 4:
           liz.setInfo( type="Video", infoLabels={ "Title": name, "Date": date, "Plot": url} )
           liz.setProperty('fanart_image',os.path.join(mobikorapath,'resources','mobikora_fanart.jpg'))

        ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        return ok
        
              
params=get_params()

url=None
name=None
mode=None
date=None
matches=None
thumb=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass
try:
        date=str(params["date"])
except:
        pass
try:
        matches=urllib.unquote_plus(params["matches"])
except:
        pass
try:
        thumb=urllib.unquote_plus(params["thumb"])
except:
        pass
    
print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "Date: "+str(date)
##print "Match: "+str(matches)
print "Thumb: "+str(thumb)


if mode==None or url==None or len(url)<1:
        print ""
        CATEGORIES()
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==1:
        print ""
        CHANNELS(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==2:
        print ""
        LEAGUES(url)
        xbmcplugin.setContent(int(sys.argv[1]),content="episodes")
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
       
elif mode==3:
        ##print ""+matches
        MATCHES(url,name,date,matches)
        xbmcplugin.setContent(int(sys.argv[1]),content="episodes")
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==4:
        print ""+url
        QUALITY(matches,date,url+'\n'+name,thumb)
        xbmcplugin.setContent(int(sys.argv[1]),content="episodes")
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==5:
        print "stream_url "+url
        if url.find("youtube") > -1:
           url = urlresolver.resolve(url)
        playVid(url,name)
