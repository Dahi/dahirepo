﻿import sys, os
import urllib, urllib2, re
import json

import xbmc, xbmcplugin, xbmcgui, xbmcaddon

#TV DASH - by You 2008.

addon_id = 'plugin.video.egytube'
addon = xbmcaddon.Addon('plugin.video.egytube')
egytubepath = addon.getAddonInfo('path')

def CATEGORIES():
        addDir('أحمد بحيري','https://www.youtube.com/user/islamheartvideos/videos?view=0&sort=dd&shelf_id=1',1,os.path.join(egytubepath,'resources','islamheartvideos.jpg'))
        addDir('Joe Tube','https://www.youtube.com/user/JoeTubeVid/videos?view=0&sort=dd&shelf_id=1',1,os.path.join(egytubepath,'resources','joetube.png'))
        addDir('Joe Show','https://www.youtube.com/channel/UCVlAsbQEG1yvoAYzt1TEo0Q/videos',1,os.path.join(egytubepath,'resources','joetube.png'))
        
def EPISODES(url):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('spf-link  ux-thumb-wrap contains-addto.+?a href="(.+?)".+?src="(.+?)".+?h3 class="yt-lockup-title.+?title="(.+?)"').findall(link.replace("\n", ""))
        for url,thumb,name in match:
            addDir(name.replace("&quot;","\""),"https://www.youtube.com"+url,3,thumb)
        if "data-uix-load-more-href=" in link:
            loadmore = re.compile('data-uix-load-more-href="(.+?)"').findall(link)
            for url2 in loadmore:
                addDir("Next","https://www.youtube.com"+url2.replace("&amp;","&"),2,os.path.join(egytubepath,'resources', 'Next.jpg'))

def EPISODES_NEXT(url):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read()
        response.close()
        link=json.loads(link)
        link_url=link['content_html']
        link_next=link['load_more_widget_html']

        match=re.compile('spf-link  ux-thumb-wrap contains-addto.+?a href="(.+?)".+?src="(.+?)".+?h3 class="yt-lockup-title.+?title="(.+?)"').findall(link_url.replace("\n", ""))
        for url,thumb,name in match:
            name=name.encode('utf-8')
            addDir(name.replace("&quot;","\""),"https://www.youtube.com"+url,3,thumb)
        if "data-uix-load-more-href=" in link_next:
            loadmore = re.compile('data-uix-load-more-href="(.+?)"').findall(link_next)
            for url2 in loadmore:
                addDir("Next","https://www.youtube.com"+url2.replace("&amp;","&"),2,os.path.join(egytubepath,'resources', 'Next.jpg'))

def ply(url,name):
    ok=True
    listitem = xbmcgui.ListItem()
    xbmc.Player().play(url,listitem)
    return ok

def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param

def addDir(name,url,mode,iconimage):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        liz.setProperty('fanart_image',iconimage)
        ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        return ok
        
              
params=get_params()
url=None
name=None
mode=None
thumb=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass
try:
        thumb=urllib.unquote_plus(params["thumb"])
except:
        pass
    
print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "Thumb: "+str(thumb)

if mode==None or url==None or len(url)<1:
        print ""
        CATEGORIES()
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==1:
        print ""+url
        EPISODES(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==2:
        print ""+url
        EPISODES_NEXT(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==3:
        import urlresolver
        print "ply url "+url
        stream_url = urlresolver.resolve(url)
        print "stream_url "+stream_url
        ply(stream_url,name)
