﻿import sys, os
import urllib, urllib2, re, requests
from resources.lib import cloudflare
import xbmc, xbmcplugin, xbmcgui, xbmcaddon

addon_id = 'plugin.video.ashams'
addon = xbmcaddon.Addon('plugin.video.ashams')
ashamspath = addon.getAddonInfo('path')
cookiepath= xbmc.translatePath('special://profile/addon_data/%s' % addon_id).decode('utf-8')
if not os.path.exists(cookiepath):
    os.makedirs(cookiepath)
cookiefile=xbmc.translatePath(os.path.join(cookiepath,'ashamscookie.txt'))

url='http://www.ashams.com/'
user_agent='Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3'


##Clear Cookie File
def CLEAR_TOKEN():
        print "Clearing Cookies: fresh start"
        try:
                LFC=open(cookiefile, 'w+')
                LFC.close()
        except:
                pass
        return

def SAVE_TOKEN(scookies):
    LFW=open(cookiefile, 'w+')
    LFW.write(scookies)
    LFW.close()
    return

def LOAD_TOKEN():
    if os.path.isfile(cookiefile):
        LFR=open(cookiefile, 'r')
        lcookies=LFR.readline()
        LFR.close()
    else:
        lcookies=''
    
    host='http://www.ashams.com/'
    s = requests.session()
    response=s.request("GET",host)
    if ( "URL=/cdn-cgi/" in response.headers.get("Refresh", "") and response.headers.get("Server", "") == "cloudflare-nginx" ):
        print "Cloudflare anti-bot is on"
        if lcookies.find("cf_clearance") > -1:
                print "Cloudflare cookie already exists"
                return lcookies
        else:
                print "Obtaining Cloudflare cookie"
                scookie=cloudflare.request(host)
                SAVE_TOKEN(scookie)
                return scookie
    else:
        print "No Cloudflare anti-bot detected"    
        return s.cookies


def CATEGORIES():
        addDir('مسلسلات','http://www.ashams.com/videos/550-مسلسلات',1,os.path.join(ashamspath,'resources', 'Categories','MSLSLAT.jpg'),0)
        addDir('افلام','http://www.ashams.com/videos/553-افلام',2,os.path.join(ashamspath,'resources', 'Categories','aflam.jpg'),0)
        addDir('مسرحيات','http://www.ashams.com/videos/587-مسرحيات',3,os.path.join(ashamspath,'resources', 'Categories','msr7yat.jpg'),0)
        addDir('اطــفال','http://www.ashams.com/videos/563-اطفال',1,os.path.join(ashamspath,'resources', 'Categories','KRTWN.jpg'),0)
        addDir('برامج تلفزيون','http://www.ashams.com/videos/1243-برامج-تلفزيون',2,os.path.join(ashamspath,'resources', 'Categories','bramg tlfzywn.jpg'),0)

def SUBCATEGORIES(url):
        cookie = LOAD_TOKEN()
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        req.add_header('Referer','http://www.ashams.com/')
        req.add_header('Cookie', cookie)
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<div class="item ias-item">.*?<a href="(.*?)"><figure style="background: url\((.*?)\).+?<div><p>(.*?)</p></div>').findall(link)
        for url, thumb, name in match:
            addDir(name,"http://www.ashams.com" +url,2,thumb,0)

def SHOWS(url):
        cookie = LOAD_TOKEN()
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        req.add_header('Referer','http://www.ashams.com/')
        req.add_header('Cookie', cookie)
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<div class="item ias-item">.*?<a href="(.*?)"><figure style="background: url\((.*?)\).+?<div><p>(.*?)</p></div>').findall(link)
        for url, thumb, name in match:
            name=name.replace('ميديا ','').replace('الشمس','').replace('| ','').replace(' |','').replace(' | ','').replace('مشاهدة ','').replace('مسلسل ','')
            addDir(name,"http://www.ashams.com" +url,3,thumb,1)
            
def VOD(url,i):
        cookie = LOAD_TOKEN()
        print "VOD URL: "+ url
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        req.add_header('Referer','http://www.ashams.com/')
        req.add_header('Cookie', cookie)
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "").replace("\t", "")
        response.close()
        match=re.compile('<div class="ias-item item">.*?<a href="(.*?)"><figure style="background: url\((.*?)\).*?<p>(.*?)</p>').findall(link)
        for url2, thumb, name in match:
            addDir(name,"http://www.ashams.com" +url2,4,thumb,i)

        url = re.sub('\?page=\d*','',url)            
        addDir("Next",url+"?page="+str(i+1),3,os.path.join(ashamspath,'resources', 'Categories','Next.jpg'),i+1)
##        try:
##            pages=re.compile('<div class=\'paging_all\'(.+?)<div class=\'footer\'>').findall(link)
##            getpage=re.compile('<a href=\'(.+?)\'>(.+?)</a></div>').findall(pages[0])
##            for url, name in getpage:
##                addDir("Page: "+name,'http://www.ashams.com'+url,3,os.path.join(ashamspath,'resources', 'Pages',name+'.jpg'))
##        except: pass

def VIDEOLINKS(url,name):
        cookie = LOAD_TOKEN()
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        req.add_header('Referer','http://www.ashams.com/')
        req.add_header('Cookie', cookie)
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        if link.find("data-video-id") > -1:
            vidlink=re.compile('data-video-id="(.+?)"').findall(link)
            configreq2 = urllib2.Request('http://cdn.playwire.com/v2/15880/related/'+vidlink[0]+'.json')
            configresponse2 = urllib2.urlopen(configreq2)
            config2=configresponse2.read()
            configresponse2.close()
            vidlink2=re.compile('mp4:video-(.+?)-').findall(config2)
            vidlink3='http://cdn.playwire.com/15880/video-'+vidlink2[0]+'-'+vidlink[0]+'.mp4'
            playVid(vidlink3,name)
            
        elif link.find("script data-config") > -1:
            vidlink=re.compile('script data-config="(.+?)"').findall(link)
            link="http:"+vidlink[0].replace("http:","").replace("zeus.json","manifest.f4m").replace("player.json","manifest.f4m")
            configreq = urllib2.Request(link)
            configresponse = urllib2.urlopen(configreq)
            config=configresponse.read()
            configresponse.close()
            vidlink2=re.compile('<baseURL>(.+?)</baseURL>').findall(config)
            vidlink3 = vidlink2[0]+"/video-sd.mp4"
            playVid(vidlink3,name)
            
        elif link.find("m3u8") > -1:
            vidlink=re.compile('<div class=\'videophone\'><a href=\'(.+?)\'>').findall(link)
            playVid(vidlink[0],name)
            
        elif link.find("www.youtube.com/watch") > -1:
            match=re.compile('http://www.youtube.com/watch?(.+?)[\'|"<]').findall(link)
            vidlink="http://www.youtube.com/watch?"+match[0].replace(" ", "")
            print "YouTube url: "+vidlink
            import urlresolver            
            stream_url = urlresolver.resolve(vidlink)
            playVid(stream_url,name)
            
        elif link.find("div class='newplayer'>") > -1:
            match=re.compile('<div class=\'newplayer\'><a href="(.+?)"').findall(link)
            print "redirect url: "+match[0]
            req = urllib2.Request(match[0])
            req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
            response = urllib2.urlopen(req)
            link1=response.read().replace("\n", "")
            response.close()
            match1=re.compile('<source type="video/mp4" src="(.+?)"').findall(link1)
            stream_url=match1[0].replace(' ','%20')
            print "Stream URL: "+stream_url
            playVid(stream_url,name)

        elif link.find("https://openload.co/embed") > -1:
            match=re.compile('https://openload.co/embed/(.+?)"').findall(link)
            vidlink = 'https://openload.co/embed/'+match[0]
            print "Openload url: "+vidlink
            import urlresolver
            stream_url = urlresolver.HostedMediaFile(url=vidlink).resolve ()
            print '#############################################'
            print stream_url
            stream_url = urlresolver.resolve(vidlink)
            print '#############################################'
            print stream_url
            print '#############################################'
            ##stream_url = urlresolver.HostedMediaFile(url=vidlink).resolve ()
            playVid(stream_url,name)
            
        elif link.find("<div class=\"main") > -1:
            match=re.compile('<div class="main".+?src="(.+?)"').findall(link)
            if match[0].find ('youtube') > -1:
                print "YouTube url: "+match[0]
                import urlresolver            
                stream_url = urlresolver.resolve(match[0])
                playVid(stream_url,name)
                
            elif match[0].find ('ashams') > -1:
                req = urllib2.Request(match[0])
                req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
                req.add_header('Referer','http://www.ashams.com/')
                response = urllib2.urlopen(req)
                link1=response.read().replace("\n", "")
                response.close()
                match1=re.compile('<source src="(.+?)"').findall(link1)
                stream_url=match1[0].replace(' ','%20')
                print "Stream URL: "+stream_url
                playVid(stream_url,name)
                
            else:
                "None"
                return ok
            
        else:
            print "UNKNOWN VIDEO"
            return ok
                
def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param

def playVid(url,name):
        listitem = xbmcgui.ListItem(name)
        xbmc.Player().play(url,listitem)
        return

def addDir(name,url,mode,iconimage,i):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)+"&i="+str(i)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        liz.setProperty('fanart_image',iconimage)
        ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        return ok
        
              
params=get_params()
url=None
name=None
mode=None
thumb=None
i=0

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass
try:
        thumb=urllib.unquote_plus(params["thumb"])
except:
        pass
try:
        i=int(params["i"])
except:
        pass
    
print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "Thumb: "+str(thumb)
print "i: "+str(i)

if mode==None or url==None or len(url)<1:
        print ""
        CATEGORIES()
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
       
elif mode==1:
        print ""+url
        SUBCATEGORIES(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==2:
        print ""+url
        SHOWS(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==3:
        print ""+url
        VOD(url,i)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==4:
        print ""+url
        VIDEOLINKS(url,name)
