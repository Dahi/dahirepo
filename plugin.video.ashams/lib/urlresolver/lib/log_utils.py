import urlresolver.xbmc
import urlresolver.xbmcaddon

addon = urlresolver.xbmcaddon.Addon('script.module.urlresolver')
name = addon.getAddonInfo('name')

LOGDEBUG = urlresolver.xbmc.LOGDEBUG
LOGERROR = urlresolver.xbmc.LOGERROR
LOGFATAL = urlresolver.xbmc.LOGFATAL
LOGINFO = urlresolver.xbmc.LOGINFO
LOGNONE = urlresolver.xbmc.LOGNONE
LOGNOTICE = urlresolver.xbmc.LOGNOTICE
LOGSEVERE = urlresolver.xbmc.LOGSEVERE
LOGWARNING = urlresolver.xbmc.LOGWARNING

def log_debug(msg):
    log(msg, level=LOGDEBUG)

def log_notice(msg):
    log(msg, level=LOGNOTICE)

def log_warning(msg):
    log(msg, level=LOGWARNING)

def log_error(msg):
    log(msg, level=LOGERROR)

def log(msg, level=LOGDEBUG):
    # override message level to force logging when addon logging turned on
    if addon.getSetting('addon_debug') == 'true' and level == LOGDEBUG:
        level = LOGNOTICE

    try:
        if isinstance(msg, unicode):
            msg = '%s (ENCODED)' % (msg.encode('utf-8'))

        xbmc.log('%s: %s' % (name, msg), level)
    except Exception as e:
        try: xbmc.log('Logging Failure: %s' % (e), level)
        except: pass  # just give up
