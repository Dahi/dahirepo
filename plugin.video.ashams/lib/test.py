﻿# This test program is for finding the correct Regular expressions on a page to insert into the plugin template.
# After you have entered the url between the url='here' - use ctrl-v
# Copy the info from the source html and put it between the match=re.compile('here')
# press F5 to run if match is blank close and try again.

import sys,os
import urllib,urllib2,re,requests,urlparse,time
##import base64
##print 'TDQo2IDAgb2JqDQo8PA0KL0xpbmVhcml6ZWQgMQ0KL0wgMTg2Ng0KL0ggWyA2NTUgMTI3IF0NCi9PIDgNCi9FIDEyMjkNCi9OIDENCi9UIDE2MjANCj4'
##data = base64.b64decode('TDQo2IDAgb2JqDQo8PA0KL0xpbmVhcml6ZWQgMQ0KL0wgMTg2Ng0KL0ggWyA2NTUgMTI3IF0NCi9PIDgNCi9FIDEyMjkNCi9OIDENCi9UIDE2MjANCj4')
##data2= data.decode('string-escape')
##data3=data2.replace('L4(','').replace('(\xf8','').replace('\\x','')
##print data3
####data4= '\xf0\xf04(\xbd1\xa5\xb9\x95\x85\xc9\xa5\xe9\x95\x90\x80\xc44(\xbd0\x80\xc4\xe0\xd8\\xd84'.decode('string-escape')
####print data4
##str2='7p7791A7665b3171g4212G6570T2110m5445C3252Z4212i2266o5595H6624d4244j1058k3174Y5310R7392I2204f2110U5285q1063O6378J4508N4236l2180V4256X2122S1064n2246W3399E7539e2106P2126c2112L6798M1056B3156D3267Q3174K7441h5275F7553'
##str3= str2.decode('string-escape')
##print str3
##string = 'TDQo2IDAgb2JqDQo8PA0KL0xpbmVhcml6ZWQgMQ0KL0wgMTg2Ng0KL0ggWyA2NTUgMTI3IF0NCi9PIDgNCi9FIDEyMjkNCi9OIDENCi9UIDE2MjANCj4+DQplbmRvYmoNCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICANCnhyZWYNCjYgNg0KMDAwMDAwMDAxNyAwMDAwMCBuDQowMDAwMDAwNTg2IDAwMDAwIG4NCjAwMDAwMDA3ODIgMDAwMDAgbg0KMDAwMDAwMDk2MyAwMDAwMCBuDQowMDAwMDAxMDQzIDAwMDAwIG4NCjAwMDAwMDA2NTUgMDAwMDAgbg0KdHJhaWxlcg0KPDwNCi9TaXplIDEyDQovUHJldiAxNjEwDQovSW5mbyA1IDAgUg0KL1Jvb3QgNyAwIFINCi9JRCBbPDZlZjdhODFiZGQ2NDYwMGFmNDQ5NmQ0MzMyMjA3ZmViPjw2ZWY3YTgxYmRkNjQ2MDBhZjQ0OTZkNDMzMjIwN2ZlYj5dDQo+Pg0Kc3RhcnR4cmVmDQowDQolJUVPRg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICANCjcgMCBvYmoNCjw8DQovVHlwZSAvQ2F0YWxvZw0KL1BhZ2VzIDEgMCBSDQovTmFtZXMgMiAwIFINCj4+DQplbmRvYmoNCjExIDAgb2JqDQo8PA0KL1MgMzYNCi9GaWx0ZXIgL0ZsYXRlRGVjb2RlDQovTGVuZ3RoIDM5DQo+Pg0Kc3RyZWFtDQp4nGNgYGBmYGDqZwACxv0M2AAHEpsZihkYAhjYTzEHMAAATukC1woNCmVuZHN0cmVhbQ0KZW5kb2JqDQo4IDAgb2JqDQo8PA0KL1R5cGUgL1BhZ2UNCi9Dcm9wQm94IFsgMCAwIDYxMiA3OTIgXQ0KL01lZGlhQm94IFsgMCAwIDYxMiA3OTIgXQ0KL1JvdGF0ZSAwDQovUmVzb3VyY2VzIDw8IC9FeHRHU3RhdGUgPDwgL0dTMCA5IDAgUiA+PiA+Pg0KL0NvbnRlbnRzIDEwIDAgUg0KL1BhcmVudCAxIDAgUg0KPj4NCmVuZG9iag0KOSAwIG9iag0KPDwNCi9CTSAvTm9ybWFsDQovQ0EgMQ0KL1NBIHRydWUNCi9UeXBlIC9FeHRHU3RhdGUNCi9jYSAxDQo+Pg0KZW5kb2JqDQoxMCAwIG9iag0KPDwNCi9GaWx0ZXIgL0ZsYXRlRGVjb2RlDQovTGVuZ3RoIDEwNA0KPj4NCnN0cmVhbQ0KeJwr5DJUMABCXRBlbmmkkJzLZWShYG5momdiaKgA5gBRDpephSmCAZPOQVabw5XBFa7FlQc0EQSL0rn03YMNFNKLuQz0zE0NzMxNwDbBORCTgfaBRM1NjBQsLYC6UrnSuAK5AM8iHgINCmVuZHN0cmVhbQ0KZW5kb2JqDQoxIDAgb2JqDQo8PA0KL1R5cGUgL1BhZ2VzDQovS2lkcyBbIDggMCBSIF0NCi9Db3VudCAxDQo+Pg0KZW5kb2JqDQoyIDAgb2JqDQo8PA0KL0phdmFTY3JpcHQgMyAwIFINCj4+DQplbmRvYmoNCjMgMCBvYmoNCjw8DQovTmFtZXMgWyAoZikgNCAwIFIgXQ0KPj4NCmVuZG9iag0KNCAwIG9iag0KPDwNCi9KUyAoYXBwLmFsZXJ0XCgnUGxlYXNlIHdhaXQuLidcKTspDQovUyAvSmF2YVNjcmlwdA0KPj4NCmVuZG9iag0KNSAwIG9iag0KPDwNCi9DcmVhdGlvbkRhdGUgKEQ6MjAxNjA3MjMyMzAzMTMrMDcnMDAnKQ0KL1Byb2R1Y2VyIChwb3B1bmRlcmpzLmNvbSkNCi9Nb2REYXRlIChEOjIwMTYwNzI0MDYxODI1KzAyJzAwJykNCj4+DQplbmRvYmoNCnhyZWYNCjAgNg0KMDAwMDAwMDAwMCA2NTUzNSBmDQowMDAwMDAxMjI5IDAwMDAwIG4NCjAwMDAwMDEyOTUgMDAwMDAgbg0KMDAwMDAwMTMzOSAwMDAwMCBuDQowMDAwMDAxMzg2IDAwMDAwIG4NCjAwMDAwMDE0ODIgMDAwMDAgbg0KdHJhaWxlcg0KPDwNCi9TaXplIDYNCi9JRCBbPDZlZjdhODFiZGQ2NDYwMGFmNDQ5NmQ0MzMyMjA3ZmViPjw2ZWY3YTgxYmRkNjQ2MDBhZjQ0OTZkNDMzMjIwN2ZlYj5dDQo+Pg0Kc3RhcnR4cmVmDQoxNzgNCiUlRU9GDQojbj'
##print base64.b64decode(string)
##st=string.split('+')
##for a in st:
##    print str(a)
##    print base64.b64decode(str(a))
##print st

##print base64.b64decode('TDQo2IDAgb2JqDQo8PA0KL0xpbmVhcml6ZWQgMQ0KL0wgMTg2Ng0KL0ggWyA2NTUgMTI3IF0NCi9PIDgNCi9FIDEyMjkNCi9OIDENCi9UIDE2MjANCj4', '-_')
##from resources.lib import cloudflare
##import old_cloudflare
##import execjs
##from xbmcstubs_master import xbmc,xbmcplugin,xbmcgui,xbmcaddon

##addon_id = 'plugin.video.bokra'
##addon = xbmcaddon.Addon(id=addon_id)
##bokrapath = addon.getAddonInfo('path')
##print bokrapath

##url='http://www.bokra.net/VideoCategory/45/Ù…Ù†ÙˆØ¹Ø§Øª_+.html'
##url='http://www.bokra.net/VideoCategory/45/Ù…Ù†ÙˆØ¹Ø§Øª_+.html'

##def CATEGORIES():
##url='http://www.ashams.com/new_media/553/%D8%A7%D9%81%D9%84%D8%A7%D9%85'
##url='http://www.ashams.com/new_media/1680/مسلسلات رمضان 2015'
##cookiefile='/home/hosam/Documents/XBMC/Ashams/plugin.video.ashams/abc.txt'
####print os.path.isfile(filecookie)
##LFW=open(cookiefile, 'w+')
##LFW.write('Hello')
##LFW.close()
##
##url='http://www.ashams.com'
##user_agent='Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3'
##scookie=cloudflare.request(url)
##print scookie
##cookie=old_cloudflare.get_cookie_string(url, user_agent)
####cookie=cloudflare.request(url)
##print cookie
##print '###################################################################'

def parseJSString(s):
    try:
        offset=1 if s[0]=='+' else 0
        val = int(eval(s.replace('!+[]','1').replace('!![]','1').replace('[]','0').replace('(','str(')[offset:]))
        return val
    except:
        pass



##url='http://www.ashams.com/new_media/554/%D8%A7%D9%81%D9%84%D8%A7%D9%85_%D8%B9%D8%B1%D8%A8%D9%8A%D8%A9'
##        addDir('MOSLSALAT','http://www.ashams.com/new_media/550/%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA',1,os.path.join(ashamspath,'resources', 'Categories','MSLSLAT.jpg'))
##        addDir('AFLAM','http://www.ashams.com/new_media/553/%D8%A7%D9%81%D9%84%D8%A7%D9%85',1,os.path.join(ashamspath,'resources', 'Categories','aflam.jpg'))
##        addDir('ATFAL','http://www.ashams.com/new_media/563/%D8%A7%D8%B7%D9%80%D9%80%D9%81%D8%A7%D9%84',1,os.path.join(ashamspath,'resources', 'Categories','aflam.jpg'))
##        addDir('MONAWA3AT','http://www.ashams.com/new_media/1156/%D9%85%D9%86%D9%88%D8%B9%D8%A7%D8%AA',1,os.path.join(ashamspath,'resources', 'Categories','mnw3at +.jpg'))
##        addDir('BARAMIG TELEFISIONYAH','http://www.ashams.com/new_media/1243/%D8%A8%D8%B1%D8%A7%D9%85%D8%AC%20%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86',2,os.path.join(ashamspath,'resources', 'Categories','bramg tlfzywn.jpg'))
    
##def SUBCATEGORIES(url):
##req = urllib2.Request(url)
####opener = urllib2.build_opener()
####opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')]
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##req.add_header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
##req.add_header('Accept-Language', 'en-US,en;q=0.5')
##req.add_header('Accept-Encoding', 'gzip, deflate')
##req.add_header('Referer', 'http://www.ashams.com/new_media/550/%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA')
##req.add_header('Cookie', '__cfduid=d74d6f95cbc50b290c830a20cc9b79b021446957085; cf_clearance=818cd6b683424f19639ca0c13124984e0461878d-1453160875-31536000; __auc=4436a70115257234da136c31c18; __asc=578727201525cbf397733fc9a38; PHPSESSID=db2s13jrteo1f1660lrnkrj0r0')
####response = opener.open(url)
##response = urllib2.urlopen(req)
####html_contents = response.read()
##link=response.read().replace("\n", "")
##response.close()
##print link




##host='http://www.ashams.com/'
##s = requests.session()
##
##response=s.request("GET",host)
##page=response.text
##cookie=s.cookies
##if ( "URL=/cdn-cgi/" in response.headers.get("Refresh", "") and response.headers.get("Server", "") == "cloudflare-nginx" ):
##            print "Cloudflare anti-bot is on"
####time.sleep(5)
##challenge = re.search(r'name="jschl_vc" value="(\w+)"', page).group(1)
##challenge_pass = re.search(r'name="pass" value="(.+?)"', page).group(1)
##print 'challenge: '+ challenge
##print 'challenge_pass: '+ challenge_pass
##
##init = re.search('setTimeout\(function\(\){\s*.*?.*:(.*?)};', page).group(1)
##builder = re.search(r"challenge-form\'\);\s*(.*)a.v", page).group(1)
##decryptVal = parseJSString(init)
##lines = builder.split(';')
##
##for line in lines:
##    if len(line)>0 and '=' in line:
##        sections=line.split('=')
##        line_val = parseJSString(sections[1])
##        decryptVal = int(eval(str(decryptVal)+sections[0][-1]+str(line_val)))
##answer = decryptVal + len(urlparse.urlparse(url).netloc)
##print 'answer: '+str(answer)
####time.sleep(5)
##headers = {'Referer':'http://www.ashams.com/'}
##params={'jschl_vc':challenge,'pass':challenge_pass,'jschl_answer':str(answer)}
##response=s.request("GET",'http://www.ashams.com/cdn-cgi/l/chk_jschl', params=params,headers=headers,cookies=cookie)
##cookie= s.cookies
##token=";".join(["%s=%s" % (k, v) for k, v in s.cookies.items()])
##print token
##cookie2= { "__cfduid": s.cookies.get("__cfduid", ""),"cf_clearance": s.cookies.get("cf_clearance", "")}
##cookie3 = s.cookies.get_dict()
##print cookie2
##print cookie3
##print dict(s.cookies)
##response.close()



    
##time.sleep(5)
##print fjs
##print ''
#answer=execjs.exec_(fjs)
##for k, v in response.headers.iteritems():
##    ##print k, v
##    if k == 'Set-Cookie': Cookies = v
##    if k == 'Refresh': redir = 'http://www.ashams.com/'+v[v.find('URL')+5:]
####print Cookies
####print redir
##
##token=";".join(["%s=%s" % (k, v) for k, v in s.cookies.items()])
##match=re.compile('<input type=\"hidden\" name="(.+?)\" value=\"(.+?)\"/>').findall(response.text)
##for k, v in match:
##    print "key: " +k+ " value: " +v

#redir = 'http://www.ashams.com/cdn-cgi/l/chk_jschl?jschl_vc='+challenge+'&pass='+challenge_pass+'&jschl_answer='+str(answer)
#print redir

####print s.cookies.get_dict()
##response.close()
##headers = {'Host':'ashams.com',
##           'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0',
##           'Cookie':token,
##           'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
##           'Accept-Language':'en-US,en;q=0.5',
##           'Accept-Encoding':'gzip, deflate',
##           ##'Referer':'http://www.ashams.com/',
##           ##'Cookie': '__cfduid=d74d6f95cbc50b290c830a20cc9b79b021446957085; cf_clearance=818cd6b683424f19639ca0c13124984e0461878d-1453160875-31536000; __auc=4436a70115257234da136c31c18; __asc=578727201525cbf397733fc9a38; PHPSESSID=db2s13jrteo1f1660lrnkrj0r0',
##           ##'Connection':'keep-alive',
##           ##'Cache-Control':'max-age=0'
##           }
####s = requests.session()
##response=s.request("GET",redir,headers=headers)
##print response.headers
##
##params={'jschl_vc':'98e859a6eb6c4fc40c3b565df05b2828',
##        'pass':'1453259934.931-CzWlT/19aI',
##        'jschl_answer':'-34530'}
##    
####print s.cookies
##token=";".join(["%s=%s" % (k, v) for k, v in s.cookies.items()])
####print token
##
##response=s.request("GET",url,headers=headers)
##link=response.text
####link=response.read()
##response.close()
####print link
##match=re.compile('<div class=\'videoscat\'><a href=\'(.+?)\'><img src=\'(.+?)\'><h2>(.+?)</h2></a></div>').findall(link)
##for url, name, thumb in match:
##    print "name: " +name+ " url: " +url+ " Thumb: " +thumb
##cookie=old_cloudflare.get_cookie_string(url)


##########################################################################################

####def SHOWS(url):
##url = 'http://www.ashams.com/videos/550-مسلسلات'
##url = 'http://www.ashams.com/videos/2044-مسلسلات-رمضان-2016'
##
####url = 'http://www.ashams.com/videos/2297-نيران-صيفية'
####
####url = 'http://www.ashams.com/videos/553-افلام'
####url = 'http://www.ashams.com/videos/554-افلام-عربية'
##
####url = 'http://www.ashams.com/videos/563-اطفال'
####url = 'http://www.ashams.com/videos/1243-برامج-تلفزيون'
##
####url = 'http://www.ashams.com/videos/2048-ميديا-الشمس-مشاهدة-مسلسل-مأمون-وشركاه-hd'
####url = 'http://www.ashams.com/videos/2048-ميديا-الشمس-مشاهدة-مسلسل-مأمون-وشركاه-hd?page=2'
##
####cookie='__cfduid=d0ebbfd4aca64608b227b5a3db93f85b71454215455; cf_clearance=3ba406f166331340787040557248bcc19f3780f4-1454215460-31536000'
####cookie="__cfduid="+ s.cookies.get("__cfduid", "")+"; cf_clearance="+s.cookies.get("cf_clearance", "")
####time.sleep(5)
####print cookie
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
####req.add_header('Cookie', cookie)
##req.add_header('Referer','http://www.ashams.com/')
##response = urllib2.urlopen(req)
##link=response.read().replace("\n", "").replace("\t", "")
####print link
##
####headers = {'Referer':'http://www.ashams.com/'}
####params={'jschl_vc':challenge,'pass':challenge_pass,'jschl_answer':str(answer)}
####response=s.request("GET",'http://www.ashams.com/cdn-cgi/l/chk_jschl', params=params,headers=headers,cookies=token)
####link =response.text
####cookie='__cfduid=dc459edf0dbdafcd58a85fda815f3dec51454119888; cf_clearance=8597f178f4e146cfac35b1150658482a8c56d766-1454119893-31536000; __auc=a063546a152904cbf60a6592db0; __asc=40ec591415293e901aafe269d33; PHPSESSID=q7gpdjcjr4aibjvpsohjr0t193'
####headers = {'Host':'www.ashams.com',
####           'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0',
####           ##'Cookie':token,
####           'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
####           'Accept-Language':'en-US,en;q=0.5',
####           'Accept-Encoding':'gzip, deflate',
####           'Referer':'http://www.ashams.com/',
####           'Cookie': cookie,
####           'Connection':'keep-alive'
####           ##'Cache-Control':'max-age=0'
####           }
######ss = requests.session()
####resp=s.request("GET",url,headers=headers)
####time.sleep(5)
####link =resp.text
##
##response.close()

#####################################################################################
##def SUBCATEGORIES(url):
##url = 'http://www.ashams.com/videos/550-%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA'
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##req.add_header('Referer','http://www.ashams.com/')
####req.add_header('Cookie', cookie)
##response = urllib2.urlopen(req)
##link=response.read().replace("\n", "")
####print link
##response.close()
##match=re.compile('<div class="item ias-item">.*?<a href="(.*?)"><figure style="background: url\((.*?)\).*?<p>(.*?)</p>').findall(link)
##for url, thumb, name in match:
##    url = re.sub('article/\d*','',url)
##    name=name.replace('ميديا ','').replace('الشمس','').replace('| ','').replace(' |','').replace(' | ','').replace('مشاهدة ','').replace('مسلسل ','')
##    print "name: " +name+ " url: http://www.ashams.com" +url+ " Thumb: " +thumb
    
############################################################################
####def SHOWS(url):
##url = 'http://www.ashams.com/videos/551-مسلسلات-عربية'
####url='http://www.ashams.com/new_media/922/%D8%AD%D8%B1%D9%8A%D9%85_%D8%A7%D9%84%D8%B3%D9%84%D8%B7%D8%A7%D9%86_%D8%A7%D9%84%D8%AC%D8%B2%D8%A1_%D8%A7%D9%84%D8%AA%D8%A7%D9%86%D9%8A'
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##req.add_header('Referer','http://www.ashams.com/')
####req.add_header('Cookie', cookie)
##response = urllib2.urlopen(req)
##link=response.read().replace("\n", "")
####print link
##response.close()
##match=re.compile('<div class="item ias-item">.*?<a href="(.*?)"><figure style="background: url\((.*?)\).*?<p>(.*?)</p>').findall(link)
####print match
##for url, thumb, name in match:
##    print "name: " +name+ " url: " +url+ " Thumb: " +thumb
##try:
##    pages=re.compile('<div class=\'paging_all\'(.+?)<div class=\'footer\'>').findall(link)
##    getpage=re.compile('<a href=\'(.+?)\'>(.+?)</a></div>').findall(pages[0])
##    for url, name in getpage:
##        print "Page: "+name+ " url: " +'http://www.ashams.com'+url
##except: pass    

##########################################################################
####def VOD(url):
##url = 'http://www.ashams.com/videos/2305-%D8%A7%D9%84%D8%B3%D8%A8%D8%B9-%D8%A8%D9%86%D8%A7%D8%AA'
####url='http://www.ashams.com/new_media/922/%D8%AD%D8%B1%D9%8A%D9%85_%D8%A7%D9%84%D8%B3%D9%84%D8%B7%D8%A7%D9%86_%D8%A7%D9%84%D8%AC%D8%B2%D8%A1_%D8%A7%D9%84%D8%AA%D8%A7%D9%86%D9%8A'
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##response = urllib2.urlopen(req)
##link=response.read().replace("\n", "")
####print link
##response.close()
##match=re.compile('<div class="ias-item item">.*?<a href="(.*?)"><figure style="background: url\((.*?)\).*?<p>(.*?)</p>').findall(link)
##for url2, thumb, name in match:
##    print "name: " +name+ " url: " +url2+ " Thumb: " +thumb
##url = re.sub('\?page=\d*','',url)
##print "name: Next" + " url: " +url
##try:
##    pages=re.compile('<div class=\'paging_all\'(.+?)<div class=\'footer\'>').findall(link)
##    getpage=re.compile('<a href=\'(.+?)\'>(.+?)</a></div>').findall(pages[0])
##    for url, name in getpage:
##        print "Page: "+name+ " url: " +'http://www.ashams.com'+url
##except: pass    
############################################################################
##url='http://www.ashams.com/video/266253-ولاد-تسعة--الحلقة-30'
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##response = urllib2.urlopen(req)
##link=response.read().replace("\n", "")
####print link
##response.close()
##if link.find("https://openload.co/embed") > -1:
##    match=re.compile('https://openload.co/embed/(.+?)"').findall(link)
##    vidlink = 'https://openload.co/embed/'+match[0]
##    print vidlink



#############################################################################
##url='https://openload.co/stream/MVkPwAr9a4Q~1488339145~67.216.0.0~rXruQ0UT?mime=true'
##s = requests.session()
##params={':authority':'openload.co',':path':'stream/MVkPwAr9a4Q~1488339145~67.216.0.0~rXruQ0UT?mime=true',':scheme':'https'}
##headers={'user-agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 OPR/43.0.2442.991','x-requested-with':'ShockwaveFlash/24.0.0.221'}
##response=s.request("GET",url, params=params, headers=headers)
####link =response.text
####print link
##response.close()


import urlresolver
url ='https://openload.co/embed/X-MRFHXa_x8/'
stream_url = urlresolver.resolve(url)
print 'stream_url: '+str(stream_url)
##stream_url = urlresolver.HostedMediaFile(url=vidlink).resolve ()

s = requests.session()
params={':authority':'openload.co',':path':'/embed/X-MRFHXa_x8/',':scheme':'https'}
response=s.request("GET",url, params=params)
link =response.text
##print link
match1=re.compile('shouldreport="(.+?)"').findall(link)
match2=re.compile('<div style="display:none;">\W<span id="(.+?)"').findall(link)
match3=re.compile('<meta name="description" content="(.+?)"').findall(link)
##print 'https://1fhjlpc.oloadcdn.net/dl/l/'+match2[0]+'/'+match1[0]+'/'+match3[0]
response.close()
#############################################################################

##match=re.compile('png\'><a itemprop="url" href=\'(.+?)\'><img itemprop="image" src=\'(.+?)\'><h2 itemprop="name">(.+?)</h2></a>').findall(link)
####print match
##for url, thumb, name in match:
##    print "name: " +name+ " url: " +url+" thumb: "+thumb
####try:
##    pages=re.compile('<div class=\'paging_all\'(.+?)<div class=\'footer\'>').findall(link)
##    getpage=re.compile('<a href=\'(.+?)\'>(.+?)</a></div>').findall(pages[0])
##    for url, name in getpage:
##        print "Page: " +name+ " url: " +url
##except: pass



##url = 'http://www.ashams.com/video/262642-%D8%A7%D9%84%D8%AF%D9%88%D8%A8%D9%84%D9%83%D8%B3-4--%D8%A7%D9%84%D8%AD%D9%84%D9%82%D8%A9-3-hd'
##url = 'http://www.ashams.com/video/262035-نيران-صيفية--الحلقة-5-hd'
##url = 'http://www.ashams.com/video/257999-جود--الحلقة-29-hd'
##url = 'http://www.ashams.com/video/262588-عطر-الامس--الحلقة-25-hd'
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##response = urllib2.urlopen(req)
##link=response.read().replace("\n", "")
##response.close()
####print link
##
##if link.find("data-video-id") > -1:
##    print '11111111111111111111111'
##    vidlink=re.compile('data-video-id="(.+?)"').findall(link)
##    configreq2 = urllib2.Request('http://cdn.playwire.com/v2/15880/related/'+vidlink[0]+'.json')
##    configresponse2 = urllib2.urlopen(configreq2)
##    config2=configresponse2.read()
##    configresponse2.close()
##    vidlink2=re.compile('mp4:video-(.+?)-').findall(config2)
##    vidlink3='http://cdn.playwire.com/15880/video-'+vidlink2[0]+'-'+vidlink[0]+'.mp4'
##    ##vidthumb=re.compile('poster":"http://cdn.playwire.com/(.+?)"},').findall(config2)
##    ##vidthumb2='http://cdn.playwire.com/'+vidthumb[0]
##    ##playVid(vidlink3,name)
##    print vidlink3
##elif link.find("script data-config") > -1:
##    print '2222222222222222222222222'
##    vidlink=re.compile('script data-config="(.+?)"').findall(link)
##    link="http:"+vidlink[0].replace("http:","").replace("zeus.json","manifest.f4m").replace("player.json","manifest.f4m")
##    print link
##    configreq = urllib2.Request(link)
##    configresponse = urllib2.urlopen(configreq)
##    config=configresponse.read()
##    configresponse.close()
##    vidlink2=re.compile('<baseURL>(.+?)</baseURL>').findall(config)
##    vidlink3 = vidlink2[0]+"/video-sd.mp4"
##    print vidlink3
##elif link.find("m3u8") > -1:
##    print '333333333333333333333333333333'
##    vidlink=re.compile('<div class=\'videophone\'><a href=\'(.+?)\'>').findall(link)
##    print vidlink
##
##elif link.find("www.youtube.com/watch") > -1:
##    print '44444444444444444444444444'
##    match=re.compile('http://www.youtube.com/watch?(.+?)[\'|"<]').findall(link)
##    vidlink="http://www.youtube.com/watch?"+match[0].replace(" ", "")
##    print "YouTube url "+vidlink
##    ##import urlresolver            
##    ##stream_url = urlresolver.resolve(vidlink)
##    ##playVid(stream_url,name)
##
##elif link.find("div class='newplayer'>") > -1:
##    print '55555555555555555555555555'
##    match=re.compile('<div class=\'newplayer\'><a href="(.+?)"').findall(link)
##    print "redirect url: "+match[0]
##    req = urllib2.Request(match[0])
##    req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##    response = urllib2.urlopen(req)
##    link1=response.read().replace("\n", "")
##    response.close()
##    ##print link1
##    match1=re.compile('<source type="video/mp4" src="(.+?)"').findall(link1)
##    vidlink=match1[0].replace(' ','%20')
##    print "Video url: "+vidlink
##    
##elif link.find("<div class=\"main") > -1:
##    print '666666666666666666666666'
##    match=re.compile('<div class="main".+?src="(.+?)"').findall(link)
##    if match[0].find ('youtube') > -1:
##        print "YouTube url "+match[0]
##    elif match[0].find ('ashams') > -1:
##        print match[0]
##        req = urllib2.Request(match[0])
##        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##        req.add_header('Referer','http://www.ashams.com/')
##        response = urllib2.urlopen(req)
##        link1=response.read().replace("\n", "")
##        response.close()
##        print link1
##        match1=re.compile('<source src="(.+?)"').findall(link1)
##        vidlink=match1[0].replace(' ','%20')
##        print "Video url: "+vidlink
##    else:
##        "None"
##    
##else:
##    print "Nothing"


#############################################################################

##        match=re.compile('<div class=" subcat">.+?<a href="(.+?)" title="(.+?)" >').findall(link)
##        for url, name in match:
##url='http://www.bokra.net/Videos/%D8%A7%D9%81%D9%84%D8%A7%D9%85_%D9%88%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA.html'
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##response = urllib2.urlopen(req)
##link=response.read().replace("\n", "")
##response.close()
##block=re.compile('<div class=\'videoscat\'>(.+?)<div class="sub_menu">').findall(link)
##match=re.compile('<div class="button"> <a href="(.+?)".+?\);">(.+?)</a></div>').findall(block[0])
##for url, name in match:
##    mod=re.compile('VideoCategory/(.+?)/').findall(url)
##    mod=str(mod).replace("['", "").replace("']", "")
##    if mod=='42' or mod=='43' or mod=='118':
##        ##addDir(ARABIC(name),url,1,os.path.join(bokrapath,'resources', 'Categories',ARABIC(name)+'.jpg'))
##        print "name: " +name+ " url: " +url
##
##    elif mod=='44' or mod=='45':
##        ##addDir(ARABIC(name),url,2,os.path.join(bokrapath,'resources', 'Categories',ARABIC(name)+'.jpg'))
##        print "name: " +name+ " url: " +url
##
##    elif mod=='39':
##        ##addDir(ARABIC(name),url,3,os.path.join(bokrapath,'resources', 'Categories',ARABIC(name)+'.jpg'))
##        print "name: " +name+ " url: " +url


##def SHOWS(url, thumb):
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##response = urllib2.urlopen(req)
##link=response.read().replace("\n", "")
##response.close()
##getcont1=re.compile('<div class=\"left(.+?)<div class="sitemap"').findall(link)
##getcont=re.compile('<div class="pic"><a href="(.+?)".+?<img .+?="http:(.+?)".+?<div class="title">(.+?)</div>').findall(getcont1[0])
###print getcont1[0]
##getpages=re.compile('<a class="number" href="([./\w]+?)">()(\d+?)</a> ').findall(getcont1[0])
##match=getcont+getpages
##for url, thumb, name in match:
##    if re.compile('http.+?(VideoAlbum).+?.html').findall(url)==['VideoAlbum']:
##        nom=str(name)
##        if nom.find("a href=") > 0:
##            nom2=re.compile('title=".+?">(.+?)</a>').findall(nom)
##            name=nom2[0]
##        ##addDir(ARABIC(name),url,3,'http:'+thumb)
##        print "name: " +name+ " url: " +url
##
##    elif re.compile('http.+?(Videos).+?.html').findall(url)==['Videos']:
##        ##addDir(ARABIC(name),url,4,'http:'+thumb)
##        print "name: " +name+ " url: " +url
##
##    else:
##        ##addDir('Page '+name,'http://www.bokra.net'+url,3,os.path.join(bokrapath,'resources', 'Pages',name+'.jpg'))
##        print "name: " +name+ " url: " +url
