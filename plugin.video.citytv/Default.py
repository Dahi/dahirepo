import sys,os
import httplib,urllib,urllib2,re
import HTMLParser
import pyamf
from pyamf import remoting, amf3, util

import xbmc, xbmcplugin, xbmcgui, xbmcaddon

#TV DASH - by You 2008.

addon_id = 'plugin.video.citytv'
addon = xbmcaddon.Addon('plugin.video.citytv')
citypath = addon.getAddonInfo('path')

def get_episode_info(key, content_id, url, exp_id):
    conn = httplib.HTTPSConnection("secure.brightcove.com")
    envelope = build_amf_request(key, content_id, url, exp_id)
    headers = {"Content-type": "application/x-amf",
               "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
               "Accept-Encoding": "deflate",
               "Accept-Language": "en-US,en;q=0.5",
               "Connection": "keep-alive",
               "Host": "secure.brightcove.com",
               "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0"}
    conn.request("POST", "/services/messagebroker/amf?playerKey="+key, str(remoting.encode(envelope).read()),headers)
    response = conn.getresponse().read()
    response = remoting.decode(response).bodies[0][1].body
    return response

class ViewerExperienceRequest(object):
    def __init__(self, URL, contentOverrides, experienceId, playerKey, TTLToken=''):
        self.contentOverrides = contentOverrides
        self.TTLToken = TTLToken
        self.URL = URL
        self.deliveryType = float(0)
        self.experienceId = experienceId
        self.playerKey = playerKey

class ContentOverride(object):
    def __init__(self, contentId, contentType=0, target='videoPlayer'):
        self.contentType = contentType
        self.contentId = contentId
        self.target = target
        self.contentIds = None
        self.contentRefId = None
        self.contentRefIds = None
        self.contentType = 0
        self.featureId = float(0)
        self.featuredRefId = None

def build_amf_request(key, content_id, url, exp_id):
    print 'ContentId:'+content_id
    print 'ExperienceId:'+exp_id
    print 'URL:'+url
    const = '3a2590933426add549078ef16b2f1df14786523b'
    pyamf.register_class(ViewerExperienceRequest, 'com.brightcove.experience.ViewerExperienceRequest')
    pyamf.register_class(ContentOverride, 'com.brightcove.experience.ContentOverride')
    content_override = ContentOverride(int(content_id))
    viewer_exp_req = ViewerExperienceRequest(url, [content_override], int(exp_id), key)
    
    env = remoting.Envelope(amfVersion=3)
    env.bodies.append(
       (
          "/1",
          remoting.Request(
             target="com.brightcove.experience.ExperienceRuntimeFacade.getDataForExperience",
             body=[const, viewer_exp_req],
             envelope=env
          )
       )
    )
    return env


def CATEGORIES():
        addDir('Latest Videos','http://www.mancity.com/CityTV/Listing/?team=all-teams&mood=anything&duration=all-the-time&sort=latest',1,os.path.join(citypath,'resources', 'Categories','Latest Videos.jpg'),"")
        addDir('Match Highlights','http://www.mancity.com/CityTV/Listing/?team=all-teams&mood=highlights&duration=all-the-time&sort=latest',1,os.path.join(citypath,'resources', 'Categories','Match Highlights.jpg'),"")
        ##addDir('City Today','https://www.mcfc.com/CityTV/City-Today',1,os.path.join(citypath,'resources', 'Categories','City Today.jpg'),"")
        addDir('Interviews','http://www.mancity.com/citytv/listing?team=all-teams&mood=reactions&duration=all-the-time&sort=latest',1,os.path.join(citypath,'resources', 'Categories','Interviews.jpg'),"")
        addDir('Tunnel Cam','http://www.mancity.com/citytv/listing?team=all-teams&mood=tunnel-cam&duration=all-the-time&sort=latest',1,os.path.join(citypath,'resources', 'Categories','Tunnel Cam.jpg'),"")
        addDir('Inside City','http://www.mancity.com/citytv/listing?team=all-teams&mood=inside-city&duration=all-the-time&sort=latest',1,os.path.join(citypath,'resources', 'Categories','Features.jpg'),"")
        addDir('Goals','http://www.mancity.com/citytv/listing?team=all-teams&mood=great-goals&duration=all-the-time&sort=latest',1,os.path.join(citypath,'resources', 'Categories','City Square.jpg'),"")

def VIDEOLIST(url):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read()
        response.close()
        match=re.compile('<section class=\"listing--list-item\".+?\"date\":\" (.+?)\",\"title\":\"(.+?)\".+?category-heading\">(.+?)</h2>.+?<a href=(.+?)>.+?<picture>.+?src=\"(.+?)\"').findall(link.replace("\n", "").replace("\t", ""))
        ##for url,thumb,name,des in match:
        for date,name,cat,url2,thumb in match:
            name=HTMLParser.HTMLParser().unescape(name)
            ##thumb= thumb.replace("https://d297csy1wc3uin.cloudfront.net","http://content.mcfc.co.uk")
            ##desc=re.compile('<span.*?>(.+?)</span>').findall(des)
            ##for i in range(0, len(desc)):
            ##    desc[i]=desc[i].strip()
            ##descr='\n'.join(desc)
            addDir(name,'http://www.mancity.com'+url2,2,'http://www.mancity.com'+thumb,cat+'\n'+date)
        if "page=" in url: 
            pg=url.split('page=')
            page=int(pg[1])+1
            addDir('Next Page',pg[0]+'page='+str(page),1,os.path.join(citypath,'resources', 'Categories', 'Next.jpg'),"")
        else:
            addDir('Next Page',url+'&page=1',1,os.path.join(citypath,'resources', 'Categories', 'Next.jpg'),"")
        ##pagination=re.compile('Video.*?List_Pager_PageLinks.+?<a href="(.+?)".*?>(.+?)</a></li>').findall(link)
        ##for url,name in pagination:
            ##addDir(('Page '+name).replace("Page Next","Next"),'https://www.mcfc.com'+url,1,os.path.join(citypath,'resources', 'Pages',name+'.jpg'),"")

def SOURCES(url, name, thumb):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##        req.add_header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
##        req.add_header('Accept-Language', 'en-US,en;q=0.5')
##        req.add_header('Accept-Encoding', 'gzip, deflate')
##        req.add_header('Connection', 'keep-alive')
        response = urllib2.urlopen(req)
        link = response.read()
        response.close()
##        match=re.compile('<div id="content-main-feature">.+?playerID=(.+?)&amp;playerKey=(.+?)&amp;.+?%40videoPlayer=(.+?)&amp;.+?"/>').findall(link.replace("\n", "").replace("\t", ""))
##        for exp_id,key,content_id in match:
##            print "playerID: "+exp_id+"/// playerKey: "+key+"/// videoPlayer: "+content_id
##        contid_1= content_id[:5]
##        swfUrl = 'https://sadmin.brightcove.com/viewer/us20150903.1327/federatedVideoUI/BrightcovePlayer.swf?uid=1450316298186'
##        pub_Id = '10228001001'
##        renditions = get_episode_info(key, content_id, url, exp_id)['programmedContent']['videoPlayer']['mediaDTO']['renditions']
##        bitrate='5'
##        
##        if str(renditions).find('mcfc-f')>0:
##            rend=re.compile('\[{u*\'videoCodec\': u*\'H264\', u*\'defaultURL\': u*\'(.+?)'+pub_Id+'/(.+?)/.+?_.+?_(.+?)\', u*\'encodingRate').findall(str(renditions))
##            host = rend[0][0]+pub_Id+'/'+rend[0][1]+'/'
##            vid = rend[0][2]
##            url2=''
##            match=re.compile('defaultURL\': u\''+host+'(.+?)001_'+vid+'.+?frameWidth\': (.+?),').findall(str(renditions))
##            match= sorted(match)
##            for url1,framewidth in match:
##                url2 = url2+','+url1
##                if framewidth <= '640':
##                    bitrate = '2'
##            data = '.csmil/bitrate='+bitrate+'?videoId='+content_id+'&lineUpId=&pubId='+pub_Id+'&playerId='+exp_id+'&affiliateId=&bandwidthEstimationTest=true&v=3.4.0&fp=WIN%2019,0,0,226&r=AKODB&g=PRECNOZSPCYM'
##            addr=host+url2+',001_'+vid+data
##        elif str(renditions).find('mancityhdhd')>0:
##            vidid2=''
##            rend=re.compile('{u*\'videoCodec\': u*\'H264\', u*\'defaultURL\': u*\'(.+?)_(.+?)_(.+?)\', u*\'encodingRate.+?frameWidth\': (.+?),').findall(str(renditions))
##            for base,vidid, file,framewidth in rend:
##                vidid1=vidid[:3]
##                vidid2=vidid2+','+vidid[3:-3]
##                vidid3=vidid[-3:]
##                if framewidth <= '640':
##                    bitrate = '2'
##                
##            url=base+'_'+vidid1+vidid2+','+vidid3+'_'+file
##            data = '.csmil/bitrate='+bitrate+'?videoId='+content_id+'&lineUpId=&pubId='+pub_Id+'&playerId='+exp_id+'&v=3.4.0&fp=WIN%2019,0,0,226&r=AKODB&g=PRECNOZSPCYM'
##            addr=url+data
##        else:
##            print 'Unknown AMF3 Format'

        match = re.compile('<article class=\"video-player--video">.+?data-account=\"(.+?)\".+?data-video-id=\"(.+?)\"').findall(link.replace("\n", "").replace("\t", ""))
        for acct, vid in match:
            print "Account: "+acct+" / Video ID: "+vid
            vidurl= "https://edge.api.brightcove.com/playback/v1/accounts/"+acct+"/videos/"+vid
            req = urllib2.Request(vidurl)
            req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
            req.add_header('Accept', 'application/json;pk=BCpkADawqM21j-KlK-ysjQFYLzbv4OSE3pRClfc3OazECrpmd2iBwdO3BhkFWxKKhtnkLCTDOwd8Qd99ySTpoUqcLTOnhwiELhirubv3zvTiDZFc7CEaCxYft90')
            response = urllib2.urlopen(req)
            link = response.read()
            match = re.compile('avg_bitrate\":(.+?),\"width\":(.+?),\"[streaming_]*src\":\"(.+?)\".+?height\":(.+?),\"duration":(.+?),').findall(link.replace("\n", "").replace("\t", ""))
            for bitr, wi, vidlink, hi, dur in match:
                source=re.compile('htt(?:ps|p)://(.+?)\.').findall(vidlink)
                src=str(source[0])
                vidurl=vidlink.replace('http://mcfc-f','https://mcfcuds-a')
                addDir(src,vidurl,3,thumb,'Bitrate: '+bitr+'\nResolution: '+wi+'x'+hi+'\nDuration: '+dur)

def playVid(url,name):
        ok=True
        listitem = xbmcgui.ListItem(name)
        xbmc.Player().play(url, listitem)
        return ok
             
def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param

def addDir(name,url,mode,iconimage,desc):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)+"&desc="+urllib.quote_plus(desc)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ 'title': name, 'plot':desc, 'plotoutline':desc } )
        liz.setProperty('fanart_image',iconimage)
        xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        return ok
        
              
params=get_params()
url=None
name=None
mode=None
thumb=None
desc=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass
try:
        thumb=urllib.unquote_plus(params["thumb"])
except:
        pass
try:
        desc=urllib.unquote_plus(params["desc"])
except:
        pass
    
print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "Thumb: "+str(thumb)
print "Description: "+str(desc)

if mode==None or url==None or len(url)<1:
        print ""
        CATEGORIES()
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==1:
        print ""+url
        VIDEOLIST(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
                   
elif mode==2:
        print ""+url
        SOURCES(url,name,thumb)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==3:
        print ""+url
        playVid(url,name)
