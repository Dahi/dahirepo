import sys,os
import httplib,urllib,urllib2,re
import HTMLParser
import pyamf
from pyamf import remoting, amf3, util

def get_episode_info(key, content_id, url, exp_id):
    conn = httplib.HTTPSConnection("secure.brightcove.com")
    envelope = build_amf_request(key, content_id, url, exp_id)
    headers = {"Content-type": "application/x-amf",
               "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
               "Accept-Encoding": "deflate",
               "Accept-Language": "en-US,en;q=0.5",
               "Connection": "keep-alive",
               "Host": "secure.brightcove.com",
               "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0"}
    conn.request("POST", "/services/messagebroker/amf?playerKey="+key, str(remoting.encode(envelope).read()),headers)
    response = conn.getresponse().read()
    response = remoting.decode(response).bodies[0][1].body
    return response

class ViewerExperienceRequest(object):
    def __init__(self, URL, contentOverrides, experienceId, playerKey, TTLToken=''):
        self.contentOverrides = contentOverrides
        self.TTLToken = TTLToken
        self.URL = URL
        self.deliveryType = float(0)
        self.experienceId = experienceId
        self.playerKey = playerKey

class ContentOverride(object):
    def __init__(self, contentId, contentType=0, target='videoPlayer'):
        self.contentType = contentType
        self.contentId = contentId
        self.target = target
        self.contentIds = None
        self.contentRefId = None
        self.contentRefIds = None
        self.contentType = 0
        self.featureId = float(0)
        self.featuredRefId = None

def build_amf_request(key, content_id, url, exp_id):
    print 'ContentId:'+content_id
    print 'ExperienceId:'+exp_id
    print 'URL:'+url
    const = '3a2590933426add549078ef16b2f1df14786523b'
    pyamf.register_class(ViewerExperienceRequest, 'com.brightcove.experience.ViewerExperienceRequest')
    pyamf.register_class(ContentOverride, 'com.brightcove.experience.ContentOverride')
    content_override = ContentOverride(int(content_id))
    viewer_exp_req = ViewerExperienceRequest(url, [content_override], int(exp_id), key)
    
    env = remoting.Envelope(amfVersion=3)
    env.bodies.append(
       (
          "/1",
          remoting.Request(
             target="com.brightcove.experience.ExperienceRuntimeFacade.getDataForExperience",
             body=[const, viewer_exp_req],
             envelope=env
          )
       )
    )
    return env

##url='https://www.mcfc.com/CityTV/Match-highlights'
####url='https://www.mcfc.com/CityTV/Interviews/2015/December/Stoke-v-City-Manuel-Pellegrini-press-conference-part-two?play=1'




##-----------------------------------------------------------------------------------------------------------------

##url='http://www.mancity.com/CityTV/Listing/?team=all-teams&mood=anything&duration=all-the-time&sort=latest'
####if "page=" in url: 
####    pg=url.split('page=')
####    page=int(pg[1])+1
####    print pg[0]+'page='+str(page)
##
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##response = urllib2.urlopen(req)
##link=response.read()
##response.close()
####print link
####match=re.compile('Video.*?List_*?Items.+?href="(.+?)".+?img src="(.+?)".+?<span class="span-h5">(.+?)</span>(.+?)</span></a></li>').findall(link.replace("\n", "").replace("\t", ""))
##match=re.compile('<section class=\"listing--list-item\".+?\"date\":\" (.+?)\",\"title\":\"(.+?)\".+?category-heading\">(.+?)</h2>.+?<a href=(.+?)>.+?<picture>.+?src=\"(.+?)\"').findall(link.replace("\n", "").replace("\t", ""))
######print match
##for date,name,cat,url,thumb in match:
##    name=HTMLParser.HTMLParser().unescape(name)
##    print "URL: https://www.mancity.com"+url+"\nTHUMB: https://www.mancity.com"+thumb+"\nNAME: "+name+"\nDESC: "+cat+'\n'+date
##-----------------------------------------------------------------------------------------------------------------

    
####    pic = thumb.split("~")
####    thumb="http://content.mcfc.co.uk/"+"~"+pic[1]
##    thumb= thumb.replace("https://d297csy1wc3uin.cloudfront.net","http://content.mcfc.co.uk")
##    print "URL: https://www.mancity.com"+url+"; THUMB: "+thumb[:thumb.index('ashx')]+'jpg'+"; NAME: "+name
##    desc=re.compile('<span.*?>(.+?)</span>').findall(des)
##    for i in range(0, len(desc)):
##        desc[i]=desc[i].strip()
##    descr='\n'.join(desc)
##    print "Desc: "+ descr+'\n'
##pagination=re.compile('Video.*?List_Pager_PageLinks.+?<a href="(.+?)".*?>(.+?)</a></li>').findall(link)
##for url,name in pagination:
##    print ('Page '+name).replace("Page Next","Next")
##    print "URL: https://www.mcfc.com"+url+"; NAME: "+name

##url='https://www.mcfc.com/CityTV/Features/2015/December/Inside-City-Episode-176?play=1'
##url='https://www.mcfc.com/CityTV/Interviews/2015/December/Manuel-Pellegrini-season-so-far-CityTV-interview-part-2?play=1'
##url='https://www.mcfc.com/CityTV/Interviews/2015/December/Arsenal-v-City-Yaya-Toure-reaction?play=1'
##url='https://www.mcfc.com/CityTV/Interviews/2015/December/Manuel-Pellegrini-season-so-far-CityTV-interview-part-one?play=1'
##url='https://www.mcfc.com/CityTV/Interviews/2015/December/Stoke-v-City-Manuel-Pellegrini-press-conference-part-two?play=1'
##url='https://www.mcfc.com/CityTV/Features/2016/GOTD-4-June-Kinky-1995?play=1'
##url='https://www.mcfc.com/CityTV/Interviews/2016/June/Khaldoon-Al-Mubarak-MCFC-end-of-season-interview-June-2016-part-one?play=1'


##-----------------------------------------------------------------------------------------------------------------
url='https://www.mancity.com/CityTV/Goals/2016/09/05/goal-of-the-day-balotelli-v-united-2011/1473073286695'
##url='https://edge.api.brightcove.com/playback/v1/accounts/10228001001/videos/4920517626001'
req = urllib2.Request(url)
req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##req.add_header('Accept', 'application/json;pk=BCpkADawqM21j-KlK-ysjQFYLzbv4OSE3pRClfc3OazECrpmd2iBwdO3BhkFWxKKhtnkLCTDOwd8Qd99ySTpoUqcLTOnhwiELhirubv3zvTiDZFc7CEaCxYft90')
##req.add_header('Accept-Language', 'en-US,en;q=0.5')
##req.add_header('Accept-Encoding', 'gzip, deflate')
##req.add_header('Connection', 'keep-alive')
response = urllib2.urlopen(req)
link = response.read()
##print link
response.close()
match = re.compile('<article class=\"video-player--video">.+?data-account=\"(.+?)\".+?data-video-id=\"(.+?)\"').findall(link.replace("\n", "").replace("\t", ""))
for acct, vid in match:
    vidurl= "https://edge.api.brightcove.com/playback/v1/accounts/"+acct+"/videos/"+vid
    print vidurl
##url='https://edge.api.brightcove.com/playback/v1/accounts/10228001001/videos/4920517626001'
req = urllib2.Request(vidurl)
req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
req.add_header('Accept', 'application/json;pk=BCpkADawqM21j-KlK-ysjQFYLzbv4OSE3pRClfc3OazECrpmd2iBwdO3BhkFWxKKhtnkLCTDOwd8Qd99ySTpoUqcLTOnhwiELhirubv3zvTiDZFc7CEaCxYft90')
response = urllib2.urlopen(req)
link = response.read()
##print link
match = re.compile('avg_bitrate\":(.+?),\"width\":(.+?),\"[streaming_]*src\":\"(.+?)\".+?height\":(.+?),\"duration":(.+?),').findall(link.replace("\n", "").replace("\t", ""))
##print match
for bitr, wi, vidlink, hi, dur in match:
    source=re.compile('htt(?:ps|p)://(.+?)\.').findall(vidlink)
    vidurl=vidlink.replace('http://mcfc-f','https://mcfcuds-a')
    print "Source: "+source[0]+' Bitrate:'+bitr+" ("+wi+"x"+hi+") / DURATION: "+dur+" VIDEO URL: "+vidurl
##-----------------------------------------------------------------------------------------------------------------


##match=re.compile('<div id="content-main-feature">.+?playerID=(.+?)&amp;playerKey=(.+?)&amp;.+?%40videoPlayer=(.+?)&amp;.+?"/>').findall(link.replace("\n", "").replace("\t", ""))
##for exp_id,key,content_id in match:
##    print "playerID: "+exp_id+"/// playerKey: "+key+"/// videoPlayer: "+content_id
##
##
##swfUrl = 'https://sadmin.brightcove.com/viewer/us20150903.1327/federatedVideoUI/BrightcovePlayer.swf?uid=1450316298186'
####playerKey
##key = 'AQ~~%2CAAAAAmGi6Ok~%2C1XzRvnR-bI_QrK1bSooWJI_xYcmcBQlM'
####@videoPlayer
####content_id = '4652330715001'
##contid_1= content_id[:5]
####playerID
##exp_id = '2857356479001'
##pub_Id = '10228001001'
##renditions = get_episode_info(key, content_id, url, exp_id)['programmedContent']['videoPlayer']['mediaDTO']['renditions']
####print 'RENDITIONS: '+str(renditions)
##bitrate='5'
##if str(renditions).find('mcfc-f')>0:
##print match
##    match= sorted(match)
##    ##print match
##    for url1,framewidth in match:
##        url2 = url2+','+url1
##        if framewidth <= '640':
##            bitrate = '2'
##    data = '.csmil/bitrate='+bitrate+'?videoId='+content_id+'&lineUpId=&pubId='+pub_Id+'&playerId='+exp_id+'&affiliateId=&bandwidthEstimationTest=true&v=3.4.0&fp=WIN%2019,0,0,226&r=AKODB&g=PRECNOZSPCYM'
##    print host+url2+',001_'+vid+data
##elif str(renditions).find('mancityhdhd')>0:
##    vidid2=''
##    vidid22=''
##    vidid23=''
##    url2=''
##    url3=''
##    ##print str(renditions)
##    rend=re.compile('{u*\'videoCodec\': u*\'H264\', u*\'defaultURL\': u*\'(.+?)_(.+?)_(.+?)\', u*\'encodingRate.+?frameWidth\': (.+?),').findall(str(renditions))
##    for base,vidid, file,framewidth in rend:
##        vidid1=vidid[:5]
##        vidid12=vidid[:4]
##        vidid13=vidid[:3]
##        ##print vidid1
##        vidid2=vidid2+','+vidid[5:-3]
##        vidid22=vidid22+','+vidid[4:-3]
##        vidid23=vidid23+','+vidid[3:-3]
##        ##print vidid2
##        vidid3=vidid[-3:]
##        ##print vidid3
##        if framewidth <= '640':
##            bitrate = '2'
##        
##    ##print bitrate
##    ##print base
##    ##print file
##    url=base+'_'+vidid1+vidid2+','+vidid3+'_'+file
##    url2=base+'_'+vidid12+vidid22+','+vidid3+'_'+file
##    url3=base+'_'+vidid13+vidid23+','+vidid3+'_'+file
##    data = '.csmil/bitrate='+bitrate+'?videoId='+content_id+'&lineUpId=&pubId='+pub_Id+'&playerId='+exp_id+'&v=3.4.0&fp=WIN%2019,0,0,226&r=AKODB&g=PRECNOZSPCYM'
##    url=url+data
##    url2=url2+data
##    url3=url3+data
##    print url
##    print url2
##    print url3
##else:
##    print 'Unknown AMF3 Format'
####print host+url2+',001_'+vid+data
