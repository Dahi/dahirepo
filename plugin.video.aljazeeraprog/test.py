﻿import sys,os
import httplib,urllib,urllib2,re,requests
from datetime import date
import pyamf
from pyamf import remoting, amf3, util

def get_episode_info(key, content_id, url, exp_id):
    conn = httplib.HTTPSConnection("secure.brightcove.com")
    envelope = build_amf_request(key, content_id, url, exp_id)
    headers = {"Content-type": "application/x-amf",
               "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
               "Accept-Encoding": "deflate",
               "Accept-Language": "en-US,en;q=0.5",
               "Connection": "keep-alive",
               "Host": "secure.brightcove.com",
               "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0"}
    conn.request("POST", "/services/messagebroker/amf?playerKey="+key, str(remoting.encode(envelope).read()),headers)
    response = conn.getresponse().read()
    response = remoting.decode(response).bodies[0][1].body
    return response

class ViewerExperienceRequest(object):
    def __init__(self, URL, contentOverrides, experienceId, playerKey, TTLToken=''):
        self.contentOverrides = contentOverrides
        self.TTLToken = TTLToken
        self.URL = URL
        self.deliveryType = float(0)
        self.experienceId = experienceId
        self.playerKey = playerKey

class ContentOverride(object):
    def __init__(self, contentId, contentType=0, target='videoPlayer'):
        self.contentType = contentType
        self.contentId = contentId
        self.target = target
        self.contentIds = None
        self.contentRefId = None
        self.contentRefIds = None
        self.contentType = 0
        self.featureId = float(0)
        self.featuredRefId = None

def build_amf_request(key, content_id, url, exp_id):
    ##print 'ContentId:'+content_id
    ##print 'ExperienceId:'+exp_id
    ##print 'URL:'+url
    const = 'd4ac6ddf7cb50c418f192cd5600efeffc4caa52a'
    pyamf.register_class(ViewerExperienceRequest, 'com.brightcove.experience.ViewerExperienceRequest')
    pyamf.register_class(ContentOverride, 'com.brightcove.experience.ContentOverride')
    content_override = ContentOverride(int(content_id))
    viewer_exp_req = ViewerExperienceRequest(url, [content_override], int(exp_id), key)
    
    env = remoting.Envelope(amfVersion=3)
    env.bodies.append(
       (
          "/1",
          remoting.Request(
             target="com.brightcove.experience.ExperienceRuntimeFacade.getDataForExperience",
             body=[const, viewer_exp_req],
             envelope=env
          )
       )
    )
    return env



####def SHOWS(url):
url='http://www.aljazeera.net/programs/discussions'
req = urllib2.Request(url)
req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
response = urllib2.urlopen(req)
link=response.read()
response.close()
##print link
match=re.compile('<h1 class=\"heading-section\">(.+?)</h1>.+?<a href=\"(.+?)\".+?<img src=\"(.+?)\".+?<p>(.+?)</p>').findall(link.replace("\n", "").replace("\t", ""))
##print match
for name,url,thumb,desc in match:
    name=name.replace('&quot;','\"').replace('&#160;','').replace('\\','')
    thumb = thumb.replace("Ã—","×")
    print "URL: http://www.aljazeera.net"+url+"; THUMB: http://www.aljazeera.net"+thumb+"; NAME: "+name+"; DESCRIPTION: "+desc

##def GETREPORTS
##url='http://www.aljazeera.net/ReportsLibraryService/api/ReportsLibrary/GetReportsVideoFilter/0/51'
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##response = urllib2.urlopen(req)
##link=response.read()
####print link
##response.close()
##match=re.compile('"Title\":\"(.+?)\".+?image640x360\":\"(.+?)\".+?FriendlyURL\":\"(.+?)\"').findall(link.replace("\n", "").replace("\t", ""))
##for name,thumb,url in match:
##    thumb = thumb.replace("Ã—","×")
##    print "URL: http://www.aljazeera.net"+url+"; THUMB: http://www.aljazeera.net/File/GetImageCustom/"+thumb+"; NAME: "+name

##def EPISODES(url):
##url='http://www.aljazeera.net/program/behindthenews'
##url=urllib.quote(url, safe='~@#$&()*!+=:;,.?/\'')
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##response = urllib2.urlopen(req)
##link=response.read()
####print link
##response.close()
##match=re.compile('<a class=\"play\" href=\"(.+?)\">.+?<img src=\"(.+?)\".+?title=\"(.+?)\" />').findall(link.replace("\n", "").replace("\t", ""))
##for url,thumb,name in match:
##    ##url=urllib.quote_plus(url)
##    thumb = thumb.replace("Ã—","×")
##    name=name.replace('&quot;','\"')
##    print "URL: http://www.aljazeera.net"+url+"; THUMB: http://www.aljazeera.net"+thumb+"; NAME: "+name##+"; DESCRIPTION: "+desc
##prog=re.compile('<input id="hdnObjectId" type="hidden" value=\"(.+?)\" />').findall(link.replace("\n", "").replace("\t", ""))
##prog_id = prog[0]
####print prog_id
##
##url='http://www.aljazeera.net/Services/GetProgramEpisode/?strProgramId='+prog_id+'&intPageNo=2&intPagesize=9&intPublishOnSort=0&strTopicId=&loadMore=true'
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##response = urllib2.urlopen(req)
##link2=response.read()
####print link2
##response.close()
##loadmore=re.compile('\"Title\\\\":\\\\"(.+?)\\\\",.+?FriendlyURL\\\\":\\\\"(.+?)\\\\"').findall(link2.replace("\n", "").replace("\t", ""))
##for name, url in loadmore:
##    print "URL: http://www.aljazeera.net"+url+"; NAME: "+name

##def ARCHIVE(prog_id):
##from datetime import date
##for y in range(date.today().year,1997,-1):
##    url='http://www.aljazeera.net/programsarchive?resourcesId='+prog_id+'&year='+str(y)
##    name=str(y)
##    print url

##def ARCHIVE_EPI(url,thumb):
##url='http://www.aljazeera.net/programsarchive?resourcesId=8284a8a9-1fe1-43b7-9f05-a9c0bddc419a&year=2014'
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##response = urllib2.urlopen(req)
##link=response.read()
####print link
##response.close()
##match=re.compile('<li><span>(.+?)</span> - <a href="(.+?)">(.+?)</a></li>').findall(link.replace("\n", ""))
####print match
##for date,url,title in match:
##    print "URL: http://www.aljazeera.net"+url+" /// TITLE: "+date+" - "+title

##def ARCHIVE_VOD(url,name,thumb):
##url='http://www.aljazeera.net/programs/fromwashington/2014/11/29/ريمكس-فلسطين-نافذة-تفاعلية-غير-مسبوقة'
##url='http://www.aljazeera.net/programs/fromwashington/2012/9/3/خيارات-أميركا-لحل-الأزمة-السورية'
##url='http://www.aljazeera.net/programs/fromwashington/2012/3/6/صندوق-النقد-وسيطرة-المؤسسات-العسكرية'
##url=urllib.quote(url, safe='~@#$&()*!+=:;,.?/\'')
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##response = urllib2.urlopen(req)
##link=response.read().replace("\n", "")
##response.close()
####print link
##swfUrl = 'http://admin.brightcove.com/viewer/us20151216.1310/federatedVideoUI/BrightcovePlayer.swf?uid=1450551585323'
##exp_id = '664986189001'
##pub_Id = '665001584001'
##
##if link.find('drawBrightcoveVideo')>0:
##    match=re.compile('drawBrightcoveVideo.+?,.+?,.+?\'(.+?)\', \'(.+?)\', \'myExperience').findall(link.replace("\n", "").replace("\t", ""))
##    print "1"
##    print match
##elif link.find('<object class=\"BrightcoveExperience')>0 or link.find('object id="flashObj')>0:
##    match=re.compile('<div id="DynamicContentContainer.+?playerKey\" value=\"(.+?)\".+?videoPlayer\" value=\"(.+?)\"').findall(link.replace("\n", "").replace("\t", ""))
##    print "3"
##    print match
##    ##match=re.compile('flashvars=\"@videoPlayer=(.+?)&.+?playerKey=(.+?)&').findall(link.replace("\n", "").replace("\t", ""))
##elif link.find('embed src=\"http://www.youtube')>0:
##    match=re.compile('embed src=\"(.+?)\"').findall(link)
##    print url
##    print match
##    print "2"
##else:
##    print url
##    print "UNKNOWN SOURCE"

##def VOD(url,name,thumb):    
####url='http://www.aljazeera.net/programs/al-jazeera-correspondents/2015/10/13/سياحة-الريف-بماليزيا-والحناء-بموريتانيا'
##url='http://www.aljazeera.net/programs/behindthenews/2017/6/15/أي-مدى-ستبلغ-الاحتجاجات-على-نقل-تيران-وصنافير-للسعودية'
####url='http://www.aljazeera.net/programs/behindthenews/2017/6/15/%D8%A3%D9%8A-%D9%85%D8%AF%D9%89-%D8%B3%D8%AA%D8%A8%D9%84%D8%BA-%D8%A7%D9%84%D8%A7%D8%AD%D8%AA%D8%AC%D8%A7%D8%AC%D8%A7%D8%AA-%D8%B9%D9%84%D9%89-%D9%86%D9%82%D9%84-%D8%AA%D9%8A%D8%B1%D8%A7%D9%86-%D9%88%D8%B5%D9%86%D8%A7%D9%81%D9%8A%D8%B1-%D9%84%D9%84%D8%B3%D8%B9%D9%88%D8%AF%D9%8A%D8%A9'
##url=urllib.quote(url, safe='~@#$&()*!+=:;,.?/\'')
##
####url='http://www.aljazeera.net/programs/opendialogue/2010/11/15/%D9%85%D8%A4%D8%AA%D9%85%D8%B1-%D8%A7%D9%84%D8%AC%D8%A7%D9%84%D9%8A%D8%A7%D8%AA-%D8%A7%D9%84%D9%81%D9%84%D8%B3%D8%B7%D9%8A%D9%86%D9%8A%D8%A9-%D9%81%D9%8A-%D8%A3%D9%85%D9%8A%D8%B1%D9%83%D8%A7'
####
##req = urllib2.Request(url)
####req.add_header('Host', 'www.aljazeera.net')
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0')
####req.add_header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8')
####req.add_header('Accept-Language', 'en-US,en;q=0.8')
####req.add_header('Accept-Encoding', 'gzip, deflate, sdch')
####req.add_header('Upgrade-Insecure-Requests','1')
####req.add_header('If-Modified-Since', 'Sat, 17 Jun 2017 03:57:23 GMT')
####req.add_header('Referer', 'http://www.aljazeera.net/program/al-jazeera-correspondents')
####req.add_header('Cookie', '__auc=7d72dc701510340535e99816431; _ga=GA1.2.1697695096.1447458528; owa_v=cdh%3D%3E2ed1b53e%7C%7C%7Cvid%3D%3E1447458538720319913%7C%7C%7Cfsts%3D%3E1447458538%7C%7C%7Cdsfs%3D%3E37%7C%7C%7Cnps%3D%3E14; BC_BANDWIDTH=1450550360074X2522; mf_0735d2ef-61eb-441f-bf50-fa0c11e4c547=-1; __asc=ed7a9dba151c1f02abe5ffe5280; owa_s=cdh%3D%3E2ed1b53e%7C%7C%7Clast_req%3D%3E1450669220%7C%7C%7Csid%3D%3E1450668762034374531%7C%7C%7Cdsps%3D%3E0%7C%7C%7Creferer%3D%3E%28none%29%7C%7C%7Cmedium%3D%3Edirect%7C%7C%7Csource%3D%3E%28none%29%7C%7C%7Csearch_terms%3D%3E%28none%29; _gat=1')
####req.add_header('Connection', 'keep-alive')
####req.add_header('Cache-Control', 'max-age=0')
##response = urllib2.urlopen(req)
##link = response.read().replace("\n", "")
####print link
##response.close()
##swfUrl = 'http://admin.brightcove.com/viewer/us20151216.1310/federatedVideoUI/BrightcovePlayer.swf?uid=1450551585323'
##exp_id = '664986189001' ##playerID
##pub_Id = '665001584001'
####print link
##
##if link.find('drawBrightcoveVideo')>0:
##    match=re.compile('drawBrightcoveVideo.+?,.+?,.+?\'(.+?)\', \'(.+?)\', \'myExperience').findall(link.replace("\n", "").replace("\t", ""))
##    match=match+[1]
##elif link.find('flashvars')>0:
##    match=re.compile('flashvars=\"@videoPlayer=(.+?)&.+?playerKey=(.+?)&').findall(link.replace("\n", "").replace("\t", ""))
##    match=match+[1]
##else:
##    match=re.compile('data-video-id="(.+?)" data-account="(.+?)" data-player="(.+?)"').findall(link.replace("\n", "").replace("\t", ""))
##    match=match+[2]
##if match[1] == 1:
##    for content_id,key in match:
##        print "content_id: "+content_id ##@videoPlayer
##        print "key: "+key ##playerKey
##        renditions = get_episode_info(key, content_id, url, exp_id)['programmedContent']['videoPlayer']['mediaDTO']['renditions']
##        match=re.compile('defaultURL\': u\'(.+?)\',.+?\'audioOnly\': (.+?),.+?frameHeight\': (\d+)[}]?[,]?').findall(str(renditions))
##        from operator import itemgetter
##        for url,audio,res in sorted(match,key=lambda x: int(x[2]), reverse=True):
##            if audio == 'False':
##                ##url=url.replace("&", "")
##                rtmp=url.split("&")
##                tcUrl=rtmp[0]
##                PlayPath=rtmp[1]
##                url=tcUrl+PlayPath
##                print tcUrl
##                print PlayPath            
##                print 'URL: '+url+' / AUDIO: '+audio+' / RES: '+res
##else:
##    ##vid='https://edge.api.brightcove.com/playback/v1/accounts/665001584001/videos/5472692040001'
##    data_video_id = match[0][0]
##    data_account = match[0][1]
##    data_player = match[0][2]
####    print "data-video-id: "+data_video_id
####    print "data-account: "+data_account
####    print "data-player: "+data_player
##    js_url='http://players.brightcove.net/'+data_account+'/'+data_player+'_default/index.min.js'
##    req = urllib2.Request(js_url)
##    req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0')
##    response = urllib2.urlopen(req)
##    js_link = response.read()
##    response.close()
##    ##print js_link
##    js_match=re.compile('policyKey:"(.+?)"').findall(js_link)
##    key = js_match[0]
####    print key
##    vid='https://edge.api.brightcove.com/playback/v1/accounts/'+data_account+'/videos/'+data_video_id
####    print vid
##    ##vid='http://c.brightcove.com/services/mobile/streaming/index/rendition.m3u8?assetId=5472911259001&videoId=5472692040001'
##    ##vid='https://edge.api.brightcove.com/playback/v1/accounts/665001584001/videos/5472692040001'
####    vid_req = urllib2.Request(vid)
####    vid_req.add_header('Accept', 'application/json;pk='+key)
####    vid_req.add_header('Origin', 'www.aljazeera.net')
####    vid_req.add_header('Referer', url)
####    vid_req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0')
####    response = urllib2.urlopen(vid_req)
####    vid_link = response.read()
##    
##
##    s = requests.session()
####    params={':authority':'openload.co',
####            ':method':'GET',
####            ':path':'stream/595TqXQSLnY',
####            ':scheme':'https',
####            'mime':'true'
####            }
##    headers={'Host': 'edge.api.brightcove.com',
##             'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',
##             'Accept-Language': 'en-US,en;q=0.5',
##             'Accept': 'application/json;pk='+key,
##             'Accept-Encoding': 'gzip, deflate, br',
##             'Origin':'www.aljazeera.net',
##             'Referer': url,
##             'Connection': 'keep-alive',
##             'Cache-Control': 'max-age=0'
##             }
##    response=s.request("GET",vid,headers=headers)
##    vid_link =response.text
##    ##print response.headers
##    ##print response.cookies
##    ##print response.status_code
##    ##print vid_link
##    response.close()
##    vid_match=re.compile('"avg_bitrate":(\d+?),"width":(\d+?),"src":"(.+?)"').findall(vid_link)
##    
##    index = -1
##    for i, v in enumerate(vid_match):
##      ##print v[1]
##      if v[1] == u'0':
##        index = i
##        break
##    if index >= 0:
##      vid_match[index] = (vid_match[index][0],u'Audio Only', vid_match[index][2])
##
##    vid_match = sorted(vid_match, key=lambda tup: tup[1])
##    
##    for bitrate, width, vid_url in vid_match:
##        print "URL: "+vid_url+" / Resolution: "+width+' ('+bitrate+')'
            
    
