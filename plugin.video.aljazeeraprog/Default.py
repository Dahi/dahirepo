# -*- coding: utf-8 -*-

import sys,os
import httplib,urllib,urllib2,re
from datetime import date
import urlresolver
import pyamf
from pyamf import remoting, amf3, util
from operator import itemgetter
import xbmc, xbmcplugin, xbmcgui, xbmcaddon


addon_id = 'plugin.video.aljazeeraprog'
addon = xbmcaddon.Addon('plugin.video.aljazeeraprog')
aljazeerapath = addon.getAddonInfo('path')

def get_episode_info(key, content_id, url, exp_id):
    conn = httplib.HTTPSConnection("secure.brightcove.com")
    envelope = build_amf_request(key, content_id, url, exp_id)
    headers = {"Content-type": "application/x-amf",
               "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
               "Accept-Encoding": "deflate",
               "Accept-Language": "en-US,en;q=0.5",
               "Connection": "keep-alive",
               "Host": "secure.brightcove.com",
               "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0"}
    conn.request("POST", "/services/messagebroker/amf?playerKey="+key, str(remoting.encode(envelope).read()),headers)
    response = conn.getresponse().read()
    response = remoting.decode(response).bodies[0][1].body
    return response

class ViewerExperienceRequest(object):
    def __init__(self, URL, contentOverrides, experienceId, playerKey, TTLToken=''):
        self.contentOverrides = contentOverrides
        self.TTLToken = TTLToken
        self.URL = URL
        self.deliveryType = float(0)
        self.experienceId = experienceId
        self.playerKey = playerKey

class ContentOverride(object):
    def __init__(self, contentId, contentType=0, target='videoPlayer'):
        self.contentType = contentType
        self.contentId = contentId
        self.target = target
        self.contentIds = None
        self.contentRefId = None
        self.contentRefIds = None
        self.contentType = 0
        self.featureId = float(0)
        self.featuredRefId = None

def build_amf_request(key, content_id, url, exp_id):
    print 'ContentId:'+content_id
    print 'ExperienceId:'+exp_id
    const = 'd4ac6ddf7cb50c418f192cd5600efeffc4caa52a'
    print const
    pyamf.register_class(ViewerExperienceRequest, 'com.brightcove.experience.ViewerExperienceRequest')
    pyamf.register_class(ContentOverride, 'com.brightcove.experience.ContentOverride')
    content_override = ContentOverride(int(content_id))
    viewer_exp_req = ViewerExperienceRequest(url, [content_override], int(exp_id), key)
    
    env = remoting.Envelope(amfVersion=3)
    env.bodies.append(
       (
          "/1",
          remoting.Request(
             target="com.brightcove.experience.ExperienceRuntimeFacade.getDataForExperience",
             body=[const, viewer_exp_req],
             envelope=env
          )
       )
    )
    return env

def CATEGORIES():
        addDir('برامج حوارية','http://www.aljazeera.net/programs/discussions',1,os.path.join(aljazeerapath,'resources', 'Categories','discussions.jpg'),'')
        addDir('مجلة تلفزيونية','http://www.aljazeera.net/programs/newsmagazineshows',1,os.path.join(aljazeerapath,'resources', 'Categories','newsmagazineshows.jpg'),'')
        addDir('برامج وثائقية','http://www.aljazeera.net/programs/documentaries',1,os.path.join(aljazeerapath,'resources', 'Categories','documentaries.jpg'),'')
        addDir('برامج متوقفة','http://www.aljazeera.net/programs/discontinued',1,os.path.join(aljazeerapath,'resources', 'Categories','discontinued.jpg'),'')
        addDir('مكتبة التقارير','http://www.aljazeera.net/ReportsLibraryService/api/ReportsLibrary/GetReportsVideoFilter/0/51',2,os.path.join(aljazeerapath,'resources', 'Categories','reportslibrary.jpg'),'')

def SHOWS(url):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<h1 class=\"heading-section\">(.+?)</h1>.+?<a href=\"(.+?)\".+?<img src=\"(.+?)\".+?<p>(.+?)</p>').findall(link.replace("\n", "").replace("\t", ""))
        for name,url,thumb,desc in match:
            name=name.replace('&quot;','\"').replace('&#160;','').replace('\\','')
            url=urllib.quote(url, safe='~@#$&()*!+=:;,.?/\'')
            thumb = thumb.replace("Ã—","×")
            addDir(name,"http://www.aljazeera.net"+url,3,"http://www.aljazeera.net"+thumb,desc)

def GETREPORTS(url):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read()
        response.close()
        match=re.compile('"Title\":\"(.+?)\".+?image640x360\":\"(.+?)\".+?FriendlyURL\":\"(.+?)\"').findall(link.replace("\n", "").replace("\t", ""))
        for name,thumb,url in match:
            name=name.replace('&quot;','\"').replace('&#160;','').replace('\\','')
            url=urllib.quote(url, safe='~@#$&()*!+=:;,.?/\'')
            thumb = thumb.replace("Ã—","×")
            addDir(name,"http://www.aljazeera.net"+url,6,"http://www.aljazeera.net/File/GetImageCustom/"+thumb,'')

def EPISODES(url,thumb):
        gif=thumb
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<a class=\"play\" href=\"(.+?)\">.+?<img src=\"(.+?)\".+?title=\"(.+?)\" />').findall(link.replace("\n", "").replace("\t", ""))
        for url,thumb,name in match:
            name=name.replace('&quot;','\"').replace('&#160;','').replace('\\','')
            url=urllib.quote(url, safe='~@#$&()*!+=:;,.?/\'')
            thumb = thumb.replace("Ã—","×")
            addDir(name,"http://www.aljazeera.net"+url,6,"http://www.aljazeera.net"+thumb,'')
        
        try:
            prog=re.compile('<input id="hdnObjectId" type="hidden" value=\"(.+?)\" />').findall(link.replace("\n", "").replace("\t", ""))
            prog_id = prog[0]            
            url='http://www.aljazeera.net/Services/GetProgramEpisode/?strProgramId='+prog_id+'&intPageNo=2&intPagesize=9&intPublishOnSort=0&strTopicId=&loadMore=true'
            req = urllib2.Request(url)
            req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
            response = urllib2.urlopen(req)
            link2=response.read()
            response.close()
            loadmore=re.compile('\"Title\\\\":\\\\"(.+?)\\\\",.+?FriendlyURL\\\\":\\\\"(.+?)\\\\"').findall(link2.replace("\n", "").replace("\t", ""))
            for name, url in loadmore:
                name=name.replace('&quot;','\"').replace('&#160;','').replace('\\','')
                url=urllib.quote(url, safe='~@#$&()*!+=:;,.?/\'')
                addDir(name,"http://www.aljazeera.net"+url,6,"http://www.aljazeera.net/Views/Shared/shared/images/jazeraLogo%20475%20%C3%97%20267.png",'')
        except: pass
        
        try:
            addDir('أرشيف البرنامج',prog_id,4,gif,'')
        except: pass

def ARCHIVE(prog_id,thumb):
        for y in range(date.today().year,1997,-1):
            url='http://www.aljazeera.net/programsarchive?resourcesId='+prog_id+'&year='+str(y)
            name=str(y)
            addDir(name,url,5,thumb,'')

def ARCHIVE_EPI(url,thumb):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<li><span>(.+?)</span> - <a href="(.+?)">(.+?)</a></li>').findall(link.replace("\n", ""))
        for date,url,title in match:
            title=title.replace('&quot;','\"').replace('&#160;','').replace('\\','')
            print "URL: http://www.aljazeera.net"+url+" /// TITLE: "+date+" - "+title
            url=urllib.quote(url, safe='~@#$&()*!+=:;,.?/\'')
            addDir(date+" - "+title,"http://www.aljazeera.net"+url,7,thumb,'')
    
def VOD(url,name,thumb):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read()
        response.close()
        
        swfUrl = 'http://admin.brightcove.com/viewer/us20151216.1310/federatedVideoUI/BrightcovePlayer.swf?uid=1450551585323'
        exp_id = '664986189001'
        pub_Id = '665001584001'
        
        if link.find('drawBrightcoveVideo')>0:
            match=re.compile('drawBrightcoveVideo.+?,.+?,.+?\'(.+?)\', \'(.+?)\', \'myExperience').findall(link.replace("\n", "").replace("\t", ""))
            match=match+[1]
        elif link.find('flashvars')>0:
            match=re.compile('flashvars=\"@videoPlayer=(.+?)&.+?playerKey=(.+?)&').findall(link.replace("\n", "").replace("\t", ""))
            match=match+[1]
        else:
            match=re.compile('data-video-id="(.+?)" data-account="(.+?)" data-player="(.+?)"').findall(link.replace("\n", "").replace("\t", ""))
            match=match+[2]
        if match[1] == 1:    
            for content_id,key in match:
                renditions = get_episode_info(key, content_id, url, exp_id)['programmedContent']['videoPlayer']['mediaDTO']['renditions']
                match=re.compile('defaultURL\': u\'(.+?)\',.+?\'audioOnly\': (.+?),.+?frameHeight\': (\d+)[}]?[,]?').findall(str(renditions))
                from operator import itemgetter
                for uri,audio,res in sorted(match,key=lambda x: int(x[2]), reverse=True):
                    if audio == 'False':
                        rtmp=uri.split("&")
                        tcUrl=rtmp[0]
                        PlayPath=rtmp[1]
                        uri=tcUrl+PlayPath
                        playVid(uri,name,tcUrl,PlayPath,thumb)
        else:
            data_video_id = match[0][0]
            data_account = match[0][1]
            data_player = match[0][2]

            js_url='http://players.brightcove.net/'+data_account+'/'+data_player+'_default/index.min.js'
            req = urllib2.Request(js_url)
            req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0')
            response = urllib2.urlopen(req)
            js_link = response.read()
            response.close()
            js_match=re.compile('policyKey:"(.+?)"').findall(js_link)
            key = js_match[0]
            vid='https://edge.api.brightcove.com/playback/v1/accounts/'+data_account+'/videos/'+data_video_id

            vid_req = urllib2.Request(vid)
            vid_req.add_header('Host', 'edge.api.brightcove.com')
            vid_req.add_header('User-Agent', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0')
            vid_req.add_header('Accept', 'application/json;pk='+key)
            vid_req.add_header('Accept-Language', 'en-US,en;q=0.5')
            vid_req.add_header('Accept-Encoding', 'gzip, deflate, br')
            vid_req.add_header('Origin', 'www.aljazeera.net')
            vid_req.add_header('Referer', url)
            vid_req.add_header('Connection', 'keep-alive')
            vid_req.add_header('Cache-Control', 'max-age=0')

            try:
                vid_resp = urllib2.urlopen(vid_req)
            except urllib2.HTTPError, e:
                print e

            vid_link = vid_resp.read()
            vid_resp.close()
            vid_match=re.compile('"avg_bitrate":(\d+?),"width":(\d+?),"src":"(.+?)"').findall(vid_link)
            index = -1
            for i, v in enumerate(vid_match):
              if v[1] == u'0':
                index = i
                break
            if index >= 0:
              vid_match[index] = (vid_match[index][0],u'Audio Only', vid_match[index][2])
            vid_match = sorted(vid_match, key=lambda tup: tup[1])
            dialog = xbmcgui.Dialog()
            index = dialog.select('Choose Resolution', [m[1]+' ('+m[0]+')' for m in vid_match])
            if index > -1:
                vid_play(vid_match[index][2],name)
            else:
                return
               
def ARCHIVE_VOD(url,name,thumb):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        
        swfUrl = 'http://admin.brightcove.com/viewer/us20151216.1310/federatedVideoUI/BrightcovePlayer.swf?uid=1450551585323'
        exp_id = '664986189001'
        pub_Id = '665001584001'
        
        if link.find('drawBrightcoveVideo')>0:
            match=re.compile('drawBrightcoveVideo.+?,.+?,.+?\'(.+?)\', \'(.+?)\', \'myExperience').findall(link.replace("\n", "").replace("\t", ""))
            for content_id,key in match:
                renditions = get_episode_info(key, content_id, url, exp_id)['programmedContent']['videoPlayer']['mediaDTO']['renditions']
                match=re.compile('defaultURL\': u\'(.+?)\',.+?\'audioOnly\': (.+?),.+?frameHeight\': (\d+)[}]?[,]?').findall(str(renditions))
                from operator import itemgetter
                for uri,audio,res in sorted(match,key=lambda x: int(x[2]), reverse=True):
                    if audio == 'False':
                        rtmp=uri.split("&")
                        tcUrl=rtmp[0]
                        PlayPath=rtmp[1]
                        uri=tcUrl+PlayPath
                        playVid(uri,name,tcUrl,PlayPath,thumb)
                        
        elif link.find('<object class=\"BrightcoveExperience')>0:
            match=re.compile('<div id="DynamicContentContainer.+?playerKey\" value=\"(.+?)\".+?videoPlayer\" value=\"(.+?)\"').findall(link.replace("\n", "").replace("\t", ""))
            for key,content_id in match:
                renditions = get_episode_info(key, content_id, url, exp_id)['programmedContent']['videoPlayer']['mediaDTO']['renditions']
                match=re.compile('defaultURL\': u\'(.+?)\',.+?\'audioOnly\': (.+?),.+?frameHeight\': (\d+)[}]?[,]?').findall(str(renditions))
                from operator import itemgetter
                for uri,audio,res in sorted(match,key=lambda x: int(x[2]), reverse=True):
                    if audio == 'False':
                        rtmp=uri.split("&")
                        tcUrl=rtmp[0]
                        PlayPath=rtmp[1]
                        uri=tcUrl+PlayPath
                        playVid(uri,name,tcUrl,PlayPath,thumb)
                    
        elif link.find('object id=\"flashObj')>0:
            match=re.compile('<div id="DynamicContentContainer.+?videoPlayer=(.+?)&.+?playerKey=(.+?)&').findall(link.replace("\n", "").replace("\t", ""))
            for content_id,key in match:
                renditions = get_episode_info(key, content_id, url, exp_id)['programmedContent']['videoPlayer']['mediaDTO']['renditions']
                match=re.compile('defaultURL\': u\'(.+?)\',.+?\'audioOnly\': (.+?),.+?frameHeight\': (\d+)[}]?[,]?').findall(str(renditions))
                from operator import itemgetter
                for uri,audio,res in sorted(match,key=lambda x: int(x[2]), reverse=True):
                    if audio == 'False':
                        rtmp=uri.split("&")
                        tcUrl=rtmp[0]
                        PlayPath=rtmp[1]
                        uri=tcUrl+PlayPath
                        playVid(uri,name,tcUrl,PlayPath,thumb)
                        
        elif link.find('embed src=\"http://www.youtube')>0:
            match=re.compile('embed src=\"(.+?)\"').findall(link)
            stream_url = urlresolver.resolve(match[0])
            playyoutube(stream_url,name,thumb)
            
        else:
            print "Unknown Source"

def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param

def playVid(url,name,tcUrl,PlayPath,iconimage):
        ok=True
        
        liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        liz.setProperty('PlayPath', PlayPath)
        liz.setProperty('tcUrl', tcUrl)
        xbmc.Player(xbmc.PLAYER_CORE_DVDPLAYER).play(tcUrl, liz)
        return ok

def playyoutube(url,name,iconimage):
    ok=True
    listitem = xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage=iconimage)
    xbmc.Player().play(url,listitem)
    return ok

def vid_play(url,name):
        listitem = xbmcgui.ListItem(name)
        xbmc.Player().play(url,listitem)
        return

def addDir(name,url,mode,iconimage,desc):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)+"&thumb="+urllib.quote_plus(iconimage)+"&desc="+urllib.quote_plus(desc)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name, "Description": desc } )
        liz.setProperty('fanart_image',iconimage)
        ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        return ok
        
              
params=get_params()
url=None
name=None
mode=None
thumb=None
desc=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass
try:
        thumb=urllib.unquote_plus(params["thumb"])
except:
        pass
try:
        desc=urllib.unquote_plus(params["desc"])
except:
        pass
    
print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "Thumb: "+str(thumb)
print "Description: "+str(desc)

if mode==None or url==None or len(url)<1:
        print ""
        CATEGORIES()
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==1:
        print ""+url
        SHOWS(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==2:
        print ""+url
        GETREPORTS(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==3:
        print ""+url
        EPISODES(url,thumb)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==4:
        print ""+url
        ARCHIVE(url,thumb)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==5:
        print ""+url
        ARCHIVE_EPI(url,thumb)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==6:
        print ""+url
        VOD(url,name,thumb)
        ##xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==7:
        print ""+url
        ARCHIVE_VOD(url,name,thumb)
        
##elif mode==8:
##        print ""+url
##        SOURCES(url,name)

