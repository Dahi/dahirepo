import sys, os
import urllib, urllib2, re

import xbmc, xbmcplugin, xbmcgui, xbmcaddon

#TV DASH - by You 2008.

addon_id = 'plugin.video.dubaitv'
addon = xbmcaddon.Addon('plugin.video.dubaitv')
dubaitvpath = addon.getAddonInfo('path')


def CATEGORIES():
        url='http://admin.mangomolo.com/analytics/index.php/plus/categories?lang=ar&user_id=71'
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
        req.add_header('Accept-Language', 'en-US,en;q=0.5')
        ##req.add_header('Accept-Encoding', 'gzip, deflate')
        req.add_header('Referer', 'http://www.dcndigital.ae/')
        req.add_header('Origin', 'http://www.dcndigital.ae')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('"id": "(.+?)",.+?"title_ar": "(.+?)",.+?"icon": "(.+?)".+?"user_id": "(.+?)",').findall(link)
        for uid, name, img, user_id in match:
           if not name == "MyDCN":
              title=name.decode('unicode-escape').encode('utf-8')
              thumb='http://admin.mangomolo.com/analytics/'+img.replace("\\","")
              addDir(title,'http://admin.mangomolo.com/analytics/index.php/plus/category?id='+uid+'&o=category&user_id='+user_id+'&p=1&limit=200',1,thumb,os.path.join(dubaitvpath,'resources', 'Categories',uid+'.jpg'))
            
def SHOWS(url):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
        req.add_header('Accept-Language', 'en-US,en;q=0.5')
        ##req.add_header('Accept-Encoding', 'gzip, deflate')
        req.add_header('Referer', 'http://www.dcndigital.ae/')
        req.add_header('Origin', 'http://www.dcndigital.ae')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('"id": "(.+?)",.+?"title_ar": "(.+?)",.+?"cover": "(.+?)",.+?"thumbnail": "(.+?)".+?"user_id": "(.+?)"').findall(link)
        for show_id, name, img, icon, user_id in match:
            title=name.decode('unicode-escape').encode('utf-8')
            thumb='http://admin.mangomolo.com/analytics/'+icon.replace("\\","")
            fanart='http://admin.mangomolo.com/analytics/'+img.replace("\\","")
            addDir(title,show_id+','+user_id,2,thumb,fanart)

def EPISODES(keys):        
        key = keys.split(',')
        show_id = key[0]
        user_id = key[1]
        address = 'http://admin.mangomolo.com/analytics/index.php/plus/show'
        values = {'season':'false', 'channel_user_id':'false'}
        values['show_id'] = str(show_id)
        values['user_id'] = str(user_id)
        data = urllib.urlencode(values)
        req = urllib2.Request(address, data)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
        req.add_header('Accept-Language', 'en-US,en;q=0.5')
        ##req.add_header('Accept-Encoding', 'gzip, deflate')
        req.add_header('Referer', 'http://www.dcndigital.ae/')
        req.add_header('Origin', 'http://www.dcndigital.ae')
        response = urllib2.urlopen(req)
        link = response.read().replace("\n", "")
        response.close()
        if link.find("seasons") > -1:
            seasons = re.compile('"seasons":(.+?),    "also"').findall(link)
            season = re.compile('"id"\: "(.+?)".+?title_ar"\: "(.+?)",.+?"cover"\: "(.+?)".+?"thumbnail": "(.+?)",.+?"user_id": "(.+?)"').findall(seasons[0])
            for show_id, name, fanart, thumb, userid in season:
                title=name.decode('unicode-escape').encode('utf-8')
                thumb='http://admin.mangomolo.com/analytics/'+thumb.replace("\\","")
                fanart='http://admin.mangomolo.com/analytics/'+fanart.replace("\\","")
                addDir2('Season: '+title,show_id+','+user_id,2,thumb,fanart)

        img=re.compile('"cover": \"(.+?)\",').findall(link)
        fanart='http://admin.mangomolo.com/analytics/'+img[0].replace("\\","")
        block = re.compile('"videos": \[(.+?)\]').findall(link)
        match=re.compile('"id":.+?"(.+?)",.+?"url":.+?"(.+?)",.+?"img":.+?"(.+?)",.+?"title_ar":.+?"(.+?)",.+?"user_id":.+?"(.+?)"').findall(block[0])
        for uid, url, icon, name, user_id in match:
           title=name.decode('unicode-escape').encode('utf-8')
           title = title.replace(" :",":").replace(": ",":")
           thumb='http://admin.mangomolo.com/analytics/'+icon.replace("\\","")
           addDir2(title,'http://dmi.mangomolo.com'+url.replace("\\", ""),3,thumb,fanart)
        
def VID(url,name):
        listitem = xbmcgui.ListItem(name)
        xbmc.Player().play(url,listitem)
        return
             
def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param

def addDir(name,url,mode,iconimage,fanart):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        liz.setProperty('fanart_image',fanart)
        xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        ok=xbmcplugin.addSortMethod(int(sys.argv[1]), xbmcplugin.SORT_METHOD_TITLE)
        return ok

def addDir2(name,url,mode,iconimage,fanart):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        liz.setProperty('fanart_image',fanart)
        xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)  
        return ok
        
              
params=get_params()
url=None
name=None
mode=None
thumb=None
fanart=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass
try:
        thumb=urllib.unquote_plus(params["thumb"])
except:
        pass
try:
        fanart=urllib.unquote_plus(params["fanart"])
except:
        pass
    
print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "Thumb: "+str(thumb)
print "FanArt: "+str(fanart)

if mode==None or url==None or len(url)<1:
        print ""
        CATEGORIES()
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==1:
        print ""+url
        SHOWS(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==2:
        print ""+url
        EPISODES(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==3:
        print "stream_url "+url
        VID(url,name)
