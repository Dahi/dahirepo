# This test program is for finding the correct Regular expressions on a page to insert into the plugin template.
# After you have entered the url between the url='here' - use ctrl-v
# Copy the info from the source html and put it between the match=re.compile('here')
# press F5 to run if match is blank close and try again.

import sys,os
import urllib,urllib2,re
##from xbmcstubs_master import xbmc,xbmcplugin,xbmcgui,xbmcaddon

##addon_id = 'plugin.video.bokra'
##addon = xbmcaddon.Addon(id=addon_id)
##bokrapath = addon.getAddonInfo('path')
##print bokrapath

###  def CATEGORIES():  #####
##url='http://awaan.ae/categories'
##req = urllib2.Request(url)
####req.add_header('Host', 'http://admin.mangomolo.com')
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
##req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
##req.add_header('Accept-Language', 'en-US,en;q=0.5')
####req.add_header('Accept-Encoding', 'gzip, deflate')
####req.add_header('Referer', 'http://www.dcndigital.ae/')
####req.add_header('Origin', 'http://www.dcndigital.ae')
####req.add_header('Connection', 'keep-alive')
##response = urllib2.urlopen(req)
##link=response.read().replace("\n", "")
####print link
##response.close()
##match=re.compile('<div class=\"col-lg-2 col-md-3 col-sm-4 col-xs-6\">.+?<a href=\"(.+?)"><img data-src="(.+?)\".+?class="drama-title\">(.+?)</a>').findall(link)
##for url, thumb, title in match:
##  print 'url: '+ url+' / title: '+title+' / thumb: '+thumb

    
#####  def SHOWS(url):  #####
##url='http://awaan.ae/relatedshows/206562/%D8%B1%D9%85%D8%B6%D8%A7%D9%86%202016'
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
##req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
##req.add_header('Accept-Language', 'en-US,en;q=0.5')
####req.add_header('Accept-Encoding', 'gzip, deflate')
##req.add_header('Referer', 'http://www.dcndigital.ae/')
##req.add_header('Origin', 'http://www.dcndigital.ae')
##response = urllib2.urlopen(req)
##link=response.read().replace("\n", "")
####print link
##response.close()
##match=re.compile('<div class=\"col-lg-4 col-md-6 col-sm-6 col-xs-6\">.+?<a href="(.+?)\"><img data-src=\"(.+?)".+?class="drama-title giveMeEllipsis\">(.+?)<').findall(link)
##for url, img, title in match:
##    print 'URL: '+ url+' / IMAGE: '+ img+' / title: '+title##.decode('unicode-escape')

#####  def EPISODES(url):  #####

##url = 'http://awaan.ae/show/206629/%D8%B3%D9%8A%D9%86-%D8%AC%D9%8A%D9%85'
####url = 'http://awaan.ae/show/199434/%D9%85%D8%B3%D9%84%D8%B3%D9%84-%D8%AD%D8%B1%D9%8A%D9%85-%D8%A7%D9%84%D8%B3%D9%84%D8%B7%D8%A7%D9%86'
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
##req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
##req.add_header('Accept-Language', 'en-US,en;q=0.5')
####req.add_header('Accept-Encoding', 'gzip, deflate')
####req.add_header('Referer', 'http://www.dcndigital.ae/')
####req.add_header('Origin', 'http://www.dcndigital.ae')
##response = urllib2.urlopen(req)
##link = response.read().replace("\n", "")
####print link
##response.close()
##match=re.compile('<div class=\"col-lg-3 col-md-4 col-sm-6 col-xs-6\">.+?<a href="(.+?)\"><img data-src=\"(.+?)\".+?title=\"(.+?)\"').findall(link)
##for url, img, title in match:
##    print 'URL: '+ url+' / IMAGE: '+ img+' / title: '+title
##    
##if link.find("seasons") > -1:
##    seasons = re.compile('<select id="seasons(.+?)</select>').findall(link)
##    season = re.compile('<option value="(.+?)">(.+?)</').findall(seasons[0])
##    for url, title in season:
##        print 'SEASON URL: '+url+' / TITLE: '+title
    
url='http://vod.dmi.ae/program/205056/%D9%85%D8%B3%D9%84%D8%B3%D9%84_%D8%A8%D8%AF%D9%88%D9%86_%D8%B0%D9%83%D8%B1_%D8%A3%D8%B3%D9%85%D8%A7%D8%A1'
req = urllib2.Request(url)
req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
response = urllib2.urlopen(req)
link=response.read().replace("\n", "")
#print link
image=re.compile('<ul id="slider">			<li class="image"><img src="(.+?)jpg"').findall(link)
fanart=image[0]+"jpg"
#print fanart
response.close()
match=re.compile('<a class="item" href="/(.+?)">.+?<img src="(.+?)" alt="(.+?)" />').findall(link)
pagination=re.compile('<div class="pagination">(.+?)<div class="footer">').findall(link)
pages=re.compile('<a href="(.+?)">(\d)</a>').findall(pagination[0])
for url, thumb, name in match:
    print "thumb: " +thumb+ " url: http://vod.dmi.ae/" +url+ " name: " +ARABIC(name).replace("MSLSL ", "")
for url, name in pages:
    print "thumb: NONE" + " url: http://vod.dmi.ae" +re.sub('/.+?<a href="','',url)+ " name: " +name

#####  def VID(url,name):  #####
##url='http://vod.dmi.ae/media/257393/%D9%85%D8%B3%D9%84%D8%B3%D9%84_%D9%82%D8%B5%D8%B5_%D8%A7%D9%84%D8%B9%D8%AC%D8%A7%D8%A6%D8%A8_%D9%81%D9%8A_%D8%A7%D9%84%D9%82%D8%B1%D8%A2%D9%86_%D8%A7%D9%84%D8%AD%D9%84%D9%82%D8%A9_28'
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##response = urllib2.urlopen(req)
##link=response.read().replace("\n", "")
###print link
##response.close()
##match=re.compile('"fileUrl":"(.+?)\?cdnParams=').findall(link)
##stream_url=match[0].replace("\\","")
##print stream_url

