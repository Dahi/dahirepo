# -*- coding: utf-8 -*-
import sys, os
import urllib, urllib2, re
import requests

import xbmc, xbmcplugin, xbmcgui, xbmcaddon

#TV DASH - by You 2008.

addon_id = 'plugin.video.gratisiptv'
addon = xbmcaddon.Addon('plugin.video.gratisiptv')
gratisiptvpath = addon.getAddonInfo('path')

def M3U_LIST():
        url='https://www.gratisiptv.com/africa/arab-countries/'
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
        req.add_header('Accept-Language', 'en-US,en;q=0.5')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('div class="archive-info"(.+?)Download').findall(link)

        match2=re.compile('entry-footer cf"><a href="(.+?)" class="continue-reading').findall(match[0])
        req = urllib2.Request(match2[0])
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
        req.add_header('Accept-Language', 'en-US,en;q=0.5')
        response = urllib2.urlopen(req)
        link2=response.read().replace("\n", "")
        response.close()

        match3=re.compile('Or click on link to download Arab Countries iptv channels list(.+?)footer class="entry-footer cf"').findall(link2)
        match4=re.compile('<a href="(.+?)">Download Arab Countries IpTV List').findall(match3[0])

        for idx, url in enumerate(match4):
          print 'i: '+str(idx+1)+' / url: '+ url
          addDir('Link: '+str(idx+1),url,1,os.path.join(gratisiptvpath,'resources', 'Pages',str(idx+1)+'.jpg'),os.path.join(gratisiptvpath,'resources', 'Pages',str(idx+1)+'.jpg'))
            
def CHANNELS(url):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
        req.add_header('Accept-Language', 'en-US,en;q=0.5')
        response = urllib2.urlopen(req)
        link=response.read().replace("\r", "")
        response.close()
        if "tvg-name" in link:
                match=re.compile('#EXTINF:-1.*?tvg-name="(.+?)".*?tvg-logo="(.*?)".+?\n(.+?)\n').findall(link)
                for title, logo, url in match:
                        addDir(title,url,2,'','')
        else:
                match=re.compile('#EXTINF:-1,(.+?)\n(.+?)\n').findall(link)
                for title, url in match:
                        addDir(title,url,2,'','')

def VID(stream_url,name):
        print "stream_url: "+stream_url
        listitem = xbmcgui.ListItem(name)
        xbmc.Player().play(stream_url,listitem)

        return

def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param

def addDir(name,url,mode,iconimage,fanart):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        liz.setProperty('fanart_image',fanart)
        xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        ##ok=xbmcplugin.addSortMethod(int(sys.argv[1]), xbmcplugin.SORT_METHOD_TITLE)
        return ok
        
              
params=get_params()
url=None
name=None
mode=None
thumb=None
fanart=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass
try:
        thumb=urllib.unquote_plus(params["thumb"])
except:
        pass
try:
        fanart=urllib.unquote_plus(params["fanart"])
except:
        pass
    
print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "Thumb: "+str(thumb)
print "FanArt: "+str(fanart)

if mode==None or url==None or len(url)<1:
        print ""
        M3U_LIST()
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==1:
        print ""+url
        CHANNELS(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==2:
        print ""+url
        VID(url,name)
##        xbmcplugin.endOfDirectory(int(sys.argv[1]))
##        
##elif mode==4:
##        print ""+url
##        VID(url,name)
