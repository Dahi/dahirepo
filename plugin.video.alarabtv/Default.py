# -*- coding: utf-8 -*-
import sys, os
import urllib, urllib2, re

import xbmc, xbmcplugin, xbmcgui, xbmcaddon

#TV DASH - by You 2008.

addon_id = 'plugin.video.alarabtv'
addon = xbmcaddon.Addon('plugin.video.alarabtv')
alarabtvpath = addon.getAddonInfo('path')
host='https://vod.alarab.com'

def CATEGORIES():
        url='https://vod.alarab.com/view-299/%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA-%D8%AA%D8%B1%D9%83%D9%8A%D8%A9'
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
        req.add_header('Accept-Language', 'en-US,en;q=0.5')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<ul id="*?navbar"*?>(.+?)</div>').findall(link)

        match2=re.compile('href="(.+?)".*?>(.+?)</a>').findall(match[0])

        for cat, title in match2:
          print 'url: '+ host+cat+' / title: '+title
          img=re.compile('view-(.+?)/').findall(cat)
          thumb=os.path.join(alarabtvpath,'resources', 'Categories',img[0]+'.jpg')
          if img[0] == '4' or img[0] == '8' or img[0] == '299' or img[0] == '311' or img[0] == '1951':
                  addDir(title,host+cat,1,thumb,thumb)
          else:
                  addDir(title,host+cat,2,thumb,thumb)
            
def SHOWS(url):
        url = url.decode('utf-8').encode('ascii', 'xmlcharrefreplace')
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
        req.add_header('Accept-Language', 'en-US,en;q=0.5')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<div class="description-box "><div class="video-box"><a href="(.+?)".+?data-src="(.+?)" alt="(.+?)"').findall(link)
        match = sorted(match, key=lambda tup: tup[2])
        for url, img, title in match:
            addDir(title,host+url,2,img,img)
        pages=re.compile('class="tsc_3d_button red"\s*?href="(.+?)".*?title="(.+?)"').findall(link)
        for url, title in pages:
            addDir(title,host+url,1,os.path.join(alarabtvpath,'resources', 'Pages',title+'.jpg'),os.path.join(alarabtvpath,'resources', 'Pages','pages.jpg'))

def EPISODES(url):
        url = url.decode('utf-8').encode('ascii', 'xmlcharrefreplace')
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
        req.add_header('Accept-Language', 'en-US,en;q=0.5')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<div class="description-box "><div class="video-box"><a href="(.+?)".+?data-src="(.+?)" alt="(.+?)"').findall(link)
        match = sorted(match, key=lambda tup: tup[2])
        for url, img, title in match:
            addDir(title,host+ url,3,img,img)
        page=re.compile('class="tsc_3d_button red"\s*?href="(.+?)" title="(.+?)"').findall(link)
        for url, title in page:
           addDir(title,host+ url,2,os.path.join(alarabtvpath,'resources', 'Pages',title+'.jpg'),os.path.join(alarabtvpath,'resources', 'Pages','pages.jpg'))
        
def SOURCES(url,name):
        url = url.decode('utf-8').encode('ascii', 'xmlcharrefreplace')
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()

        match=re.compile('<div class="resp-container">.*?<iframe class="resp-iframe".*?src="(.+?)"').findall(link)
        url = match[0]
        url = url.decode('utf-8').encode('ascii', 'xmlcharrefreplace')
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()

##        match=re.compile('file: \'(.+?)\'').findall(link)
##        if match[0].find("https") == 0:
##            pos1 = [m.start() for m in re.finditer(r"net/",match[0])][0]+4
##            pos2 = [m.start() for m in re.finditer(r"/manifest",match[0])][0]
##            vid = match[0][pos1:pos2]
##        else:
##            vid = match[0]
##            
##        VID('https://videodelivery.net/'+vid+'/manifest/video.m3u8',name)

        try:
            match=re.compile('file: \'(.+?)\'').findall(link)
            if match[0].find("https") == 0:
                pos1 = [m.start() for m in re.finditer(r"net/",match[0])][0]+4
                pos2 = [m.start() for m in re.finditer(r"/manifest",match[0])][0]
                vid = match[0][pos1:pos2]
            else:
                vid = match[0]
            print 'URL: '+ 'https://videodelivery.net/'+vid+'/manifest/video.m3u8'+' / title: m3u8'
            m3u8_match=[('https://videodelivery.net/'+vid+'/manifest/video.m3u8','m3u8')]  
        except:
            m3u8_match=[]
            pass

        try:
            m3u8_2_match=re.compile('"file": "(.+?)"').findall(link)
            ##print 'URL: '+ flv_match[0]+' / title: Flash'
            m3u8_2_match=[(m3u8_2_match[0],'m3u8 (2)')]
        except:
            m3u8_2_match=[]
            pass

        try:
            m3u8_3_match=re.compile('<source src="(.+?)"').findall(link)
            ##print 'URL: '+ flv_match[0]+' / title: Flash'
            m3u8_3_match=[(m3u8_3_match[0],'m3u8 (3)')]
        except:
            m3u8_3_match=[]
            pass
        
        try:
            flv_match=re.compile('<PARAM NAME="movie".+?file=(.+?)\.flv&').findall(link)
            print 'URL: '+ flv_match[0]+' / title: Flash'
            flv_match=[(flv_match[0],'Flash')]
        except:
            flv_match=[]
            pass
        
        try:
            match=re.compile('var playerInstance(.+?)startparam:').findall(link)
            mp4_match=re.compile('file: "(.+?)".*?label: "(.+?)"').findall(match[0])
            print 'URL: '+ mp4_match[0][0]+' / title: '+ mp4_match[0][1]
            try:
                    print 'URL: '+ mp4_match[1][0]+' / title: '+ mp4_match[1][1]
            except:
                    pass
        except:
            mp4_match=[]
            pass
        
        sources = m3u8_match+m3u8_2_match+m3u8_3_match+flv_match+mp4_match
        
        index = -1
        dialog = xbmcgui.Dialog()
        index = dialog.select('Select Source', [m[1] for m in sources])
        if index > -1:
                VID(sources[index][0],name)

def VID(stream_url,name):
        if stream_url.find('youtube') > -1:
                youtube=re.compile('https://www.youtube.com/(.+?)\.flv').findall(stream_url)
                vid= 'https://www.youtube.com/'+youtube[0]
                print vid
                import urlresolver
                stream_url = urlresolver.resolve(vid)
        print "stream_url: "+stream_url
        listitem = xbmcgui.ListItem(name)
        xbmc.Player().play(stream_url,listitem)

        return

def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param

def addDir(name,url,mode,iconimage,fanart):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        liz.setProperty('fanart_image',fanart)
        xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        ##ok=xbmcplugin.addSortMethod(int(sys.argv[1]), xbmcplugin.SORT_METHOD_TITLE)
        return ok
        
              
params=get_params()
url=None
name=None
mode=None
thumb=None
fanart=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass
try:
        thumb=urllib.unquote_plus(params["thumb"])
except:
        pass
try:
        fanart=urllib.unquote_plus(params["fanart"])
except:
        pass
    
print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "Thumb: "+str(thumb)
print "FanArt: "+str(fanart)

if mode==None or url==None or len(url)<1:
        print ""
        CATEGORIES()
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==1:
        print ""+url
        SHOWS(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==2:
        print ""+url
        EPISODES(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==3:
        print ""+url
        SOURCES(url,name)
##        xbmcplugin.endOfDirectory(int(sys.argv[1]))
##        
##elif mode==4:
##        print ""+url
##        VID(url,name)
