﻿import sys, os
import urllib, urllib2, re

import xbmc, xbmcplugin, xbmcgui, xbmcaddon

#TV DASH - by You 2008.

addon_id = 'plugin.video.hala'
addon = xbmcaddon.Addon('plugin.video.hala')
halapath = addon.getAddonInfo('path')

def CATEGORIES():
        addDir('مسلسلات رمضان 2016','http://www.haladrama.tv/browse.php?c=466&p=1',1,os.path.join(halapath,'resources', 'Categories','19.jpg'))
        addDir('مسلسلات رمضان 2015','http://www.haladrama.tv/browse.php?c=220&p=1',1,os.path.join(halapath,'resources', 'Categories','12.jpg'))
        addDir('مسلسلات رمضان 2014','http://www.haladrama.tv/browse.php?c=23&p=1',1,os.path.join(halapath,'resources', 'Categories','11.jpg'))
        
        addDir('مسلسلات عربية','http://www.haladrama.tv/browse.php?c=3&p=1',1,os.path.join(halapath,'resources', 'Categories','13.jpg'))
        addDir('مسلسلات تركية','http://www.haladrama.tv/browse.php?c=2&p=1',1,os.path.join(halapath,'resources', 'Categories','14.jpg'))
        addDir('برامج تلفزيونية','http://www.haladrama.tv/browse.php?c=6&p=1',1,os.path.join(halapath,'resources', 'Categories','15.jpg'))
        addDir('مسلسلات اجنبية','http://www.haladrama.tv/browse.php?c=5&p=1',1,os.path.join(halapath,'resources', 'Categories','16.jpg'))
        addDir('مسلسلات الاسيوية','http://www.haladrama.tv/browse.php?c=1&p=1',1,os.path.join(halapath,'resources', 'Categories','17.jpg'))
        addDir('مسلسلات هندية','http://www.haladrama.tv/browse.php?c=4&p=1',1,os.path.join(halapath,'resources', 'Categories','18.jpg'))

        addDir('افلام اجنبية','http://www.haladrama.tv/browse.php?c=178&p=1',2,os.path.join(halapath,'resources', 'Categories','20.jpg'))
        addDir('افلام هندية','http://www.haladrama.tv/browse.php?c=179&p=1',2,os.path.join(halapath,'resources', 'Categories','21.jpg'))

def SHOWS(url):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<p class="subcategory">            <a href="(.+?)"><i class="fa fa-folder"></i>(.+?)</a>          </p>').findall(link)
        for url, name in match:
            url2=url.replace("http://www.haladrama.tv/", "")
            thumb="http://www.haladrama.tv/files/image/"+url2+".jpg"
            addDir(name.replace("حلقات مسلسل ", ""),url,2,thumb)
            
        pages=re.compile('<div class="pagination">(.+?)</div>').findall(link)
        page=re.compile('<a href="(.+?)">([0-9]+?)</a>').findall(pages[0])
        for url, pg in page:
            addDir(pg,url,1,os.path.join(halapath,'resources', 'Pages',pg+'.jpg'))

def EPISODES(url):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<div class="file-info">.+?<img src="(.+?)" width=.+?<div class="file-link">            <a href="(.+?)">(.+?)</a>').findall(link)
        for thumb, epiurl, name in match:
            addDir(name.replace(" مسلسل",""),epiurl,3,thumb)
            
        if "<div class=\"pagination\">" in link:
            pagesection = re.compile('<div class="pagination">(.+?)<div class="file-box clearfix">').findall(link)
            page = re.compile('<a href="(.+?)">(\d)</a>').findall(pagesection[0])
            for epiurl2, name2 in page:
                addDir("Page: "+name2,epiurl2,2,os.path.join(halapath,'resources', 'Pages',name2+'.jpg'))

def SOURCES(url, name, thumb):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile("(0\"|<div class=\"center\"><iframe|<iframe rel=\"nofollow\") src=\"(.+?)\"").findall(link)
        i=1
        for trunc, src in match:
            nom=re.compile('//(.+?)/').findall(src)
            if nom[0] == 'www.cloudy.ec':
                req = urllib2.Request(src)
                req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
                response = urllib2.urlopen(req)
                link=response.read().replace("\n", "")
                response.close()
                match=re.compile('  key: "(.+?)",.+?file:"(.+?)",').findall(link)
                for key, file in match:
                    req2 = urllib2.Request("http://www.cloudy.ec/api/player.api.php?file=" +file+ "&user=&cid=1&pass=&numOfErrors=0&cid2=&cid3=cloudy.ec&key=" +key)
                    req2.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
                    response2 = urllib2.urlopen(req2)
                    link2=response2.read().replace("\n", "")
                    qvid = re.compile('^url=(.+?)&title=').findall(link2)
                    vid = urllib.unquote(qvid[0])
                    addDir("Source "+str(i)+": Cloudy",vid,4,thumb)
                    response2.close()
                    
            elif nom[0] == 'myvi.ru':
                    oid=src.rsplit('/',1)
                    url2='http://myvi.ru/player/api/Video/Get/'+oid[1]+'?sig=%26ap=True'

                    req2 = urllib2.Request(url2)
                    req2.add_header('Host', 'myvi.ru')
                    req2.add_header('User-Agent', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0')
                    req2.add_header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
                    req2.add_header('Accept-Language', 'en-US,en;q=0.5')
                    req2.add_header('Referer', 'http://videoplayer.ru/ru/player/spruto/player.swf?v=3.2.0.14')            
                    req2.add_header('Cookie', '__cfduid=dc22e8ed9d32b16aaeb8b7d9c9579123f1471485289; UniversalUserID=a363f629b0904ac0ab6ac8e7769ed130')
                    response2 = urllib2.urlopen(req2)
                    link2=response2.read().replace("\n", "")
                    response2.close()

                    match2=re.compile('\"video\":\[{\"url\":\"(.+?)\"}\],\"videoId\":\"').findall(link2)
                    for video in match2:
                        vidurl=urllib.unquote_plus(video).replace("\\", "")
                        print 'vidurl: '+vidurl
                    addDir("Source "+str(i)+": myvi",vidurl,6,thumb)

            elif nom[0] == 'www.dailymotion.com':
                print 'dailymotion'
                url0 = re.compile("(.+?)\?").findall(src)
                url = url0[0].replace("embed", "")
                req = urllib2.Request(url)
                req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
                response = urllib2.urlopen(req)
                link=response.read().replace("\n", "")
                response.close()
                match=re.compile("application\\\/x-mpegURL.+?\"}\],(.+?)},").findall(link)
                match2=re.compile("\"(.+?)\":\[{\"type\":\"video\\\/mp4\",\"url\":\"(.+?)\"}\]").findall(match[0])
                for res, video in match2:
                        vidurl=urllib.unquote_plus(video).replace("\\", "")
                        addDir("Source "+str(i)+": Dailymotion "+res+"P",vidurl,4,thumb)
                    
            elif nom[0] == 'player.vimeo.com':
                req = urllib2.Request(src)
                req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
                response = urllib2.urlopen(req)
                link=response.read().replace("\n", "")
                response.close()
                vid=re.compile('\"hd\":\{\"profile\".+?\"url\":\"(.+?)\",\"height\":720').findall(link)
                addDir("Source "+str(i)+": Vimeo",str(vid[0]),4,thumb)
                
            else:
                print 'UNKNOWN SOURCE'
                req = urllib2.Request(src)
                req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
                response = urllib2.urlopen(req)
                link=response.read().replace("\n", "")
                response.close()
                vid=re.compile('http.+?\.(.+?)\.').findall(src)
                addDir("Source "+str(i)+": UNKNOWN"+vid[0],src,5,thumb)
                
            i=i+1

def ply(url,name):
    ok=True
    listitem = xbmcgui.ListItem()
    xbmc.Player().play(url,listitem)
    return ok

def play_myvi(url,name):
    ok=True
    liz=xbmcgui.ListItem()
    dictn = { 'User-Agent' : 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3' , 'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' , 'Accept-Language' : 'en-US,en;q=0.5' , 'Accept-Encoding' : 'gzip, deflate' , 'Cookie' : '__cfduid=dc22e8ed9d32b16aaeb8b7d9c9579123f1471485289; UniversalUserID=a363f629b0904ac0ab6ac8e7769ed130' , 'Connection' : 'keep-alive'}
    keys = urllib.urlencode(dictn)
    vid = url + "|" + keys
    xbmc.Player(xbmc.PLAYER_CORE_DVDPLAYER).play(vid, liz)
    return ok
             
def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param

def addDir(name,url,mode,iconimage):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        liz.setProperty('fanart_image',iconimage)
        xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        ok=xbmcplugin.addSortMethod(int(sys.argv[1]), xbmcplugin.SORT_METHOD_TITLE)
        return ok
        
              
params=get_params()
url=None
name=None
mode=None
thumb=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass
try:
        thumb=urllib.unquote_plus(params["thumb"])
except:
        pass
    
print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "Thumb: "+str(thumb)

if mode==None or url==None or len(url)<1:
        print ""
        CATEGORIES()
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==1:
        print ""+url
        SHOWS(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==2:
        print ""+url
        EPISODES(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==3:
        print ""+url
        SOURCES(url,name,thumb)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==4:
        print "ply url "+url
        ply(url,name)
        
elif mode==5:
        import urlresolver
        print "ply url "+url
        stream_url = urlresolver.resolve(url)
        print "stream_url "+stream_url
        ply(url,name)

elif mode==6:
        print "play_myvi "+url
        play_myvi(url,name)
