﻿# This test program is for finding the correct Regular expressions on a page to insert into the plugin template.
# After you have entered the url between the url='here' - use ctrl-v
# Copy the info from the source html and put it between the match=re.compile('here')
# press F5 to run if match is blank close and try again.

import sys,os
import urllib,urllib2,re, httplib, urllib
import cookielib

##from xbmcstubs_master import xbmc,xbmcplugin,xbmcgui,xbmcaddon

addon_id = 'plugin.video.bokra'
##addon = xbmcaddon.Addon(id=addon_id)
##bokrapath = addon.getAddonInfo('path')
##print bokrapath

def ARABIC(name):
    nomm=name.strip().replace("\xd8\xa1", "A").replace("\xd8\xa2", "A").replace("\xd8\xa3", "A").replace("\xd8\xa4", "O2").replace("\xd8\xa5", "E").replace("\xd8\xa6", "YE2").replace("\xd8\xa7", "A").replace("\xd8\xa8","B").replace("\xd8\xa9","AH").replace("\xd8\xaa","T").replace("\xd8\xab","TH").replace("\xd8\xac","G").replace("\xd8\xad","7").replace("\xd8\xae","7'").replace("\xd8\xaf","D").replace("\xd8\xb0","TH").replace("\xd8\xb1","R").replace("\xd8\xb2","Z").replace("\xd8\xb3","S").replace("\xd8\xb4","SH").replace("\xd8\xb5","SU").replace("\xd8\xb6","DH").replace("\xd8\xb7","TU").replace("\xd8\xb8","ZH").replace("\xd8\xb9","3").replace("\xd8\xba","3'").replace("\xd9\x81","F").replace("\xd9\x82","Q").replace("\xd9\x83","K").replace("\xd9\x84","L").replace("\xd9\x85","M").replace("\xd9\x86","N").replace("\xd9\x87","H").replace("\xd9\x88","W").replace("\xd9\x89","A").replace("\xd9\x8a","Y").replace("\xd9\xb1","A")
    return nomm
#####  def SHOWS(url):  #####
##url='http://www.haladrama.tv/browse.php?c=466&p=1'
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##response = urllib2.urlopen(req)
##link=response.read().replace("\n", "")
##response.close()
####print link
##match=re.compile('<p class="subcategory">            <a href="(.+?)"><i class="fa fa-folder"></i>(.+?)</a>          </p>').findall(link)
##for url, name in match:
##    url2=url.replace("http://www.haladrama.tv/", "")
##    thumb="http://www.haladrama.tv/files/image/"+url2+".jpg"
##    print "thumb: " +thumb+ " url: " +url+ " name: " +name.replace(" حلقات مسلسل", "")
##pages = re.compile('<div class="pagination">(.+?)</div>').findall(link)
##page=re.compile('<a href="(.+?)">([0-9]+?)</a>').findall(pages[0])
##for url, pg in page:
##    print 'PAGE: '+url+' / Page No.: '+pg
####/browse.php?c=246&p=1"   http://www.haladrama.tv/files/image/cat-24.jpg

#####  def EPISODES(url):  #####
##url='http://www.haladrama.tv/browse.php?c=178&p=1'
##req = urllib2.Request(url)
##req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
##response = urllib2.urlopen(req)
##link=response.read().replace("\n", "")
####print link
##response.close()
##match=re.compile('<div class="file-info">.+?<img src="(.+?)" width=.+?<div class="file-link">            <a href="(.+?)">(.+?)</a>').findall(link)
##for thumb, epiurl, name in match:
##    print "thumb: " +thumb+ " url: " +epiurl+ " name: " +ARABIC(name).replace("MSLSL ","").replace("AL7LQAH ","- E")
##if "<div class=\"pagination\">" in link:
##    pagesection = re.compile('<div class="pagination">(.+?)<div class="file-box clearfix">').findall(link)
##    page = re.compile('<a href="(.+?)">(\d)</a>').findall(pagesection[0])
##    for epiurl2, name2 in page:
##        print " url: " +epiurl2+ " Page " +name2

#####  def SOURCES(url, thumb):  #####
##url='http://proxy-08.dailymotion.com/sec(6281b69957d285b6288d987f4c16b9c8)/video/532/539/148935235_mp4_h264_aac_ld.mnft'
##url='http://www.dailymotion.com//video/x2fo0lm'
##url='http://www.haladrama.tv/cat-220'
##url='http://www.dailymotion.com/cdn/manifest/video/x2fo0lm.mnft?auth=1427688613-2562-iqkszy0d-03f127f8d92bfd1ecc647cd213ca002a'
##url='http://www.dailymotion.com/swf/video/x211irl?wmode=transparent&isInIframe=1&start=&api=false&id=&network=dsl&startscreen=flash&controls=flash&is_webapp=false&related=1&reporting=1&logo=0&social=true&syndication=&analytics_services%5Bpinba%5D%5Benable%5D=true&cookie_root_domain=.dailymotion.com&device_type=desktop&env=prod&gifloader=false&ip=73.21.42.207&referer=http%3A%2F%2Fwww.dailymotion.com&s_server_time=05%3A12-Tuesday&userEnv%5Bas%5D=AS7922&userEnv%5Bcc%5D=US&userEnv%5Bversion%5D=us&sdk_liverail%5Bswf%5D=http%3A%2F%2Fvox-static.liverail.com%2Fswf%2Fv4%2Fadmanager.swf&sdk_liverail%5Bjs%5D=http%3A%2F%2Fcdn-stat'
##url='www.dailymotion.com/track/player.stream.rebuffer?xid=x2fo0lm%26as=AS7922%26country=US%26cell=core%26proxy=proxy-83.dailymotion.com%26comment=none%26mode=auto%26_n=17918345'
##url='http://proxy-83.dailymotion.com/video/411/742/147247114_mp4_h264_aac_hd.mp4?auth=1427516515-4098-5s3bgikz-eedb0eb484c08d36ef9034be5638668b#cell=core'
##url='http://www.dailymotion.com/track/vod.flash.globalAd.init?xid=x2go75v&as=AS7922&country=US&_n=49284492'
##url='http://www.haladrama.tv/5468.html'
##url='http://www.haladrama.tv/6222.html'
##url='http://www.haladrama.tv/17.html'
##url='http://www.haladrama.tv/547.html'
url='http://www.haladrama.tv/5644.html'

##url='http://www.haladrama.tv/7884.html'
##
req = urllib2.Request(url)
req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
response = urllib2.urlopen(req)
link=response.read().replace("\n", "")
##print link
response.close()
match=re.compile("(0\"|<div class=\"center\"><iframe|<iframe rel=\"nofollow\") src=\"(.+?)\"").findall(link)
print match
print '1111111111111111'
i=1
for trunc, src in match:
    nom=re.compile('//(.+?)/').findall(src)
    print nom
    print '22222222222222'
    if nom[0] == 'www.cloudy.ec':
        req = urllib2.Request(src)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('  key: "(.+?)",.+?file:"(.+?)",').findall(link)
        ##print match
        for key, file in match:
            uri = "http://www.cloudy.ec/api/player.api.php?file=" +file+ "&user=&cid=1&pass=&numOfErrors=0&cid2=&cid3=cloudy.ec&key=" +key
            print 'URI: '+ uri
            req2 = urllib2.Request("http://www.cloudy.ec/api/player.api.php?file=" +file+ "&user=&cid=1&pass=&numOfErrors=0&cid2=&cid3=cloudy.ec&key=" +key)
            req2.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
            response2 = urllib2.urlopen(req2)
            link2=response2.read().replace("\n", "")
            qvid = re.compile('^url=(.+?)&title=').findall(link2)
            vid = urllib.unquote(qvid[0])
            print "Source "+str(i)+": Cloudy"+vid
            response2.close()
    elif nom[0] == 'myvi.ru':
            print "myvi.ru"
            oid=src.rsplit('/',1)
            print 'SRC: '+src
            url2='http://myvi.ru/player/api/Video/Get/'+oid[1]+'?sig=%26ap=True'
            ##url2='http://myvi.ru/player/embed/html/otFUZngzBnw0x-aEMDEPNyUQ-6daf7Qyz5iT1hI0zkKr0mePN4wpyJozfbQGyOfFD0'

            print 'URL2: '+ url2
            txdata = None
            txheaders =  {'User-agent' : 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0'}
            req = urllib2.Request('http:'+src, txdata, txheaders)
            handle = urllib2.urlopen(req)
            link2=handle.read()
            cj=handle.info().getheader('Set-Cookie')
            print cj
            cookie=re.compile('__cfduid=(.+?);.+?UniversalUserID=(.+?);').findall(cj)
            cookies='__cfduid='+str(cookie[0][0])+'; UniversalUserID='+str(cookie[0][1])
            print cookies
##            opener.addheaders.append(("Host", "www.myvi.ru"))
##            opener.addheaders.append(("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0"))
##            opener.addheaders.append(("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"))
##            opener.addheaders.append(("Accept-Language", "en-US,en;q=0.5"))
##            opener.addheaders.append(("Accept-Encoding", "gzip, deflate"))           
##            opener.addheaders.append(("Cookie", "UniversalUserID=19f393774ad3495badd13a261ba9630c;.ASPXANONYMOUS=A3rH-HeF0QEkAAAAOTcwYjJhYmMtZDM0OS00OTBmLWJiMDItMzJkZjJhNDIyMWU50;vp=0.5"))
##            opener.addheaders.append(("Cookie", "__cfduid=dc22e8ed9d32b16aaeb8b7d9c9579123f1471485289; UniversalUserID=a363f629b0904ac0ab6ac8e7769ed130"))
##            opener.addheaders.append(("Connection", "keep-alive"))

            
            req2 = urllib2.Request(url2)
            req2.add_header('Host', 'myvi.ru')
            req2.add_header('User-Agent', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0')
            req2.add_header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
            req2.add_header('Accept-Language', 'en-US,en;q=0.5')
            req2.add_header('Referer', 'http://videoplayer.ru/ru/player/spruto/player.swf?v=3.2.0.14')            
            req2.add_header('Cookie', '__cfduid=dc22e8ed9d32b16aaeb8b7d9c9579123f1471485289; UniversalUserID=a363f629b0904ac0ab6ac8e7769ed130')
            
            ##req2.add_header('Cookie', '__cfduid=d77c366aba9e7114de9f733b698bd4ab41471838413; UniversalUserID=58224d535f9d49e79f8477ca20f6d2da')
            ##req2.add_header('Cookie', cookies)
##            req2.add_header('Connection', 'keep-alive')
            response2 = urllib2.urlopen(req2)
            
            link2=response2.read().replace("\n", "")
            response2.close()
            
            match2=re.compile('\"video\":\[{\"url\":\"(.+?)\"}\],\"videoId\":\"').findall(link2)
            ##print match2
            for video in match2:
                vidurl=urllib.unquote_plus(video).replace("\\", "")
                print 'vidurl: '+vidurl

            print 'I: '+str(i)
            print "Source "+str(i)+": myvi: "+vidurl

    elif nom[0] == 'www.dailymotion.com':
        print '444444444444444444444444444444'
        url0 = re.compile("(.+?)\?").findall(src)
        url = url0[0].replace("embed", "")
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        ##print link
        response.close()
        ##match=re.compile("autoURL%22%3A%22(.+?)%22%2C%22").findall(link)
        match=re.compile("application\\\/x-mpegURL.+?\"}\],(.+?)},").findall(link)
        match2=re.compile("\"(.+?)\":\[{\"type\":\"video\\\/mp4\",\"url\":\"(.+?)\"}\]").findall(match[0])
        ##print match2
        for res, video in match2:
            vidurl=urllib.unquote_plus(video).replace("\\", "")
            print 'RES: '+res+' / VIDURL: '+vidurl
        print "Source "+str(i)+": Dailymotion"+vidurl
        
    elif nom[0] == 'player.vimeo.com':
        req = urllib2.Request(src)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        vid=re.compile('\"hd\":\{\"profile\".+?\"url\":\"(.+?)\",\"height\":720').findall(link)
        print "Source "+str(i)+": Vimeo"+str(vid[0])
    else:
        req = urllib2.Request(src)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        vid=re.compile('http.+?\.(.+?)\.').findall(src)
        print "Source "+str(i)+" / SRC: "+ src+ " :Not Determined"
    i=i+1

