# -*- coding: utf-8 -*-
import sys, os
import urllib, urllib2, re
import xbmc, xbmcplugin, xbmcgui, xbmcaddon

addon_id = 'plugin.video.shahidlive'
addon = xbmcaddon.Addon('plugin.video.shahidlive')
shahidlivepath = addon.getAddonInfo('path')
host = 'http://www.shahidlive.co'


def CATEGORIES():
        req = urllib2.Request(host)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0')
        req.add_header('Accept', 'text/html,application/xhtml+xm…plication/xml;q=0.9,*/*;q=0.8')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<div class="row cat">.+?<h2><a href="(.+?)" title="(.+?)" >').findall(link)
        for url, name in match:
            img=url.replace("/", "")
            if url == '/Cat-27-1' or url == '/Cat-43-1' or url == '/Cat-42-1' or url == '/Cat-118-1':
                thumb=os.path.join(shahidlivepath,'resources', 'Categories',img+'.jpg')
                addDir(name,host+url,1,thumb)
            else:
                thumb=os.path.join(shahidlivepath,'resources', 'Categories',img+'.jpg')
                addDir(name,host+url,2,thumb)

                       
def SUBCATEGORIES(url1):
        req = urllib2.Request(url1)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0')
        req.add_header('Accept', 'text/html,application/xhtml+xm…plication/xml;q=0.9,*/*;q=0.8')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<div class=" subcat">.+?<a href="(.+?)".+?title="(.+?)"').findall(link)
        
        for url, name in match:
            if url == 'Cat-27-1' or url == 'Cat-98-1':
                req = urllib2.Request(host+"/"+url)
                req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0')
                response = urllib2.urlopen(req)
                link=response.read().replace("\n", "")
                response.close()
                match=re.compile('<div class=" subcat">.+?<a href="(.+?)" title="(.+?)" >').findall(link)
                for url, name in match:
                    img=url.replace("/", "")
                    thumb=os.path.join(shahidlivepath,'resources', 'Categories',img+'.jpg')
                    addDir(name,host+"/"+url,2,thumb)
            else:
                img=url.replace("/", "")
                thumb=os.path.join(shahidlivepath,'resources', 'Categories',img+'.jpg')
                addDir(name,host+"/"+url,2,thumb)

def SHOWS(url):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0')
        req.add_header('Accept', 'text/html,application/xhtml+xm…plication/xml;q=0.9,*/*;q=0.8')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<div class="col-md-3 col-sm-4 col-xs-4 col-xxs-6 item">.+?<a href="(.+?)">.+?<img src=.+?data-src="(.+?)".+?<div class="title"><h4>(.+?)</h4>').findall(link)
        for url1, thumb1, name1 in match:
            if re.compile('http.+?(Album)-.+?').findall(url1)==['Album'] or url1.find('/a_') > -1 or url1.find('/a1_') > -1:
                addDir(name1,host+url1,2,thumb1)
            elif url1.find('Video') > -1 or url1.find('/v_') > -1 or url1.find('/vi_')> -1 or url1.find('/vid_') > -1 or url1.find('/vidpage_') > -1:
                addDir(name1,host+url1,3,thumb1)
            else:
                addDir(name1,host+url1,2,thumb1)
        if re.match('.+?-\d+$', url) > 0:
            ma = re.search('-\d+$', url)
            ind = int(ma.group(0).replace("-",""))+1
            page=re.sub('-\d+$', '-'+str(ind), url)
            addDir('Page '+str(ind),page,2,os.path.join(shahidlivepath,'resources','Pages',str(ind)+'.jpg'))
        pagination = re.compile('class="page" href="(.+?)">(.+?)<').findall(link)
        for page, name1 in pagination:
            addDir('Page '+name1,host+"/"+page,2,os.path.join(shahidlivepath,'resources','Pages',name1+'.jpg'))

def VIDEOLINKS(url,name):
        getid=re.compile('[-_](.+?)(?:_\d)?$').findall(url)
        ##getid=re.compile('[-_](.+?)$').findall(url)
        vid= 'http://www.shahidlive.co/Play/'+getid[0]+'-768-431'
        print 'VID: '+vid
        req = urllib2.Request(vid)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0')
        req.add_header('Accept', 'text/html,application/xhtml+xm…plication/xml;q=0.9,*/*;q=0.8')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        if link.find("data-video-id") > -1:
            vidlink=re.compile('data-video-id="(.+?)" >').findall(link)
            configreq2 = urllib2.Request('http://cdn.playwire.com/v2/15448/related/'+vidlink[0]+'.json')
            configresponse2 = urllib2.urlopen(configreq2)
            config2=configresponse2.read()
            configresponse2.close()
            vidlink2=re.compile('mp4:video-(.+?)-').findall(config2)
            vidlink3='http://cdn.playwire.com/15448/video-'+vidlink2[0]+'-'+vidlink[0]+'.mp4'
            playVid(vidlink3,name)
        elif link.find("youtube-player") > -1:
            match=re.compile('src="(.+?)"').findall(link)
            vidlink=match[0].replace(" ", "")
            print "YouTube url "+vidlink
            import urlresolver            
            stream_url = urlresolver.resolve(vidlink)
            playVid(stream_url,name)                
        else:
            match=re.compile('<iframe src="(.+?)"').findall(link)
            print 'if-else http://shahidlive.com/'
            vidlink=match[0].replace(" ", "")
            if vidlink.find("http:") == -1:
                vidlink = 'http:' + vidlink
            req = urllib2.Request(vidlink)
            req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0')
            req.add_header('Accept', 'text/html,application/xhtml+xm…plication/xml;q=0.9,*/*;q=0.8')
            response = urllib2.urlopen(req)
            link=response.read().replace("\n", "")
            response.close()
            if link.find("http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js") > -1:
                rtmp=re.compile('poster="(.+?)".+?<source src="(.+?)"').findall(link)
                thumb=rtmp[0][0]
                r=rtmp[0][1].split('&')
                tcurl=r[0]
                playpath=r[1]
                swfplayer='http://vjs.zencdn.net/4.12/video-js.swf'
                print 'Poster: '+ thumb
                print 'SWFPlayer: '+ swfplayer
                print 'PlayPath: '+ playpath
                print 'tcUrl: '+ tcurl
                print 'PageURL: '+ vidlink
                print 'VidURL: ' + tcurl+'&'+playpath
                ##playRtmp(vidlink,playpath,swfplayer,tcurl,name,thumb)
                playVid(tcurl+'&'+playpath,name)
            elif link.find("nadstream_new") > -1:
                print "nadstream_new"
                rtmp=re.compile('poster="(.+?)".+?<source src="(.+?)"').findall(link)
                
                if rtmp[0][0].startswith('//'): thumb='http:'+rtmp[0][0]
                else: thumb=rtmp[0][0]

                if rtmp[0][1].startswith('//'): VidURL='http:'+rtmp[0][1]
                else: VidURL=rtmp[0][1]
                
##                if rtmp[0][1].find("&") > -1:
##                        r=rtmp[0][1].split('&')
##                        tcurl=r[0]
##                        playpath=r[1]
##                        swfplayer='http://vjs.zencdn.net/swf/5.0.1/video-js.swf'
##                        print 'Poster: '+ thumb
##                        print 'SWFPlayer: '+ swfplayer
##                        print 'PlayPath: '+ playpath
##                        print 'tcUrl: '+ tcurl
##                        print 'PageURL: '+ vidlink
##                        playRtmp(vidlink,playpath,swfplayer,tcurl,name,thumb)
##                else:
                print 'VidURL: ' + VidURL
                playVid(VidURL,name)
            else:
                rtmp=re.compile('"flashplayer": "(.+?)",.+?\'image\':\'(.+?)\',.+?"file": "(.+?)",.+?"streamer": "(.+?)",.+?').findall(link)
                for flashplayer, thumb, playpath, streamer in rtmp:
                        print 'URL: '+ vidlink
                        print 'Thumb: '+ thumb
                        print 'Streamer: '+ streamer
                        print 'PlayPath: '+ playpath
                        print 'Flashplayer: http://nadstream.shahidlive.com+'+flashplayer
                        playRtmp(vidlink,playpath,'http://nadstream.shahidlive.com'+flashplayer,streamer,name,thumb)
            
def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param

def playRtmp(pageurl,playpath,flashplayer,tcurl,name,iconimage):
        ok=True
        print "rtmp: "+tcurl+playpath
        
        liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        liz.setProperty('flashver', 'LNX 11,2,202,559')
        liz.setProperty('SWFPlayer',flashplayer)
        liz.setProperty('PlayPath', playpath)
        liz.setProperty('tcUrl', tcurl)
        liz.setProperty('PageURL', pageurl)
        xbmc.Player(xbmc.PLAYER_CORE_DVDPLAYER).play(tcurl, liz)
        ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=url,listitem=liz)
        return ok
    
def playVid(url,name):
        ok=True
        listitem = xbmcgui.ListItem(name)
        xbmc.Player().play(url,listitem)
        return ok

def addDir(name,url,mode,iconimage):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        liz.setProperty('fanart_image',iconimage)
        ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        return ok
        
              
params=get_params()
url=None
name=None
mode=None
thumb=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass
try:
        thumb=urllib.unquote_plus(params["thumb"])
except:
        pass

print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "Thumb: "+str(thumb)

if mode==None or url==None or len(url)<1:
        print ""
        CATEGORIES()
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
       
elif mode==1:
        print ""+url
        SUBCATEGORIES(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==2:
        print ""+url
        SHOWS(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==3:
        print ""+url
        VIDEOLINKS(url,name)
