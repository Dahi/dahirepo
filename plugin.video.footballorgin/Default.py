import sys, os
import urllib, urllib2, re

import xbmc, xbmcplugin, xbmcgui, xbmcaddon

#TV DASH - by You 2008.

addon_id = 'plugin.video.footballorgin'
addon = xbmcaddon.Addon('plugin.video.footballorgin')
footballorginpath = addon.getAddonInfo('path')


def CATEGORIES():
        addDir('Full Match Replay','http://www.footballorgin.com/category/full-match-replay/',1,os.path.join(footballorginpath,'resources','fullmatchreplay.png'),0)
        addDir('Review Show','http://www.footballorgin.com/category/review-show/',1,os.path.join(footballorginpath,'resources','Review Show.jpg'),0)
        addDir('Premier League - EPL','http://www.footballorgin.com/category/leagues/premier-league-epl/',1,os.path.join(footballorginpath,'resources','49901.jpg'),0)
        addDir('La Liga','http://www.footballorgin.com/category/leagues/la-liga/',1,os.path.join(footballorginpath,'resources','201.jpg'),0)
        addDir('Scottish Premiership','http://www.footballorgin.com/category/leagues/scottish-premiership/',1,os.path.join(footballorginpath,'resources','scottish premier league.png'),0)
        addDir('Championship','http://www.footballorgin.com/category/leagues/championship/',1,os.path.join(footballorginpath,'resources','Championship.jpg'),0)
        addDir('Serie A','http://www.footballorgin.com/category/leagues/serie-a/',1,os.path.join(footballorginpath,'resources','2801.jpg'),0)
        addDir('TV Show','http://www.footballorgin.com/category/tv-show/',1,os.path.join(footballorginpath,'resources','TV Show.jpg'),0)
        addDir('Cup Games','http://www.footballorgin.com/category/cup-games/',1,os.path.join(footballorginpath,'resources','cup games.jpg'),0)
        addDir('UFC','http://www.footballorgin.com/category/ufc/',1,os.path.join(footballorginpath,'resources','ufc.jpg'),0)
            
def SHOWS(url,t):
        j=t+1
        values = {'action': 'infinite_scroll',
                  'page_no': j }
        headers = {'Accept': '*/*',
                   ##'Accept-Encoding': 'gzip, deflate, lzma',
                   'Accept-Language': 'en-US,en;q=0.8',
                   'Connection': 'keep-alive',
                   'Content-Length': 32,
                   'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                   ##'Cookie': 'b72609bfe00df3ae2da0960ed6e34f4c=6f221aafe0f7bfa5b1e84d595f12fd88; pwUID=497128534879415; playwirePageViews=2; _pwbolt_id.9238=62b3e781-cec3-4954-8c7f-f5e917a80cfd.1486001113.1.1486001239.1486001113.06b18c79-0392-46bd-848c-8236e8004bd5; _ga=GA1.2.2047591863.1486001113',
                   'Host': 'www.footballorgin.com',
                   'Origin': 'http://www.footballorgin.com',
                   ##'Referer': 'http://www.footballorgin.com/category/review-show/',
                   'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0',
                   'X-Requested-With': 'XMLHttpRequest'
                   }

        data = urllib.urlencode(values)
        req = urllib2.Request(url, data, headers)
        response = urllib2.urlopen(req)
        link = response.read().replace("\n", "")
        match=re.compile('<article id="post.+?src="(.+?)".+?<h2 class="entry-title"><a href="(.+?)" rel="bookmark">(.+?)</a></h2>').findall(link)
        for thumb, url2, title in match:
            title=title.replace("&#8211;","-")
            print 'url: '+ url2+' / title: '+title+' / THUMB: '+ thumb
            addDir(title,url2,2,thumb,j)
        addDir('Next',url,1,thumb,j)

def EPISODES(url,thumb):        
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
        req.add_header('Accept-Language', 'en-US,en;q=0.5')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        match=re.compile('<li class="subpage-.+?<a href="(.+?)">(.+?)</a>').findall(link)
        for url, title in match:
            print 'URL: '+ url+' / title: '+title
            addDir(title,url,3,thumb,0)
        
def VID(url,name,thumb):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
        req.add_header('Accept-Language', 'en-US,en;q=0.5')
        response = urllib2.urlopen(req)
        link = response.read().replace("\n", "")
        response.close()
        try:
                match=re.compile('<script data-config="(.+?)"').findall(link)
                config='https:'+match[0]
                req = urllib2.Request(config)
                req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
                req.add_header('Accept-Language', 'en-US,en;q=0.5')
                response = urllib2.urlopen(req)
                config_link = response.read().replace("\n", "")
                response.close()
                match_config=re.compile('"f4m":"(.+?)"').findall(config_link)

                req = urllib2.Request(match_config[0])
                req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
                req.add_header('Accept-Language', 'en-US,en;q=0.5')
                response = urllib2.urlopen(req)
                vid_link = response.read().replace("\n", "")
                response.close()
                match_vid=re.compile('<media url="http(.+?)"').findall(vid_link)
                url_vid="http"+match_vid[0]
                print url_vid
                playVid(url_vid,name)
        except:
                if '<iframe src=' in link:
                        match=re.compile('<iframe src="(.+?)"').findall(link)
                else:
                        match=re.compile('" src="(.+?)"').findall(link)
                config='https:'+match[0]

                req = urllib2.Request(config)
                req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
                req.add_header('Accept-Language', 'en-US,en;q=0.5')
                response = urllib2.urlopen(req)
                config_link = response.read().replace("\n", "")
                response.close()

                vid2=re.compile('{\\\&quot;name\\\&quot;:\\\&quot;(.+?)\\\&quot;.+?url\\\&quot;:\\\&quot;(.+?)\\\&quot;').findall(config_link)
                for name, vidurl in vid2:
                        mediaurl= vidurl.replace('\\\\u0026','&')
                        print 'Name: ok.ru ('+name+') / URL: '+mediaurl
                        print 'Name: ok.ru ('+name+') / URL: '+mediaurl
                        addDir('ok.ru ('+name+')',mediaurl,4,' ',thumb)
        
        return

def playVid(url,name):
        listitem = xbmcgui.ListItem(name)
        xbmc.Player().play(url,listitem)
        return
             
def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param

def addDir(name,url,mode,iconimage,i):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)+"&i="+str(i)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        ##liz.setProperty('fanart_image',fanart)
        xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        ##ok=xbmcplugin.addSortMethod(int(sys.argv[1]), xbmcplugin.SORT_METHOD_TITLE)
        return ok
        
              
params=get_params()
url=None
name=None
mode=None
thumb=None
i=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass
try:
        thumb=urllib.unquote_plus(params["thumb"])
except:
        pass
try:
        i=int(params["i"])
except:
        pass
    
print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "Thumb: "+str(thumb)
print "i: "+str(i)

if mode==None or url==None or len(url)<1:
        print ""
        CATEGORIES()
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==1:
        print ""+url
        SHOWS(url,i)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==2:
        print ""+url
        EPISODES(url,thumb)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==3:
        print "stream_url "+url
        VID(url,name,thumb)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode==4:
        print "stream_url "+url
        playVid(url,name)
