# -*- coding: utf-8 -*-

import sys, os, base64
import urllib, urllib2, re
import xbmc, xbmcplugin, xbmcgui, xbmcaddon

addon_id = 'plugin.video.yallashoot'
addon = xbmcaddon.Addon(addon_id)
yallashootpath = addon.getAddonInfo('path')

def CATEGORIES():
        addDir('مباريات اليوم','http://www.yalla-shoot.com/live/',1,os.path.join(yallashootpath,'resources','leagues.png'))
        addDir('مباريات الغد','http://www.yalla-shoot.com/live/tomorrow_matches.php',1,os.path.join(yallashootpath,'resources','channels.png'))
        

def MATCHES(url):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        if url.find('tomorrow_matches') > -1:
                block=re.compile('<a href="#">(.+?)<div class="cler').findall(link)
        else:
                block=re.compile('<a href="#">.+?target(.+?)<div class="cler').findall(link)
        
        if block[0].find('لا يوجد مباريات مهمة اليوم') > -1:
                print 'لا يوجد مباريات مهمة اليوم'
                addDir('لا يوجد مباريات مهمة اليوم','',2,'')
        else:
                match=re.compile('<a href="(.+?)".+?<span class="matsh_(.+?)">.+?<td align="right" class="fc_name">(.+?)</td>.+?<td align="left" class="fc_name">(.+?)</td>.+?<p>(.+?)</p>.+?<p>(.+?)</p>.+?<p>(.+?)</p>.+?</li><div class="sb_15"></div>').findall(block[0])

                for url,stat,team1,team2,channel,announcer,comp in match:
                    name=comp+': '+team1+' vs. '+team2
                    url='http://www.yalla-shoot.com/live/'+url
                    if stat=="start":
                        stat= "Started"
                    elif stat=="wait":
                        stat= "Starts Soon"
                    else:
                        stat="Not Yet Started"
                    addDir(name,url,2,'')
                    print "URL: http://www.yalla-shoot.com/live/"+url+" / Status: "+stat+" /  Competition: "+comp+' / Teams: '+team1+' vs. '+team2+' / Channel: '+channel+" / Announcer: "+announcer


def SOURCES(url):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
        response = urllib2.urlopen(req)
        link=response.read().replace("\n", "")
        response.close()
        tabs=re.compile('var idsem = {(.+?)}').findall(link)
        tabs=tabs[0]+','
        sources=re.compile('"(.+?)":"(.+?)",').findall(tabs)
        block=re.compile('role="tablist" id="tablist">(.+?)</ul><div class="tab-content"').findall(link)
        match=re.compile('href="(.+?)" class="tabs.+?;br /&gt;.+?:(.+?)">(.+?)</a></li>').findall(block[0])

        for id, source,quality in match:
            id=id.replace('#tab-','')
            
            for j in range(0, len(match)):
                if sources[j][0] == id:            
                    src=re.compile('src="(.+?)"').findall(base64.b64decode(sources[j][1]))
                    try:
                            print "url: "+src[0]+" / SOURCE: "+source+" / QUALITY: "+quality
                            name='SOURCE: '+source+' ('+quality+')'
                            addDir(name,src[0],3,'')
                    except:
                            pass
          
def VIDLINKS(url,name):
        if name.find('OK.RU') > -1:
                print 'OK.RU'
                
                import urlresolver
                stream_url = urlresolver.resolve(url)
                print stream_url

                ply(stream_url,name)
        else:
                req = urllib2.Request(url)
                req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                response = urllib2.urlopen(req)
                link=response.read().replace("\n", "")
                response.close()

                if name.find('Brocast') > -1:
                        print 'Brocast'
                        match1=re.compile('>id=\'(.+?)\'').findall(link)
                        url1= 'http://bro.adca.st/stream.php?id='+match1[0]

                        req = urllib2.Request(url1)
                        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                        req.add_header('Referer', url)
                        response = urllib2.urlopen(req)
                        link2=response.read().replace("\n", "")
                        response.close()
                        match2=re.compile('curl = "(.+?)"').findall(link2)
                        vid=base64.b64decode(match2[0])

                        req = urllib2.Request('http://bro.adca.st/getToken.php')
                        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                        req.add_header('Accept', 'application/json, text/javascript, */*; q=0.01')
                        req.add_header('X-Requested-With', 'XMLHttpRequest')
                        req.add_header('Referer', url1)
                        response = urllib2.urlopen(req)
                        link3=response.read().replace("\n", "")
                        response.close()
                        match3=re.compile('{"token":"(.+?)"').findall(link3)
                        token=match3[0]
                        token='IjUUQNgrViAV5tk9-_vfUg'
                        url=vid+token

                        dictn = { 'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0' ,
                                  'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' ,
                                  'Accept-Language' : 'en-US,en;q=0.5' ,
                                  'Accept-Encoding' : 'gzip, deflate' ,
                                  'Referer' : 'http://cdn.allofme.site/jw/jwplayer.flash.swf' ,
                                  'DNT' : '1' ,
                                  'Connection' : 'keep-alive' ,
                                  'If-Modified-Since' : 'Wed, 28 Sep 2016 23:12:33 GMT' ,
                                  'If-None-Match': '"57ec4e61-269"'
                                  }
                        keys = urllib.urlencode(dictn)                
                        stream_url = url + "|" + keys
                        stream_url='http://185.45.14.202/swf/57c8733ab361e-33154103790.swf'
                        
                        ply(stream_url,name)

                elif name.find('فيس بوك') > -1:
                        print 'فيس بوك'
                        
                        match1=re.compile('src="https://www.facebook.com(.+?)"').findall(link)
                        url1='https://www.facebook.com'+urllib.unquote_plus(match1[0])
                        
                        import urlresolver
                        stream_url = urlresolver.resolve(url1)
                        print "stream_url "+stream_url
                        
                        ply(stream_url,name)

                elif name.find('Zony') > -1:
                        print 'Zony'

                        match1=re.compile('width=(.+?), height=(.+?), channel=\'(.+?)\'.+?g=\'(.+?)\'').findall(link)
                        pageUrl= 'http://www.zony.tv/embedplayer/'+urllib.unquote_plus(match1[0][2])+'/'+urllib.unquote_plus(match1[0][3])+'/'+match1[0][0]+'/'+match1[0][1]
                        print pageUrl
                        
                        req = urllib2.Request(pageUrl)
                        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                        req.add_header('Host', 'www.zony.tv')
                        req.add_header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
                        req.add_header('Accept-Language', 'en-US,en;q=0.5')
                        req.add_header('Referer', url)
                        req.add_header('Upgrade-Insecure-Requests', '1')
                        response = urllib2.urlopen(req)
                        link2=response.read().replace("\n", "")
                        response.close()
                        match2=re.compile('SWFObject\("(.+?)".+?FlashVars\',.+?id=(.+?)&s=(.+?)&.+?pk=(.+?)\'').findall(link2)
                        print match2

                        url2='http://cdn.pubzony.com:1935/loadbalancer?'+match2[0][1]
                        req = urllib2.Request(url2)
                        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                        response = urllib2.urlopen(req)
                        link3=response.read().replace("\n", "")
                        response.close()

                        host=link3.replace('redirect=','')
                        app='stream'
                        print 'app: '+app
                        swfUrl='http://www.zony.tv'+match2[0][0]
                        print 'swfUrl: '+swfUrl
                        playpath=match2[0][2]+'?id='+match2[0][1]+'&pk='+match2[0][3]
                        print 'PlayPath: '+playpath
                        print 'pageUrl: '+url2
                        tcUrl='rtmp://'+host+'/stream'
                        print 'host: '+host
                        print 'tcUrl: '+tcUrl
                        
                        print 'rtmpdump -a "'+app+'" -f "LNX 11,2,202,632" -v -s "'+swfUrl+'" -t "'+tcUrl+'" -p "'+pageUrl+'" --host "'+host+'" --conn=S:OK -y "'+playpath+'" | "vlc" -'
                        ##print 'rtmpdump -r "'+tcUrl+'/'+playpath+'" --live --conn=S:OK -f "LNX 11,2,202,632" -s "'+swfUrl+'" | "vlc" -'
                        stream_url=tcUrl+'/'+playpath+' conn=S:OK app='+app+' flashVer=LNX\\2011,2,202,632 swfUrl='+swfUrl+' tcUrl='+tcUrl+' pageUrl='+pageUrl
                        
                        ply(stream_url,name)


                elif name.find('P3G') > -1:
                        print 'P3G'
                        match1=re.compile('width=(.+?), height=(.+?); channel=\'(.+?)\'.+?g=\'(.+?)\'').findall(link)
                        pageUrl= 'http://www.p3g.tv/embedplayer/'+urllib.unquote_plus(match1[0][2])+'/'+urllib.unquote_plus(match1[0][3])+'/'+match1[0][0]+'/'+match1[0][1]
                        
                        req = urllib2.Request(pageUrl)
                        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                        req.add_header('Host', 'www.p3g.tv')
                        req.add_header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
                        req.add_header('Accept-Language', 'en-US,en;q=0.5')
                        req.add_header('Referer', url)
                        req.add_header('Upgrade-Insecure-Requests', '1')
                        response = urllib2.urlopen(req)
                        link2=response.read().replace("\n", "")
                        response.close()

                        match2=re.compile('SWFObject\("(.+?)".+?FlashVars\',.+?id=(.+?)&s=(.+?)&.+?pk=(.+?)\'').findall(link2)
                        url3='http://www.p3gpublish.com:1935/loadbalancer?'+match2[0][1]
                        req = urllib2.Request(url3)
                        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                        response = urllib2.urlopen(req)
                        link3=response.read().replace("\n", "")
                        response.close()

                        host=link3.replace('redirect=','')
                        app='live'
                        print 'app: '+app
                        swfUrl='http://www.p3g.tv'+match2[0][0]
                        print 'swfUrl: '+swfUrl
                        playpath=match2[0][2]+'?id='+match2[0][1]+'&pk='+match2[0][3]
                        print 'PlayPath: '+playpath
                        print 'pageUrl: '+pageUrl
                        tcUrl='rtmp://'+host+'/live'
                        print 'host: '+host
                        print 'tcUrl: '+tcUrl
                        
                        print 'rtmpdump -a "'+app+'" -f "LNX 11,2,202,632" -v -s "'+swfUrl+'" -t "'+tcUrl+'" -p "'+pageUrl+'" --host "'+host+'" --conn=S:OK -y "'+playpath+'" | "vlc" -'
                        stream_url=tcUrl+'/'+playpath+' conn=S:OK app='+app+' flashVer=LNX\\2011,2,202,632 swfUrl='+swfUrl+' tcUrl='+tcUrl+' pageUrl='+pageUrl
                        ply(stream_url,name)
                        
                elif name.find('JanJua') > -1:
                        print 'JanJua'
                        
                        match2=re.compile('width=(.+?), height=(.+?), channel=\'(.+?)\', g=\'(.+?)\'').findall(link)
                        pageUrl='http://www.janjuaplayer.com/embedplayer/'+urllib.unquote(match2[0][2])+'/'+match2[0][3]+'/'+match2[0][0]+'/'+match2[0][1]
                        print 'pageUrl: '+pageUrl
                        req = urllib2.Request(pageUrl)
                        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                        req.add_header('Referer', url)
                        response = urllib2.urlopen(req)
                        link3=response.read().replace("\n", "")
                        response.close()
                        match3=re.compile('SWFObject\("(.+?)".+?FlashVars\',.+?id=(.+?)&s=(.+?)&.+?pk=(.+?)\'').findall(link3)

                        url3='http://www.janjuapublisher.com:1935/loadbalancer?'+match3[0][1]
                        req = urllib2.Request(url3)
                        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                        response = urllib2.urlopen(req)
                        link4=response.read().replace("\n", "")
                        response.close()

                        host=link4.replace('redirect=','')
                        tcUrl='rtmp://'+host+'/live'
                        print 'host: '+host
                        print 'tcUrl: '+tcUrl
                        app='live'
                        swfUrl='http://www.janjuaplayer.com'+match3[0][0]
                        print 'swfUrl: '+swfUrl
                        playpath=match3[0][2]+'?id='+match3[0][1]+'&pk='+match3[0][3]
                        print 'PlayPath: '+playpath

                        stream_url=tcUrl+'/'+playpath+' conn=S:OK app='+app+' flashVer=LNX\\2011,2,202,632 swfUrl='+swfUrl+' tcUrl='+tcUrl+' pageUrl='+pageUrl
                        ply(stream_url,name)
                        

                elif name.find('Sawlive') > -1:
                        print 'Sawlive'
                        
                        req = urllib2.Request(url)
                        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                        response = urllib2.urlopen(req)
                        link=response.read().replace("\n", "")
                        response.close()

                        match1=re.compile('src=\'(.+?)\'').findall(link)
                        url1=match1[0]
                        print 'url: '+url1
                        req = urllib2.Request(url1)
                        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                        req.add_header('Referer', url1)
                        response = urllib2.urlopen(req)
                        link3=response.read().replace("\n", "")
                        response.close()

                        match2=re.compile('src="(.+?)\'.+?var .+?=\[(.+?)\].+?\+\'(.+?)\"').findall(link3)
                        s=match2[0][1].replace("\"", "").replace(",", "")
                        pageUrl=match2[0][0]+s+match2[0][2]                
                        print pageUrl

                        
                        req = urllib2.Request(pageUrl)
                        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                        req.add_header('Referer', url)
                        response = urllib2.urlopen(req)
                        link4=response.read().replace("\n", "")
                        response.close()
                        
                        if link4.find('Invalid Embed (Access Denied)') > -1:
                            print 'Access Denied'
                        else:
                            print 'Access Granted'
                        match4=re.compile('SWFObject\(\'(.+?)\'.+?function kb8axk.+?return \'(.+?)\'.+?flile = \'(.+?)\'.+?tkta = \'(.+?)\'.+?bmakz = \'(.+?)\'.+?ques = \'(.+?)\'.+?st2tas =.+?\'(.+?)\'.+?st2tas\+unescape\(\'(.+?)\'').findall(link4)
                        app=urllib.unquote_plus(match4[0][7]).replace('/','')
                        print 'app: '+app
                        swfUrl=match4[0][0]
                        print 'swfUrl: '+swfUrl
                        tcUrl='rtmp://'+match4[0][1]+'/'+urllib.unquote_plus(app)
                        print 'tcUrl: '+tcUrl
                        print 'pageUrl: '+pageUrl
                        playpath=match4[0][2].replace('33333','')+match4[0][5].replace('1?', '?')+match4[0][3]+urllib.unquote_plus(match4[0][4])
                        print 'playpath: '+playpath
                        host=match4[0][6]
                        print 'host: '+host

                        stream_url=tcUrl+'/'+playpath+' conn=S:OK app='+app+' flashVer=LNX\\2011,2,202,632 swfUrl='+swfUrl+' tcUrl='+tcUrl+' pageUrl='+pageUrl
                        ply(stream_url,name)

                        
                elif name.find('Iguide') > -1:
                        print 'Iguide'
                        match1=re.compile('src="(.+?)">').findall(link)
                        url1= match1[0]
                        
                        req = urllib2.Request(url1)
                        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                        response = urllib2.urlopen(req)
                        link2=response.read().replace("\n", "")
                        response.close()
                        match2=re.compile('<iframe src=\'(.+?)\'').findall(link2)
                        pageUrl=match2[0]

                        req = urllib2.Request(pageUrl)
                        req.add_header('Host', 'www.iguide.to')
                        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                        req.add_header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
                        req.add_header('Accept-Language', 'en-US,en;q=0.5')
                        req.add_header('Referer', url)
                        req.add_header('Connection', 'keep-alive')
                        req.add_header('Upgrade-Insecure-Requests', '1')
                        response3 = urllib2.urlopen(req)
                        link3=response3.read().replace("\n", "")
                        response3.close()
                        match3=re.compile('getJSON\("(.+?)".+?\'streamer\': \'(.+?)\'.+?file\': \'(.+?)\'.+?src: \'(.+?)\'').findall(link3)

                        for serverfile, tcUrl, playpath, swfUrl in match3:
                                app='iguide'

                                req = urllib2.Request(serverfile)
                                req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                                req.add_header('Referer', url)
                                req.add_header('X-Requested-With', 'XMLHttpRequest')
                                response = urllib2.urlopen(req)
                                link4=response.read().replace("\n", "")
                                match4=re.compile('token":"(.+?)"').findall(link4)
                                token=match4[0]
                                print 'rtmpdump -r "'+tcUrl+'/'+playpath+'" --live -f "LNX 11,2,202,632" -p "'+pageUrl+'" -s "'+swfUrl+'" -T "'+token+'" | "vlc" -'
                                stream_url=tcUrl+'/'+playpath+' live=1 app='+app+' flashVer=LNX\\2011,2,202,632 swfUrl='+swfUrl+' tcUrl='+tcUrl+' pageUrl='+pageUrl+' token='+token
                                ply(stream_url,name)
                        
                        
                elif name.find('فلاش') > -1:
                        print 'فلاش'
                        
                        match1=re.compile('src="(.+?)"').findall(link)
                        
                        if match1[0].find('library') > -1:
                                match2=re.compile('file: \'(.+?)\'').findall(link)
                                vid=match2[0].replace('\\','')
                                stream_url=vid+' conn=S:OK app=live-md flashVer=LNX\\2011,2,202,632 swfUrl=http://p.jwpcdn.com/6/12/jwplayer.flash.swf pageUrl='+url        
                                print 'rtmpdump -r "'+vid+'" conn=S:OK -f "LNX\\2011,2,202,632" -s "http://p.jwpcdn.com/6/12/jwplayer.flash.swf" -p "'+url+'" | "vlc" -'
                        else:                
                                req = urllib2.Request(match1[0])
                                req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                                response = urllib2.urlopen(req)
                                link2=response.read().replace("\n", "")
                                response.close()
                                match2=re.compile('file: "(.+?)"').findall(link2)
                                stream_url=match2[0]
                                
                        print stream_url
                        ply(stream_url,name)

                elif name.find('يوتيوب') > -1:
                        print 'يوتيوب'
                        if link.find('peer5') > -1:
                                match1=re.compile('file: "(.+?)"').findall(link)
                                stream_url= match1[0]
                        else:
                                req = urllib2.Request(url)
                                req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                                response = urllib2.urlopen(req)
                                link2=response.read().replace("\n", "")
                                response.close()
                                match2=re.compile('src="(.+?)"').findall(link2)
                                
                                import urlresolver
                                stream_url = urlresolver.resolve(match2[0].replace('?autoplay=1',''))
                        print "stream_url "+stream_url
                        ply(stream_url,name)
            
def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param


def ply(url,name):
    ok=True
    liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage='')
    liz.setInfo( type="Video", infoLabels={ "Title": name } )
    xbmc.Player().play(url,liz)
    return ok


##def playRtmp(app,host,pageurl,playpath,swfurl,tcurl,name):
##        ok=True
##        
##        liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage='')
##        liz.setInfo( type="Video", infoLabels={ "Title": name } )
##
##        stream_url=tcurl+'/'+playpath+' conn=S:OK app='+app+' flashVer=LNX\\2011,2,202,632 swfUrl='+swfurl+' tcUrl='+tcurl+' pageUrl='+pageurl
##        xbmc.Player().play(stream_url,liz)
##        return ok


def addDir(name,url,mode,iconimage):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        if mode == 1:
           liz.setInfo( type="Video", infoLabels={ "Title": name, "Plot": "plot"} )
           liz.setProperty('fanart_image','')
        if mode == 2:
           liz.setInfo( type="Video", infoLabels={ "Title": name, "Plot": "plot"} )
           liz.setProperty('fanart_image','')
        elif mode == 3 or mode == 5:
           liz.setInfo( type="Video", infoLabels={ "Title": name, "Plot": url} )
           liz.setProperty('fanart_image','')
##        elif mode == 4:
##           liz.setInfo( type="Video", infoLabels={ "Title": name, "Date": '', "Plot": url} )
##           liz.setProperty('fanart_image','')

        ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        return ok
        
              
params=get_params()

url=None
name=None
mode=None
date=None
matches=None
thumb=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass
try:
        thumb=urllib.unquote_plus(params["thumb"])
except:
        pass
    
print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "Thumb: "+str(thumb)


if mode==None or url==None or len(url)<1:
        print ""
        CATEGORIES()
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==1:
        print ""
        MATCHES(url)
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
        
elif mode==2:
        print ""
        SOURCES(url)
        xbmcplugin.setContent(int(sys.argv[1]),content="episodes")
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
       
elif mode==3:
        ##print ""+matches
        VIDLINKS(url,name)
        xbmcplugin.setContent(int(sys.argv[1]),content="episodes")
##        xbmcplugin.endOfDirectory(int(sys.argv[1]))

